process.stdin.on('data', (data) => {
    const taskDefinition = JSON.parse(data.toString()).taskDefinition;
    taskDefinition.containerDefinitions[0].image = process.argv[2];
    delete taskDefinition.status;
    delete taskDefinition.compatibilities;
    delete taskDefinition.taskDefinitionArn;
    delete taskDefinition.requiresAttributes;
    delete taskDefinition.revision;
    console.log(JSON.stringify(taskDefinition));
});
