#!/usr/bin/env bash
set -e

export DOCKER_BUILDKIT=1

tag=$(git log -1 --pretty=format:%h)
ecr_uri=158026119948.dkr.ecr.eu-west-3.amazonaws.com/solit/staging

# shellcheck disable=SC2091
$(aws ecr get-login --no-include-email --region eu-west-3)
npm run prestart:prod
docker build . -t solit/staging:"${tag}"
docker tag solit/staging:"${tag}" "${ecr_uri}":"${tag}"
docker push 158026119948.dkr.ecr.eu-west-3.amazonaws.com/solit/staging:"${tag}"
echo "Successfully pushed solit/staging:${tag}"
