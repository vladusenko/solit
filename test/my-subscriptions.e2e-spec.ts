process.env.NODE_ENV = process.env.NODE_ENV.toUpperCase();

import {Test, TestingModule} from '@nestjs/testing';
import {AppModule} from '../src/app.module';
import {TemplateProvider, templates} from '../src/app/template-provider';
import {MessageSchedulerRepository} from '../src/app/repositories/message-scheduler.repository';
import {clearDatabase} from './fake-telegram/server';
import * as request from 'supertest';
import {MAIN_MENU} from '../src/app/dictionaries/main-menu';
import * as _ from 'lodash';

const startWebhook = {
  update_id: 123456,
  message: {
    message_id: 123456,
    from: {
      id: 123456,
      is_bot: false,
      first_name: 'FirstName',
      last_name: 'LastName',
      username: 'Username',
      language_code: 'en',
    },
    date: 1562615224,
    chat: {
      id: 123456,
      type: 'private',
      username: 'Username',
    },
    text: '/start',
  },
};

const javascriptTagWebhook = {
  update_id: 123456,
  message: {
    message_id: 123456,
    from: {
      id: 123456,
      is_bot: false,
      first_name: 'FirstName',
      last_name: 'LastName',
      username: 'Username',
      language_code: 'en',
    },
    date: 1562615224,
    chat: {
      id: 123456,
      type: 'private',
      username: 'Username',
    },
    text: 'javascript',
  },
};

const javaTagWebhook = {
  update_id: 123456,
  message: {
    message_id: 123456,
    from: {
      id: 123456,
      is_bot: false,
      first_name: 'FirstName',
      last_name: 'LastName',
      username: 'Username',
      language_code: 'en',
    },
    date: 1562615224,
    chat: {
      id: 123456,
      type: 'private',
      username: 'Username',
    },
    text: 'java',
  },
};

const nodeJsTagWebhook = {
  update_id: 123456,
  message: {
    message_id: 123456,
    from: {
      id: 123456,
      is_bot: false,
      first_name: 'FirstName',
      last_name: 'LastName',
      username: 'Username',
      language_code: 'en',
    },
    date: 1562615224,
    chat: {
      id: 123456,
      type: 'private',
      username: 'Username',
    },
    text: 'node.js',
  },
};

const v8TagWebhook = {
  update_id: 123456,
  message: {
    message_id: 123456,
    from: {
      id: 123456,
      is_bot: false,
      first_name: 'FirstName',
      last_name: 'LastName',
      username: 'Username',
      language_code: 'en',
    },
    date: 1562615224,
    chat: {
      id: 123456,
      type: 'private',
      username: 'Username',
    },
    text: 'v8',
  },
};

const jvmTagWebhook = {
  update_id: 123456,
  message: {
    message_id: 123456,
    from: {
      id: 123456,
      is_bot: false,
      first_name: 'FirstName',
      last_name: 'LastName',
      username: 'Username',
      language_code: 'en',
    },
    date: 1562615224,
    chat: {
      id: 123456,
      type: 'private',
      username: 'Username',
    },
    text: 'jvm',
  },
};

const mySubscriptionsWebhook = {
  update_id: 123456,
  message: {
    message_id: 123456,
    from: {
      id: 123456,
      is_bot: false,
      first_name: 'FirstName',
      last_name: 'LastName',
      username: 'Username',
      language_code: 'en',
    },
    date: 1562615224,
    chat: {
      id: 123456,
      type: 'private',
      username: 'Username',
    },
    text: MAIN_MENU.MY_SUBSCRIPTIONS,
  },
};

describe('My Subscriptions menu command scenario', () => {
  let app;
  let sequelize;
  let redis;
  let mongoose;
  let templateProvider: TemplateProvider;
  let redlock;
  let scheduledMessagesRepository: MessageSchedulerRepository;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    sequelize = moduleFixture.get('SequelizeToken');
    templateProvider = moduleFixture.get(TemplateProvider);
    redis = moduleFixture.get('RedisToken');
    mongoose = moduleFixture.get('MongooseToken');
    redlock = moduleFixture.get('RedlockToken');
    scheduledMessagesRepository = moduleFixture.get(MessageSchedulerRepository);

    await app.init();
    await sequelize.sync({force: true});
    await redis.flushall();
  });

  afterEach(async () => {
    await app.close();
    await sequelize.close();
    await redis.disconnect();
    await redlock.quit();
    await mongoose.connection.close();

    clearDatabase();
  });

  it('should send me a list of tags I am subscribed to sorted alphabetically', async () => {
    await request(app.getHttpServer())
      .post('/api/webhooks/main')
      .send(startWebhook)
      .expect(200);
    await request(app.getHttpServer())
      .post('/api/webhooks/main')
      .send(javascriptTagWebhook)
      .expect(200);
    await request(app.getHttpServer())
      .post('/api/webhooks/main')
      .send(javaTagWebhook)
      .expect(200);
    await request(app.getHttpServer())
      .post('/api/webhooks/main')
      .send(nodeJsTagWebhook)
      .expect(200);
    await request(app.getHttpServer())
      .post('/api/webhooks/main')
      .send(v8TagWebhook)
      .expect(200);
    await request(app.getHttpServer())
      .post('/api/webhooks/main')
      .send(jvmTagWebhook)
      .expect(200);
    await request(app.getHttpServer())
      .post('/api/webhooks/main')
      .send(mySubscriptionsWebhook)
      .expect(200);

    const scheduled = await scheduledMessagesRepository.getMessages(30);
    const lastMessage = _.last(scheduled);

    expect(lastMessage.scheduledMessagePack.scheduledMessage.content).toEqual(await templateProvider.build(templates.MY_SUBSCRIPTIONS, {
      tags: [
        {name: javaTagWebhook.message.text}, {name: javascriptTagWebhook.message.text}, {name: jvmTagWebhook.message.text},
        {name: nodeJsTagWebhook.message.text}, {name: v8TagWebhook.message.text},
      ],
      total: 5,
    }));
  }, 60000);
});
