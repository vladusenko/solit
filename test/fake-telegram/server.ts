import * as express from 'express';
import * as jsonschema from 'jsonschema';
import * as bodyParser from 'body-parser';

function log(message) {
  console.log(`[FAKE TELEGRAM] ${message}`); // tslint:disable-line no-console
}

interface Message {
  text: string;
  sentAt: Date;
}

const validator = new jsonschema.Validator();

const sendMessageSchema = {
  id: '/sendMessageSchema',
  type: 'object',
  properties: {
    chat_id: {type: 'integer', minimum: 0},
    text: {type: 'string', minLength: 1},
    parse_mode: {type: 'string', enum: ['Markdown', 'HTML']},
  },
};

let server;
const app = express();
let db = new Map<number, Message[]>();
app.use(bodyParser.json());

app.post('/:botToken/sendMessage', (req, res) => {
  const errors = validator.validate(req.body, sendMessageSchema);
  if (errors.errors.length !== 0) {
    return res.status(422).json(errors);
  }
  const history = db.get(req.body.chat_id) || [];
  history.push({text: req.body.text, sentAt: new Date()});
  db.set(req.body.chat_id, history);
  return res.status(200).end();
});

app.get('/:chatId/messages', (req, res) => {
  const chatId = Number.parseInt(req.params.chatId, 10);
  const history = db.get(chatId);
  return res.status(200).json(history);
});

export function startFakeTelegram(port: number) {
  server = app.listen(port, () => log(`Listening on port ${port}`));
  return server;
}

export function clearDatabase() {
  db = new Map();
}

export function stopFakeTelegram() {
  server.close();
}
