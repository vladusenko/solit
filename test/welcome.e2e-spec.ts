import {MessageSchedulerRepository} from '../src/app/repositories/message-scheduler.repository';

process.env.NODE_ENV = process.env.NODE_ENV.toUpperCase();

import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../src/app.module';
import Axios from 'axios';
import { startFakeTelegram, stopFakeTelegram, clearDatabase } from './fake-telegram/server';
import * as request from 'supertest';
import { TemplateProvider, templates } from '../src/app/template-provider';
import {MAIN_MENU} from '../src/app/dictionaries/main-menu';
import * as _ from 'lodash';

const startWebhook = {
  update_id: 123456,
  message: {
    message_id: 123456,
    from: {
      id: 123456,
      is_bot: false,
      first_name: 'FirstName',
      last_name: 'LastName',
      username: 'Username',
      language_code: 'en',
    },
    date: 1562615224,
    chat: {
      id: 123456,
      type: 'private',
      username: 'Username',
    },
    text: '/start',
  },
};

const javascriptTagWebhook = {
  update_id: 123456,
  message: {
    message_id: 123456,
    from: {
      id: 123456,
      is_bot: false,
      first_name: 'FirstName',
      last_name: 'LastName',
      username: 'Username',
      language_code: 'en',
    },
    date: 1562615224,
    chat: {
      id: 123456,
      type: 'private',
      username: 'Username',
    },
    text: 'javascript',
  },
};

const javaTagWebhook = {
  update_id: 123456,
  message: {
    message_id: 123456,
    from: {
      id: 123456,
      is_bot: false,
      first_name: 'FirstName',
      last_name: 'LastName',
      username: 'Username',
      language_code: 'en',
    },
    date: 1562615224,
    chat: {
      id: 123456,
      type: 'private',
      username: 'Username',
    },
    text: 'java',
  },
};

const nodeJsTagWebhook = {
  update_id: 123456,
  message: {
    message_id: 123456,
    from: {
      id: 123456,
      is_bot: false,
      first_name: 'FirstName',
      last_name: 'LastName',
      username: 'Username',
      language_code: 'en',
    },
    date: 1562615224,
    chat: {
      id: 123456,
      type: 'private',
      username: 'Username',
    },
    text: 'node.js',
  },
};

const v8TagWebhook = {
  update_id: 123456,
  message: {
    message_id: 123456,
    from: {
      id: 123456,
      is_bot: false,
      first_name: 'FirstName',
      last_name: 'LastName',
      username: 'Username',
      language_code: 'en',
    },
    date: 1562615224,
    chat: {
      id: 123456,
      type: 'private',
      username: 'Username',
    },
    text: 'v8',
  },
};

const jvmTagWebhook = {
  update_id: 123456,
  message: {
    message_id: 123456,
    from: {
      id: 123456,
      is_bot: false,
      first_name: 'FirstName',
      last_name: 'LastName',
      username: 'Username',
      language_code: 'en',
    },
    date: 1562615224,
    chat: {
      id: 123456,
      type: 'private',
      username: 'Username',
    },
    text: 'jvm',
  },
};

const spacesToDotsTagWebhook = {
  update_id: 123456,
  message: {
    message_id: 123456,
    from: {
      id: 123456,
      is_bot: false,
      first_name: 'FirstName',
      last_name: 'LastName',
      username: 'Username',
      language_code: 'en',
    },
    date: 1562615224,
    chat: {
      id: 123456,
      type: 'private',
      username: 'Username',
    },
    text: 'Node Js',
  },
};

const spacesToDashesTagWebhook = {
  update_id: 123456,
  message: {
    message_id: 123456,
    from: {
      id: 123456,
      is_bot: false,
      first_name: 'FirstName',
      last_name: 'LastName',
      username: 'Username',
      language_code: 'en',
    },
    date: 1562615224,
    chat: {
      id: 123456,
      type: 'private',
      username: 'Username',
    },
    text: 'ruby on rails',
  },
};

const invalidTagWebhook = {
  update_id: 123456,
  message: {
    message_id: 123456,
    from: {
      id: 123456,
      is_bot: false,
      first_name: 'FirstName',
      last_name: 'LastName',
      username: 'Username',
      language_code: 'en',
    },
    date: 1562615224,
    chat: {
      id: 123456,
      type: 'private',
      username: 'Username',
    },
    text: 'invalidtag',
  },
};

describe('Welcome e2e scenario', () => {
  let app;
  let sequelize;
  let redis;
  let mongoose;
  let templateProvider: TemplateProvider;
  let appAxios;
  let fakeTelegramServer;
  let redlock;
  let scheduledMessagesRepository: MessageSchedulerRepository;

  beforeAll(() => {
    fakeTelegramServer = startFakeTelegram(54321);
  });

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    sequelize = moduleFixture.get('SequelizeToken');
    templateProvider = moduleFixture.get(TemplateProvider);
    redis = moduleFixture.get('RedisToken');
    mongoose = moduleFixture.get('MongooseToken');
    redlock = moduleFixture.get('RedlockToken');
    scheduledMessagesRepository = moduleFixture.get(MessageSchedulerRepository);

    await app.init();
    await sequelize.sync({force: true});
    await redis.flushall();

    appAxios = Axios.create({
      baseURL: 'localhost:3000',
    });
  });

  afterEach(async () => {
    await app.close();
    await sequelize.close();
    await redis.disconnect();
    await redlock.quit();
    await mongoose.connection.close();

    clearDatabase();
  });

  afterAll(() => {
    stopFakeTelegram();
  });

  it('should successfully welcome user, enable 5 tags and make some suggestions', async () => {
    await request(app.getHttpServer())
      .post('/api/webhooks/main')
      .send(startWebhook)
      .expect(200);
    await request(app.getHttpServer())
      .post('/api/webhooks/main')
      .send(javascriptTagWebhook)
      .expect(200);
    await request(app.getHttpServer())
      .post('/api/webhooks/main')
      .send(javaTagWebhook)
      .expect(200);
    await request(app.getHttpServer())
      .post('/api/webhooks/main')
      .send(nodeJsTagWebhook)
      .expect(200);
    await request(app.getHttpServer())
      .post('/api/webhooks/main')
      .send(v8TagWebhook)
      .expect(200);
    await request(app.getHttpServer())
      .post('/api/webhooks/main')
      .send(jvmTagWebhook)
      .expect(200);

    const scheduled = await scheduledMessagesRepository.getMessages(30);
    const history = scheduled.map((item) => item.scheduledMessagePack.scheduledMessage.content);
    expect(scheduled[1].scheduledMessagePack.scheduledMessage.content).toEqual(
      await templateProvider.build(templates.ADDED_TAG_WALKTHROUGH, {
        tag: javascriptTagWebhook.message.text,
        remainingCount: 4,
        suggestion: true,
      }),
    );
    expect(scheduled[1].scheduledMessagePack.scheduledMessage.customKeyboard).toBeInstanceOf(Array);
    expect(history[2]).toEqual(
      await templateProvider.build(templates.ADDED_TAG_WALKTHROUGH, {tag: javaTagWebhook.message.text, remainingCount: 3}),
    );
    expect(scheduled[2].scheduledMessagePack.scheduledMessage.customKeyboard).toBeInstanceOf(Array);
    expect(history[3]).toEqual(
      await templateProvider.build(templates.ADDED_TAG_WALKTHROUGH, {tag: nodeJsTagWebhook.message.text, remainingCount: 2}),
    );
    expect(scheduled[3].scheduledMessagePack.scheduledMessage.customKeyboard).toBeInstanceOf(Array);
    expect(history[4]).toEqual(
      await templateProvider.build(templates.ADDED_TAG_WALKTHROUGH, {tag: v8TagWebhook.message.text, remainingCount: 1}),
    );
    expect(scheduled[4].scheduledMessagePack.scheduledMessage.customKeyboard).toBeInstanceOf(Array);
    expect(history[5]).toEqual(
      await templateProvider.build(templates.AFTER_WALKTHROUGH, {tag: jvmTagWebhook.message.text}),
    );
    expect(scheduled[5].scheduledMessagePack.scheduledMessage.customKeyboard).toEqual(_.chunk(_.values(MAIN_MENU), 3));
    expect(history[6].includes('📥 Yo! Here\'s the lit list of questions for you:'));
  }, 45000);

  it('should let user know about invalid tag', async () => {
    await request(app.getHttpServer())
      .post('/api/webhooks/main')
      .send(startWebhook)
      .expect(200);
    await request(app.getHttpServer())
      .post('/api/webhooks/main')
      .send(invalidTagWebhook)
      .expect(200);

    const scheduled = await scheduledMessagesRepository.getMessages(30);
    const history = scheduled.map((item) => item.scheduledMessagePack.scheduledMessage.content);

    expect(history).toEqual([
      await templateProvider.build(templates.WELCOME, {}),
      await templateProvider.build(templates.TAG_NOT_FOUND, {tag: invalidTagWebhook.message.text}),
    ]);
  });

  it('should convert "node js" to "node.js"', async () => {
    await request(app.getHttpServer())
      .post('/api/webhooks/main')
      .send(startWebhook)
      .expect(200);
    await request(app.getHttpServer())
      .post('/api/webhooks/main')
      .send(spacesToDotsTagWebhook)
      .expect(200);

    const scheduled = await scheduledMessagesRepository.getMessages(30);
    const history = scheduled.map((item) => item.scheduledMessagePack.scheduledMessage.content);

    expect(history).toEqual([
      await templateProvider.build(templates.WELCOME, {}),
      await templateProvider.build(templates.ADDED_TAG_WALKTHROUGH, {tag: 'node.js', remainingCount: 4, suggestion: true}),
    ]);
  }, 10000);

  it('should convert "ruby on rails" to "ruby-on-rails"', async () => {
    await request(app.getHttpServer())
      .post('/api/webhooks/main')
      .send(startWebhook)
      .expect(200);
    await request(app.getHttpServer())
      .post('/api/webhooks/main')
      .send(spacesToDashesTagWebhook)
      .expect(200);

    const scheduled = await scheduledMessagesRepository.getMessages(30);
    const history = scheduled.map((item) => item.scheduledMessagePack.scheduledMessage.content);

    expect(history).toEqual([
      await templateProvider.build(templates.WELCOME, {}),
      await templateProvider.build(templates.ADDED_TAG_WALKTHROUGH, {tag: 'ruby-on-rails', remainingCount: 4, suggestion: true}),
    ]);
  }, 10000);

  it('should not let user add two equal tags', async () => {
    await request(app.getHttpServer())
      .post('/api/webhooks/main')
      .send(startWebhook)
      .expect(200);
    await request(app.getHttpServer())
      .post('/api/webhooks/main')
      .send(javascriptTagWebhook)
      .expect(200);
    await request(app.getHttpServer())
      .post('/api/webhooks/main')
      .send(javascriptTagWebhook)
      .expect(200);

    const scheduled = await scheduledMessagesRepository.getMessages(30);
    const history = scheduled.map((item) => item.scheduledMessagePack.scheduledMessage.content);

    expect(history).toEqual([
      await templateProvider.build(templates.WELCOME, {}),
      await templateProvider.build(templates.ADDED_TAG_WALKTHROUGH, {tag: javascriptTagWebhook.message.text, remainingCount: 4, suggestion: true}),
      await templateProvider.build(templates.TAG_ALREADY_ADDED, {tag: javascriptTagWebhook.message.text}),
    ]);
  });
});
