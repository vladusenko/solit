#!/usr/bin/env bash

password=$1
tag=$2
jwt_secret=$3
pg_host=$4
pg_port=$5
pg_user=$6
pg_password=$7
pg_database=$8

if [[ -z ${password} ]]
then
    echo "Error: password can not be empty!"
    exit 1
fi

if [[ -z ${tag} ]]
then
    echo "Error: tag can not be empty!"
    exit 1
fi

if [[ -z ${jwt_secret} ]]
then
    echo "Error: jwt secret can not be empty!"
    exit 1
fi

echo ${password} | docker login --username vladyslav48 --password-stdin
docker pull vladyslav48/solit:${tag}
docker run vladyslav48/solit:${tag} \
    --detach \
    --env JWT_SECRET=${jwt_secret} \
    --env PG_HOST=${pg_host} \
    --env PG_PORT=${pg_port} \
    --env PG_USER=${pg_user} \
    --env PG_PASSWORD=${pg_password} \
    --env PG_DATABASE=${pg_database} \
    --name solit:${tag}
    -p 3000:3000
    node main.js
docker logs -f solit:${tag} > output.log
