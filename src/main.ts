import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger } from 'nestjs-pino/dist';
import { createTerminus } from '@godaddy/terminus';

async function bootstrap() {
  process.env.NODE_ENV = process.env.NODE_ENV || 'DEV';
  const app = await NestFactory.create(AppModule, {logger: false});
  app.useLogger(app.get(Logger));
  const sequelize = app.get('SequelizeToken');
  const mongoose = app.get('MongooseToken');
  const redis = app.get('RedisToken');
  const httpServer = app.getHttpServer();
  createTerminus(httpServer, {
    healthChecks: {
      '/system/health': async () => {
        let isSequelizeConnected: boolean;
        let isMongooseConnected: boolean;
        let isRedisConnected: boolean;

        try {
          await sequelize.query('SELECT 1+1 as result', {logging: false});
          isSequelizeConnected = true;
        } catch (err) {
          isSequelizeConnected = false;
        }

        isMongooseConnected = mongoose.connection.readyState === 1;
        isRedisConnected = redis.status === 'connected';

        return isSequelizeConnected && isMongooseConnected && isRedisConnected;
      },
    },
  });
  await app.listen(process.env.PORT || 3000);
}
bootstrap();
