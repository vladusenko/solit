import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { DatabaseModule } from './db/database.module';
import { AppVariablesModule } from './variables/app-variables.module';
import { WebhookController } from './app/controllers/webhook/webhook.controller';
import { CommandHandlersRegistry } from './app/command-handlers-registry';
import { ChatRepository } from './app/repositories/models/chat.repository';
import { CommandRouter } from './app/command-router';
import { TelegramClient } from './app/services/telegram-client.service';
import { MessageRouter } from './app/message-router';
import { ChatStateRepository } from './app/repositories/states/chat-state.repository';
import { MessageHandlersRegistry } from './app/message-handlers-registry';
import { SeClient } from './app/se-client';
import { TagsStateRepository } from './app/repositories/states/tags-state.repository';
import { PreferenceRepository } from './app/repositories/models/preference.repository';
import { TemplateProvider } from './app/template-provider';
import { ipFilterMiddleware } from './middleware/ip-filter.middleware';
import { FetcherCron } from './app/crons/fetcher.cron';
import { QuestionRepository } from './app/repositories/models/question.repository';
import * as cron from 'node-cron';
import { FetcherCronStateRepository } from './app/repositories/crons/fetcher-cron-state.repository';
import {UpdaterCron} from './app/crons/updater.cron';
import {UpdaterCronStateRepository} from './app/repositories/crons/updater-cron-state.repository';
import {ChunkProcessor} from './app/crons/chunk-processor';
import {ReaperCron} from './app/crons/reaper.cron';
import {ReaperCronStateRepository} from './app/repositories/crons/reaper-cron.state.repository';
import {MessageScheduler} from './app/message-scheduler/message-scheduler';
import {MessageSchedulerRepository} from './app/repositories/message-scheduler.repository';
import {MailmanCronStateRepository} from './app/repositories/crons/mailman-cron-state.repository';
import {MailmanCron} from './app/crons/mailman.cron';
import {MessageRepository} from './app/repositories/models/message.repository';
import {TagsService} from './app/services/tags.service';
import {MenuMessageHandler} from './app/message-handlers/menu.message-handler';
import {AnswerRepository} from './app/repositories/models/answer.repository';
import {MessageService} from './app/services/message.service';
import {LinkRepository} from './app/repositories/models/link.repository';
import {AnalyticsController} from './app/controllers/analytics/analytics.controller';
import {AnalyticsService} from './app/services/analytics.service';
import {LinkService} from './app/services/link.service';
import {ReaperCronController} from './app/controllers/internal/reaper-cron.controller';
import {UpdaterCronController} from './app/controllers/internal/updater-cron.controller';
import {FetcherCronController} from './app/controllers/internal/fetcher-cron.controller';
import { LoggerModule } from 'nestjs-pino';
import {PinoLogger} from 'nestjs-pino/dist';

@Module({
  imports: [
    AppVariablesModule,
    DatabaseModule,
    LoggerModule.forRoot({
      redact: {
        paths: ['req.headers["x-secret-key"]'],
        censor: '*** SECRET ***',
      },
    }),
  ],
  controllers: [
    WebhookController,
    AnalyticsController,
    ReaperCronController,
    UpdaterCronController,
    FetcherCronController,
  ],
  providers: [
    CommandRouter,
    CommandHandlersRegistry,
    ChatRepository,
    ChatStateRepository,
    TelegramClient,
    MessageRouter,
    MessageHandlersRegistry,
    SeClient,
    TagsStateRepository,
    PreferenceRepository,
    TemplateProvider,
    FetcherCron,
    QuestionRepository,
    FetcherCronStateRepository,
    UpdaterCron,
    UpdaterCronStateRepository,
    ChunkProcessor,
    ReaperCron,
    ReaperCronStateRepository,
    ReaperCron,
    MessageScheduler,
    MessageSchedulerRepository,
    MailmanCronStateRepository,
    {
      provide: MailmanCron,
      useFactory: (
        redlock,
        messageSchedulerRepository: MessageSchedulerRepository,
        mailmanCronStateRepository: MailmanCronStateRepository,
        messageSender: TelegramClient,
        messageRepository: MessageRepository,
        logger: PinoLogger,
      ) => {
        logger.setContext(MailmanCron.name);
        const mailmanCron = new MailmanCron(
          redlock,
          messageSchedulerRepository,
          mailmanCronStateRepository,
          messageSender,
          messageRepository,
          logger,
        );

        if (process.env.RUN_CRONS) {
          cron.schedule('*/1 * * * * *', () => mailmanCron.run());
        }

        return mailmanCron;
      },
      inject: [
        'RedlockToken', MessageSchedulerRepository, MailmanCronStateRepository, TelegramClient, MessageRepository, PinoLogger,
      ],
    },
    MessageRepository,
    TagsService,
    MenuMessageHandler,
    AnswerRepository,
    MessageService,
    LinkRepository,
    AnalyticsService,
    LinkService,
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer): void {
    consumer
      .apply(ipFilterMiddleware([process.env.TG_FIRST_CIDR, process.env.TG_SECOND_CIDR]))
      .forRoutes(WebhookController);
  }
}
