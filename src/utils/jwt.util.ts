import * as jsonwebtoken from 'jsonwebtoken';

export interface Jwt {
    accessToken: string;
    expiresIn: number;
}

export class JwtUtil {
    public static generate(data: object, expiresIn: number, secret: string): Promise<Jwt> {
        return new Promise((resolve, reject) => {
            jsonwebtoken.sign(data, secret, {expiresIn}, (err, token) => {
                if (err) {
                    return reject(err);
                } else {
                    resolve({
                        accessToken: token,
                        expiresIn,
                    });
                }
            });
        });
    }
}
