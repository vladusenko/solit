import * as moment from 'moment';

export class DateUtil {
  public static now() {
    return new Date();
  }

  public static currentEpoch() {
    return new Date().getTime() / 1000;
  }

  public static oneDayBack() {
    return moment().subtract(24, 'hours').toDate();
  }

  public static twoDaysBack() {
    return moment().subtract(48, 'hours').toDate();
  }
}
