const allSpacesRegex = / +/g;

export function formatTagWithDashes(incoming: string) {
  return incoming.toLowerCase().replace(allSpacesRegex, '-');
}

export function formatTagWithDots(incoming: string) {
  return incoming.toLowerCase().replace(allSpacesRegex, '.');
}

export function wait(ms: number) {
  return new Promise((resolve => setTimeout(resolve, ms)));
}
