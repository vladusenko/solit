import * as uuid from 'node-uuid';

export class UuidUtil {
    public static v4(): string {
        return uuid.v4();
    }
}
