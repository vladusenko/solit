import * as _ from 'lodash';

export function looseArrayDifference(bigInts: number[], numbers: number[]) {
  return _.differenceWith(bigInts, numbers, (bigInt, numeric) => numeric === bigInt);
}
