import { parseWebhook, TelegramWebhook } from './tg-webhook-update-parser';
import { UnsupportedWebhookError } from './errors/unsupported-webhook.error';
import { UnsupportedChatTypeError } from './errors/unsupported-chat-type.error';
import { UnsupportedMessageTypeError } from './errors/unsupported-message-type.error';

const validWebhook: TelegramWebhook = {
  update_id: 123456,
  message: {
    message_id: 123456,
    from: {
      id: 123456,
      is_bot: false,
      first_name: 'First Name',
      last_name: 'Last Name',
      username: 'Username',
      language_code: 'en',
    },
    date: 1562615224,
    chat: {
      id: 123456,
      type: 'private',
      username: 'Username',
      first_name: 'First Name',
      last_name: 'Last Name',
    },
    text: '/start',
  },
};

const commandWithArgumentsWebhook: TelegramWebhook = {
  update_id: 123456,
  message: {
    message_id: 123456,
    from: {
      id: 123456,
      is_bot: false,
      first_name: 'First Name',
      last_name: 'Last Name',
      username: 'Username',
      language_code: 'en',
    },
    date: 1562615224,
    chat: {
      id: 123456,
      type: 'private',
      username: 'Username',
      first_name: 'First Name',
      last_name: 'Last Name',
    },
    text: '/start first second',
  },
};

const noMessageWebhook: TelegramWebhook = {
  update_id: 123456,
};

const nonPrivateChatWebhook: TelegramWebhook = {
  update_id: 123456,
  message: {
    message_id: 123456,
    from: {
      id: 123456,
      is_bot: false,
      first_name: 'First Name',
      last_name: 'Last Name',
      username: 'Username',
      language_code: 'en',
    },
    date: 1562615224,
    chat: {
      id: 123456,
      type: 'channel',
      username: 'Username',
      first_name: 'First Name',
      last_name: 'Last Name',
    },
    text: '/start first second',
  },
};

const nonTextBasedMessageWebhook: TelegramWebhook = {
  update_id: 123456,
  message: {
    message_id: 123456,
    from: {
      id: 123456,
      is_bot: false,
      first_name: 'First Name',
      last_name: 'Last Name',
      username: 'Username',
      language_code: 'en',
    },
    date: 1562615224,
    chat: {
      id: 123456,
      type: 'private',
      username: 'Username',
      first_name: 'First Name',
      last_name: 'Last Name',
    },
  },
};

const notACommandMessageTextWebhook: TelegramWebhook = {
  update_id: 123456,
  message: {
    message_id: 123456,
    from: {
      id: 123456,
      is_bot: false,
      first_name: 'First Name',
      last_name: 'Last Name',
      username: 'Username',
      language_code: 'en',
    },
    date: 1562615224,
    chat: {
      id: 123456,
      type: 'private',
      username: 'Username',
      first_name: 'First Name',
      last_name: 'Last Name',
    },
    text: 'not a command',
  },
};

describe('TG message parsers unit tests', () => {
  it('should successfully parse webhook', () => {
    const tgUpdate = parseWebhook(validWebhook);
    expect(tgUpdate).toEqual({
      updateId: validWebhook.update_id,
      chatId: validWebhook.message.chat.id,
      originalText: validWebhook.message.text,
      dateSent: commandWithArgumentsWebhook.message.date,
      command: '/start',
      arguments: null,
    });
  });

  it('should successfully parse webhook with arguments', () => {
    const tgUpdate = parseWebhook(commandWithArgumentsWebhook);
    expect(tgUpdate).toEqual({
      updateId: commandWithArgumentsWebhook.update_id,
      chatId: commandWithArgumentsWebhook.message.chat.id,
      originalText: commandWithArgumentsWebhook.message.text,
      dateSent: commandWithArgumentsWebhook.message.date,
      command: '/start',
      arguments: ['first', 'second'],
    });
  });

  it('should successfully parse webhook with simple message', () => {
    const tgUpdate = parseWebhook(notACommandMessageTextWebhook);

    expect(tgUpdate).toEqual({
      updateId: notACommandMessageTextWebhook.update_id,
      chatId: notACommandMessageTextWebhook.message.chat.id,
      originalText: notACommandMessageTextWebhook.message.text,
      dateSent: notACommandMessageTextWebhook.message.date,
      command: null,
      arguments: null,
    });
  });

  it('should throw UnsupportedWebhookError in case no message in webhook payload', () => {
    let error;

    try {
      parseWebhook(noMessageWebhook);
    } catch (err) {
      error = err;
    }

    expect(error).toBeInstanceOf(UnsupportedWebhookError);
  });

  it('should throw UnsupportedChatTypeError in case chat is not private', () => {
    let error;

    try {
      parseWebhook(nonPrivateChatWebhook);
    } catch (err) {
      error = err;
    }

    expect(error).toBeInstanceOf(UnsupportedChatTypeError);
  });

  it('should throw UnsupportedMessageTypeError in case message is not text based', () => {
    let error;

    try {
      parseWebhook(nonTextBasedMessageWebhook);
    } catch (err) {
      error = err;
    }

    expect(error).toBeInstanceOf(UnsupportedMessageTypeError);
  });
});
