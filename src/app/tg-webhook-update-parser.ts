import { UnsupportedWebhookError } from './errors/unsupported-webhook.error';
import { UnsupportedChatTypeError } from './errors/unsupported-chat-type.error';
import { UnsupportedMessageTypeError } from './errors/unsupported-message-type.error';
import * as _ from 'lodash';

export interface TelegramWebhook {
  update_id: number;
  message?: {
    message_id: number;
    from: {
      id: number;
      is_bot: boolean;
      first_name: string;
      last_name: string;
      username: string;
      language_code?: string;
    };
    date: number;
    chat: {
      id: number;
      type: string;
      username?: string;
      first_name?: string;
      last_name?: string;
    };
    text?: string;
  };
}

export interface TelegramUpdate {
  updateId: number;
  chatId: number;
  originalText: string;
  dateSent: number;
  command?: string;
  arguments?: string[];
}

export function parseWebhook(payload: TelegramWebhook): TelegramUpdate {
  if (!payload.message) {
    throw new UnsupportedWebhookError('Received not "message" update type');
  }

  if (payload.message.chat.type !== 'private') {
    throw new UnsupportedChatTypeError('Received non "private" chat type');
  }

  if (!payload.message.text) {
    throw new UnsupportedMessageTypeError('Received non "text" based message');
  }

  let parsedCommand: {command: string, arguments?: string[]};

  if (payload.message.text.charAt(0) === '/') {
    parsedCommand = parseCommand(payload.message.text);
  }

  const chatId = payload.message.chat.id;
  const updateId = payload.update_id;

  return {
    updateId,
    chatId,
    command: _.get(parsedCommand, 'command', null),
    arguments: _.get(parsedCommand, 'arguments', null),
    originalText: payload.message.text,
    dateSent: payload.message.date,
  };
}

function parseCommand(commandString: string): {command: string; arguments?: string[]} {
  const split = commandString.split(' ');
  const command = split[0];
  split.shift();
  if (split.length > 0) {
    return {
      command,
      arguments: split,
    };
  } else {
    return {
      command,
    };
  }
}
