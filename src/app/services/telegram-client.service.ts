import { Inject, Injectable } from '@nestjs/common';
import Axios from 'axios';
import { AxiosInstance } from 'axios';
import { MessageSendingError } from '../errors/message-sending.error';
import {MessageUpdateError} from '../errors/message-update.error';
import {InjectPinoLogger, PinoLogger} from 'nestjs-pino/dist';

interface KeyboardButton {
  text: string;
  request_contact?: boolean;
  request_location?: boolean;
}

interface ReplyKeyboardMarkup {
  keyboard: KeyboardButton[][];
  resize_keyboard?: boolean;
  one_time_keyboard?: boolean;
  selective?: boolean;
}

interface SendMessageBody {
  chat_id: number;
  text: string;
  parse_mode: 'HTML' | 'Markdown';
  reply_markup?: ReplyKeyboardMarkup;
  disable_web_page_preview: boolean;
}

interface UpdateMessageBody extends SendMessageBody {
  message_id: number;
}

@Injectable()
export class TelegramClient {
  private readonly axios: AxiosInstance;

  constructor(
    @Inject('TELEGRAM_TOKEN') private readonly telegramToken: string,
    @Inject('TELEGRAM_API_HOST') private readonly telegramHost: string,
    @InjectPinoLogger(TelegramClient.name) private readonly logger: PinoLogger,
  ) {
    this.axios = Axios.create({
      baseURL: `${telegramHost}/bot${this.telegramToken}`,
    });
  }

  public async sendMessage(content: string, chatId: number, customKeyboard?: string[][]): Promise<number> {
    try {
      const body: SendMessageBody = {
        chat_id: chatId,
        text: content,
        parse_mode: 'HTML',
        disable_web_page_preview: true,
      };

      if (customKeyboard && customKeyboard.length > 0) {
        const keyboard: KeyboardButton[][] = [];

        for (const row of customKeyboard) {
          const keyboardButtonRow: KeyboardButton[] = [];
          for (const item of row) {
            keyboardButtonRow.push({text: item});
          }
          keyboard.push(keyboardButtonRow);
        }

        body.reply_markup = {
          keyboard,
          resize_keyboard: true,
          one_time_keyboard: true,
        };
      }
      const { data } = await this.axios.post('/sendMessage', body);
      return data.result.message_id as number;
    } catch (err) {
      this.logger.error(err);
      throw new MessageSendingError(`Failed to send message to chat ${chatId}: ${err.message}`);
    }
  }

  public async updateMessageByTelegramId(id: number, chatId: number, content: string): Promise<void> {
    try {
      const body: UpdateMessageBody = {
        chat_id: chatId,
        message_id: id,
        text: content,
        parse_mode: 'HTML',
        disable_web_page_preview: true,
      };

      await this.axios.post('/editMessageText', body);
    } catch (err) {
      this.logger.error(err);
      throw new MessageUpdateError(`Failed to send message to chat ${chatId}: ${err.message}`);
    }
  }
}
