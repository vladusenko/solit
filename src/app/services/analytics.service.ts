import {LinkRepository} from '../repositories/models/link.repository';
import {Injectable} from '@nestjs/common';

@Injectable()
export class AnalyticsService {
  constructor(
    private readonly linkRepository: LinkRepository,
  ) {}

  public async incrementClickCount(linkId: string) {
    await this.linkRepository.incrementClickCountById(linkId);
  }
}
