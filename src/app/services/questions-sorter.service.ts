import {Question} from '../repositories/models/question.repository';

interface SortedQuestion {
  questionId: number;
  title: string;
  link: string;
  tags: string[];
}

export interface QuestionsSorterModule {
  topWithAnswers: (questions: Array<Partial<Question>>, amount: number) => SortedQuestion[];
  topWithoutAnswers: (questions: Array<Partial<Question>>, amount: number) => SortedQuestion[];
}

const IMPACT_COEFFICIENTS = Object.freeze({
  VOTES: 2,
  ANSWERS: 1,
  VIEWS: 1,
});

function sort(questions: Array<Partial<Question>>) {
  return questions.sort((questionA, questionB) => {
    const questionATotal = questionA.score * IMPACT_COEFFICIENTS.VOTES
      + questionA.answer_count * IMPACT_COEFFICIENTS.ANSWERS
      + questionA.view_count * IMPACT_COEFFICIENTS.VIEWS;
    const questionBTotal = questionB.score * IMPACT_COEFFICIENTS.VOTES
      + questionB.answer_count * IMPACT_COEFFICIENTS.ANSWERS
      + questionB.view_count * IMPACT_COEFFICIENTS.VIEWS;

    if (questionATotal < questionBTotal) {
      return 1;
    }

    if (questionATotal > questionBTotal) {
      return -1;
    }

    return 0;
  });
}

export const questionsSorterModule = {
  topWithAnswers: (questions: Array<Partial<Question>>, amount: number) => {
    const filtered = questions.filter((question) => question.is_answered);
    const sorted = sort(filtered);
    return sorted.map((item) => ({
      questionId: item.question_id,
      title: item.title,
      link: item.link,
      tags: item.tags,
    })).slice(0, amount);
  },
  topWithoutAnswers: (questions: Array<Partial<Question>>, amount: number) => {
    const filtered = questions.filter((question) => !question.is_answered);
    const sorted = sort(filtered);
    return sorted.map((item) => ({
      questionId: item.question_id,
      title: item.title,
      link: item.link,
      tags: item.tags,
    })).slice(0, amount);
  },
};
