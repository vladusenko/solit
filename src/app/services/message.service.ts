import {Inject, Injectable} from '@nestjs/common';
import {Message, MessageRepository} from '../repositories/models/message.repository';
import {Link, LinkRepository} from '../repositories/models/link.repository';
import {Sequelize} from 'sequelize';
import {UuidUtil} from '../../utils/uuid.util';
import {TelegramClient} from './telegram-client.service';
import {TemplateProvider, templates} from '../template-provider';
import * as _ from 'lodash';
import * as camelCase from 'camelcase';

export interface LinksByType {
  answeredQuestions: Array<{id: string; address: string; tags: string[]; title: string}>;
  nonAnsweredQuestions: Array<{id: string; address: string; tags: string[]; title: string}>;
  guruAnswers: Array<{id: string; address: string; tags: string[]; title: string}>;
}

@Injectable()
export class MessageService {
  constructor(
    private readonly messageRepository: MessageRepository,
    private readonly linkRepository: LinkRepository,
    private readonly telegramClient: TelegramClient,
    private readonly templateProvider: TemplateProvider,
    @Inject('SequelizeToken') private readonly sequelize: Sequelize,
    @Inject('ANALYTICS_BASE_URL') private readonly analyticsBaseUrl: string,
  ) {}

  public createLitListMessage(message: Partial<Message>, linksByType: LinksByType): Promise<string> {
    return this.sequelize.transaction<string>(async (transactionRef) => {
      const storedMessage = await this.messageRepository.create({...message, status: 'scheduled', id: UuidUtil.v4()}, transactionRef);
      const links: Array<Partial<Link>> = [];
      linksByType.guruAnswers.forEach((link) => {
        links.push({
          id: link.id,
          messageId: storedMessage.privateId,
          category: 'guru_answer',
          address: link.address,
          title: link.title,
          tags: link.tags,
          clickCount: 0,
        });
      });
      linksByType.answeredQuestions.forEach((link) => {
        links.push({
          id: link.id,
          messageId: storedMessage.privateId,
          category: 'answered_question',
          address: link.address,
          title: link.title,
          tags: link.tags,
          clickCount: 0,
        });
      });
      linksByType.nonAnsweredQuestions.forEach((link) => {
        links.push({
          id: link.id,
          messageId: storedMessage.privateId,
          category: 'non_answered_question',
          address: link.address,
          title: link.title,
          tags: link.tags,
          clickCount: 0,
        });
      });
      await this.linkRepository.bulkCreate(links, transactionRef);

      return storedMessage.id;
    });
  }

  public async createMessage(messageData: Partial<Message>): Promise<string> {
    const message = await this.messageRepository.create({...messageData, status: 'scheduled', id: UuidUtil.v4()});
    return message.id;
  }

  public async updateMessageViewByLinkId(linkId: string): Promise<void> {
    const messageFullInfo = await this.messageRepository.getFullMessageInfoByLinkId(linkId);
    const currentLink = messageFullInfo.links.find((link) => link.id === linkId);
    if (currentLink.clickCount > 1) { // means that the checkmark is already rendered
      return;
    }

    const linksByType = _.groupBy(messageFullInfo.links, (link) => `${camelCase(link.category)}Links`);
    const renderableAnsweredQuestionLinks = (_.get(linksByType, 'answeredQuestionLinks', []) as Link[]).map((link) => ({
      tags: link.tags,
      title: link.title,
      analytics_link: `${this.analyticsBaseUrl}/${link.id}`,
      clicked: link.clickCount > 0,
    }));
    const renderableNonAnsweredQuestionLinks = (_.get(linksByType, 'nonAnsweredQuestionLinks', []) as Link[]).map((link) => ({
      tags: link.tags,
      title: link.title,
      analytics_link: `${this.analyticsBaseUrl}/${link.id}`,
      clicked: link.clickCount > 0,
    }));
    const renderableGuruAnswerLinks = (_.get(linksByType, 'guruAnswerLinks', []) as Link[]).map((link) => ({
      tags: link.tags,
      title: link.title,
      analytics_link: `${this.analyticsBaseUrl}/${link.id}`,
      clicked: link.clickCount > 0,
    }));

    const updatedContent = await this.templateProvider.build(templates.LIT_LIST, {
      answeredQuestions: renderableAnsweredQuestionLinks,
      nonAnsweredQuestions: renderableNonAnsweredQuestionLinks,
      guruAnswers: renderableGuruAnswerLinks,
    });

    await this.telegramClient.updateMessageByTelegramId(messageFullInfo.telegramId, messageFullInfo.chat.telegramId, updatedContent);
    await this.messageRepository.updateContentById(messageFullInfo.id, updatedContent);
  }
}
