import {Test, TestingModule} from '@nestjs/testing';
import {LinksByType, MessageService} from './message.service';
import {Message, MessageRepository, MessageWithLinks} from '../repositories/models/message.repository';
import {LinkRepository} from '../repositories/models/link.repository';
import {TelegramClient} from './telegram-client.service';
import {TemplateProvider} from '../template-provider';
import * as sinon from 'sinon';
import {MESSAGE_CATEGORIES} from '../dictionaries/message-categories';
import {UuidUtil} from '../../utils/uuid.util';
import {LoggerModule} from 'nestjs-pino/dist';

const message: Partial<Message> = {
  content: 'some content',
  category: MESSAGE_CATEGORIES.LIT_LIST,
};

const linksByType: LinksByType = {
  answeredQuestions: [
    {id: 'first_id', title: 'title 1', address: 'link1.com', tags: ['javascript', 'node.js']},
    {id: 'second_id', title: 'title 2', address: 'link2.com', tags: ['java']},
    {id: 'third_id', title: 'title 3', address: 'link3.com', tags: ['c++']},
  ],
  nonAnsweredQuestions: [
    {id: 'fourth_id', title: 'title 4', address: 'link4.com', tags: ['angular']},
    {id: 'fifth_id', title: 'title 5', address: 'link5.com', tags: ['typescript']},
    {id: 'sixth_id', title: 'title 6', address: 'link6.com', tags: ['spring']},
  ],
  guruAnswers: [
    {id: 'seventh_id', title: 'title 7', address: 'link7.com', tags: ['javascript']},
    {id: 'eighth_id', title: 'title 8', address: 'link8.com', tags: ['react']},
    {id: 'ninth_id', title: 'title 9', address: 'link9.com', tags: ['css', 'html']},
    {id: 'tenth_id', title: 'title 10', address: 'link10.com', tags: ['java']},
    {id: 'eleventh_id', title: 'title 11', address: 'link11.com', tags: ['node.js']},
  ],
};

const messageFullInfo: MessageWithLinks = {
  id: 'fake_uuid',
  privateId: 1,
  telegramId: 12345,
  content: 'content',
  category: MESSAGE_CATEGORIES.LIT_LIST,
  status: 'sent',
  createdAt: new Date(),
  updatedAt: new Date(),
  chat: {
    id: 'fake_uuid',
    privateId: 1,
    telegramId: 12345,
  },
  links: [
    {
      id: 'first_id',
      privateId: 1,
      title: 'title 1',
      address: 'link1.com',
      tags: ['javascript', 'node.js'],
      category: 'answered_question',
      clickCount: 1,
      messageId: 1,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      id: 'second_id',
      privateId: 2,
      title: 'title 2',
      address: 'link2.com',
      tags: ['java'],
      category: 'answered_question',
      clickCount: 2,
      messageId: 1,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      id: 'third_id',
      privateId: 3,
      title: 'title 3',
      address: 'link3.com',
      tags: ['c++'],
      category: 'answered_question',
      clickCount: 0,
      messageId: 1,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      id: 'fourth_id',
      privateId: 4,
      title: 'title 4',
      address: 'link4.com',
      tags: ['angular'],
      category: 'non_answered_question',
      clickCount: 0,
      messageId: 1,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      id: 'fifth_id',
      privateId: 5,
      title: 'title 5',
      address: 'link5.com',
      tags: ['typescript'],
      category: 'non_answered_question',
      clickCount: 0,
      messageId: 1,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      id: 'sixth_id',
      privateId: 6,
      title: 'title 6',
      address: 'link6.com',
      tags: ['spring'],
      category: 'non_answered_question',
      clickCount: 0,
      messageId: 1,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      id: 'seventh_id',
      privateId: 7,
      title: 'title 7',
      address: 'link7.com',
      tags: ['javascript'],
      category: 'guru_answer',
      clickCount: 0,
      messageId: 1,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      id: 'eighth_id',
      privateId: 8,
      title: 'title 8',
      address: 'link8.com',
      tags: ['react'],
      category: 'guru_answer',
      clickCount: 0,
      messageId: 1,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      id: 'ninth_id',
      privateId: 9,
      title: 'title 9',
      address: 'link9.com',
      tags: ['css', 'html'],
      category: 'guru_answer',
      clickCount: 0,
      messageId: 1,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      id: 'tenth_id',
      privateId: 10,
      title: 'title 10',
      address: 'link10.com',
      tags: ['java'],
      category: 'guru_answer',
      clickCount: 0,
      messageId: 1,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      id: 'eleventh_id',
      privateId: 11,
      title: 'title 11',
      address: 'link11.com',
      tags: ['node.js'],
      category: 'guru_answer',
      clickCount: 0,
      messageId: 1,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
  ],
};

describe('MessageService unit tests', () => {
  let messageService: MessageService;
  let messageRepository: MessageRepository;
  let linkRepository: LinkRepository;
  let telegramClient: TelegramClient;

  beforeAll(() => {
    sinon.stub(UuidUtil, 'v4').callsFake(() => 'fake_uuid');
  });

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [
        LoggerModule.forRoot(),
      ],
      providers: [
        MessageService, MessageRepository, LinkRepository, TelegramClient, TemplateProvider,
        {
          provide: 'LinkModel',
          useValue: {},
        },
        {
          provide: 'MessageModel',
          useValue: {},
        },
        {
          provide: 'ChatModel',
          useValue: {},
        },
        {
          provide: 'SequelizeToken',
          useValue: {
            transaction: async (fun) => {
              return await fun('transaction');
            },
          },
        },
        {
          provide: 'Logger',
          useValue: {
            // tslint:disable-next-line:no-empty
            info: () => {},
            // tslint:disable-next-line:no-empty
            warn: () => {},
            // tslint:disable-next-line:no-empty
            error: () => {},
          },
        },
        {
          provide: 'RedisToken',
          useValue: {},
        },
        {
          provide: 'TELEGRAM_TOKEN',
          useValue: 'tg_token',
        },
        {
          provide: 'TELEGRAM_API_HOST',
          useValue: 'tghost',
        },
        {
          provide: 'SE_API_URL',
          useValue: 'se_api',
        },
        {
          provide: 'SE_API_KEY',
          useValue: 'se_api_key',
        },
        {
          provide: 'SE_FILTER_NAME',
          useValue: 'se_filter_name',
        },
        {
          provide: 'ANALYTICS_BASE_URL',
          useValue: 'base_url',
        },
      ],
    }).compile();

    messageService = app.get(MessageService);
    messageRepository = app.get(MessageRepository);
    linkRepository = app.get(LinkRepository);
    telegramClient = app.get(TelegramClient);
  });

  describe('MessageService.createLitListMessage tests', () => {
    it('should create a lit list message with links within a single transaction', async () => {
      const createMessageSpy = sinon.stub(messageRepository, 'create')
        .callsFake((messageData) => Promise.resolve({id: messageData.id, privateId: 1}));
      const bulkLinkCreateSpy = sinon.stub(linkRepository, 'bulkCreate').callsFake(() => Promise.resolve());

      const messageId = await messageService.createLitListMessage(message, linksByType);

      expect(messageId).toBe('fake_uuid');
      expect(createMessageSpy.calledOnceWith({
        ...message,
        status: 'scheduled',
        id: UuidUtil.v4(),
      }, 'transaction')).toBe(true);
      expect(bulkLinkCreateSpy.firstCall.args).toEqual([[
        {
          id: 'seventh_id',
          messageId: 1,
          category: 'guru_answer',
          address: 'link7.com',
          title: 'title 7',
          tags: [
            'javascript',
          ],
          clickCount: 0,
        },
        {
          id: 'eighth_id',
          messageId: 1,
          category: 'guru_answer',
          address: 'link8.com',
          title: 'title 8',
          tags: [
            'react',
          ],
          clickCount: 0,
        },
        {
          id: 'ninth_id',
          messageId: 1,
          category: 'guru_answer',
          address: 'link9.com',
          title: 'title 9',
          tags: [
            'css',
            'html',
          ],
          clickCount: 0,
        },
        {
          id: 'tenth_id',
          messageId: 1,
          category: 'guru_answer',
          address: 'link10.com',
          title: 'title 10',
          tags: [
            'java',
          ],
          clickCount: 0,
        },
        {
          id: 'eleventh_id',
          messageId: 1,
          category: 'guru_answer',
          address: 'link11.com',
          title: 'title 11',
          tags: [
            'node.js',
          ],
          clickCount: 0,
        },
        {
          id: 'first_id',
          messageId: 1,
          category: 'answered_question',
          address: 'link1.com',
          title: 'title 1',
          tags: [
            'javascript',
            'node.js',
          ],
          clickCount: 0,
        },
        {
          id: 'second_id',
          messageId: 1,
          category: 'answered_question',
          address: 'link2.com',
          title: 'title 2',
          tags: [
            'java',
          ],
          clickCount: 0,
        },
        {
          id: 'third_id',
          messageId: 1,
          category: 'answered_question',
          address: 'link3.com',
          title: 'title 3',
          tags: [
            'c++',
          ],
          clickCount: 0,
        },
        {
          id: 'fourth_id',
          messageId: 1,
          category: 'non_answered_question',
          address: 'link4.com',
          title: 'title 4',
          tags: [
            'angular',
          ],
          clickCount: 0,
        },
        {
          id: 'fifth_id',
          messageId: 1,
          category: 'non_answered_question',
          address: 'link5.com',
          title: 'title 5',
          tags: [
            'typescript',
          ],
          clickCount: 0,
        },
        {
          id: 'sixth_id',
          messageId: 1,
          category: 'non_answered_question',
          address: 'link6.com',
          title: 'title 6',
          tags: [
            'spring',
          ],
          clickCount: 0,
        },
      ], 'transaction']);
    });
  });

  describe('MessageService.updateMessageViewByLinkId', () => {
    it('should update message view with checkmark', async () => {
      sinon.stub(messageRepository, 'getFullMessageInfoByLinkId').callsFake(() => Promise.resolve(messageFullInfo));
      sinon.stub(messageRepository, 'updateContentById').callsFake(() => Promise.resolve());
      const telegramClientSpy = sinon.stub(telegramClient, 'updateMessageByTelegramId').callsFake(() => Promise.resolve());

      await messageService.updateMessageViewByLinkId(messageFullInfo.links[0].id);

      expect(telegramClientSpy.firstCall.args).toEqual([
        messageFullInfo.telegramId, messageFullInfo.chat.telegramId,
        '📥 What\'s up? Here\'s the daily top StackOverflow content for you!\n' +
        '\n' +
        '<b>Top 5 guru answers:</b>\n' +
        '<code>[javascript]</code>\n' +
        '🔥 title 7\n' +
        '<a href="base_url/seventh_id">🚀 Lit link</a>\n' +
        '\n' +
        '<code>[react]</code>\n' +
        '🔥 title 8\n' +
        '<a href="base_url/eighth_id">🚀 Lit link</a>\n' +
        '\n' +
        '<code>[css,html]</code>\n' +
        '🔥 title 9\n' +
        '<a href="base_url/ninth_id">🚀 Lit link</a>\n' +
        '\n' +
        '<code>[java]</code>\n' +
        '🔥 title 10\n' +
        '<a href="base_url/tenth_id">🚀 Lit link</a>\n' +
        '\n' +
        '<code>[node.js]</code>\n' +
        '🔥 title 11\n' +
        '<a href="base_url/eleventh_id">🚀 Lit link</a>\n' +
        '\n' +
        '<b>Top 3 answered questions:</b>\n' +
        '<code>[javascript,node.js]</code>\n' +
        '✅ title 1\n' +
        '<a href="base_url/first_id">🚀 Lit link</a>\n' +
        '\n' +
        '<code>[java]</code>\n' +
        '✅ title 2\n' +
        '<a href="base_url/second_id">🚀 Lit link</a>\n' +
        '\n' +
        '<code>[c++]</code>\n' +
        '🔥 title 3\n' +
        '<a href="base_url/third_id">🚀 Lit link</a>\n' +
        '\n' +
        '<b>Top 3 non answered questions:</b>\n' +
        '<code>[angular]</code>\n' +
        '🔥 title 4\n' +
        '<a href="base_url/fourth_id">🚀 Lit link</a>\n' +
        '\n' +
        '<code>[typescript]</code>\n' +
        '🔥 title 5\n' +
        '<a href="base_url/fifth_id">🚀 Lit link</a>\n' +
        '\n' +
        '<code>[spring]</code>\n' +
        '🔥 title 6\n' +
        '<a href="base_url/sixth_id">🚀 Lit link</a>\n' +
        '\n' +
        '\n' +
        'See you tomorrow! 👋\n',
      ]);
    });

    it('should not rerender the message if the click count is greater than one', async () => {
      sinon.stub(messageRepository, 'getFullMessageInfoByLinkId').callsFake(() => Promise.resolve(messageFullInfo));
      const telegramSpy = sinon.spy(telegramClient, 'updateMessageByTelegramId');
      const updateContentSpy = sinon.spy(messageRepository, 'updateContentById');

      await messageService.updateMessageViewByLinkId(messageFullInfo.links[1].id);

      expect(telegramSpy.notCalled).toBe(true);
      expect(updateContentSpy.notCalled).toBe(true);
    });
  });
});
