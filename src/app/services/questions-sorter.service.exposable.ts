import {expose} from 'threads/worker';
import {questionsSorterModule} from './questions-sorter.service';

expose(questionsSorterModule);
