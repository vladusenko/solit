import {Link, LinkRepository} from '../repositories/models/link.repository';
import {Injectable} from '@nestjs/common';
import {EntityNotFoundError} from '../errors/entity-not-found.error';

@Injectable()
export class LinkService {
  constructor(
    private readonly linkRepository: LinkRepository,
  ) {}

  public async getById(id: string): Promise<Link> {
    const linkInstance = await this.linkRepository.getById(id);
    if (!linkInstance) {
      throw new EntityNotFoundError(`Link not found by id ${id}`);
    }
    return linkInstance;
  }
}
