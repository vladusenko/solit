import {SeClient} from '../se-client';
import {Injectable} from '@nestjs/common';
import * as _ from 'lodash';

@Injectable()
export class TagsService {
  constructor(
    private readonly seClient: SeClient,
  ) {}

  async getRelated(tag: string, amount: number): Promise<string[]> {
    const related = await this.seClient.getRelatedTags(tag);
    return _.slice(related, 1, amount);
  }
}
