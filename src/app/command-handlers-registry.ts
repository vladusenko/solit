import { Commands } from '../enums/commands';
import { StartCommandHandler } from './command-handlers/start.command-handler';
import { ChatRepository } from './repositories/models/chat.repository';
import { Inject, Injectable } from '@nestjs/common';
import { Sequelize } from 'sequelize';
import { CommandHandler } from './command-handlers/command-handler';
import { UnsupportedCommandError } from './errors/unsupported-command.error';
import { ChatStateRepository } from './repositories/states/chat-state.repository';
import { TemplateProvider } from './template-provider';
import { MessageScheduler } from './message-scheduler/message-scheduler';
import {MessageService} from './services/message.service';
import {PinoLogger} from 'nestjs-pino/dist';

@Injectable()
export class CommandHandlersRegistry {
  private readonly handlersMap: Map<Commands, CommandHandler> = new Map();
  constructor(
    chatRepository: ChatRepository,
    @Inject('SequelizeToken') sequelize: Sequelize,
    chatStateRepository: ChatStateRepository,
    templateProvider: TemplateProvider,
    messageScheduler: MessageScheduler,
    messageService: MessageService,
    logger: PinoLogger,
  ) {
    this.handlersMap.set(Commands.START, new StartCommandHandler(
      chatRepository,
      chatStateRepository,
      sequelize,
      templateProvider,
      messageScheduler,
      messageService,
      logger,
    ));
  }

  public getHandlerForCommand(command: Commands) {
    const handler = this.handlersMap.get(command);
    if (!handler) {
      throw new UnsupportedCommandError(`No handler found for command ${command}`);
    }

    return handler;
  }
}
