import { Injectable } from '@nestjs/common';
import { CommandHandlersRegistry } from './command-handlers-registry';
import { Commands } from '../enums/commands';
import { TelegramUpdate } from './tg-webhook-update-parser';

@Injectable()
export class CommandRouter {
  constructor(
    private readonly commandHandlersRegistry: CommandHandlersRegistry,
  ) {}

  public route(tgUpdate: TelegramUpdate) {
    return this.commandHandlersRegistry.getHandlerForCommand(tgUpdate.command as Commands).handle(tgUpdate);
  }
}
