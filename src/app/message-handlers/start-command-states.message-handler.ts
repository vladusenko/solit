import { MessageHandler } from './message-handler';
import { TelegramUpdate } from '../tg-webhook-update-parser';
import { ChatStateRepository, State } from '../repositories/states/chat-state.repository';
import { START_COMMAND_STATE } from '../states/states';
import { TagsStateRepository } from '../repositories/states/tags-state.repository';
import { SeClient } from '../se-client';
import { Pipeline } from 'ioredis';
import { PreferenceRepository } from '../repositories/models/preference.repository';
import { UuidUtil } from '../../utils/uuid.util';
import { ChatRepository } from '../repositories/models/chat.repository';
import { UnsupportedStateStepError } from '../errors/unsupported-state-step.error';
import {spawn, Pool, Worker} from 'threads';
import * as _ from 'lodash';
import { TemplateProvider, templates } from '../template-provider';
import {MessageScheduler} from '../message-scheduler/message-scheduler';
import {DateUtil} from '../../utils/date.util';
import {MESSAGE_CATEGORIES} from '../dictionaries/message-categories';
import {formatTagWithDashes, formatTagWithDots} from '../../utils/generic-utils';
import {TagsService} from '../services/tags.service';
import {MAIN_MENU} from '../dictionaries/main-menu';
import {Question, QuestionRepository} from '../repositories/models/question.repository';
import {AnswerAggregationResult, AnswerRepository} from '../repositories/models/answer.repository';
import {LinksByType, MessageService} from '../services/message.service';

export class StartCommandStatesMessageHandler extends MessageHandler {
  private readonly setupTagCount = 5;

  constructor(
    private readonly chatRepository: ChatRepository,
    private readonly chatStateRepository: ChatStateRepository,
    private readonly tagsStateRepository: TagsStateRepository,
    private readonly preferenceRepository: PreferenceRepository,
    private readonly questionsRepository: QuestionRepository,
    private readonly answersRepository: AnswerRepository,
    private readonly seClient: SeClient,
    private readonly messageScheduler: MessageScheduler,
    private readonly templateProvider: TemplateProvider,
    private readonly tagsService: TagsService,
    private readonly messageService: MessageService,
    private readonly analyticsEndpoint: string,
  ) {
    super();
  }

  private async validateTag(tag: string): Promise<string|null> {
    let validatedTag = await this.seClient.validateTag(tag);

    if (validatedTag !== null) {
      return validatedTag;
    }

    validatedTag = await this.seClient.validateTag(formatTagWithDashes(tag));

    if (validatedTag !== null) {
      return validatedTag;
    }

    validatedTag = await this.seClient.validateTag(formatTagWithDots(tag));

    if (validatedTag !== null) {
      return validatedTag;
    }

    return null;
  }

  public handle(tgUpdate: TelegramUpdate, state: State) {
    switch (state.step) {
      case START_COMMAND_STATE.steps.AFTER_WELCOME: {
        return this.handleAfterWelcome(tgUpdate);
      }
      case START_COMMAND_STATE.steps.TAGS_SETUP: {
        return this.handleTagsSetup(tgUpdate);
      }
      default: throw new UnsupportedStateStepError(`Didn't find handler for step ${state.step} on command ${state.command}`);
    }
  }

  private async handleAfterWelcome(tgUpdate: TelegramUpdate) {
    const tag = await this.validateTag(tgUpdate.originalText);

    const chat = await this.chatRepository.getByTelegramId(tgUpdate.chatId);
    if (!tag) {
      const tagNotFoundMessageData = {
        content: await this.templateProvider.build(templates.TAG_NOT_FOUND, {tag: tgUpdate.originalText}),
        category: MESSAGE_CATEGORIES.REGULAR,
        chatId: chat.privateId,
      };
      const tagNotFoundMessageId = await this.messageService.createMessage(tagNotFoundMessageData);
      return void await this.messageScheduler.scheduleMessage({
        ...tagNotFoundMessageData,
        privateChatId: chat.privateId,
        telegramChatId: chat.telegramId,
      }, tagNotFoundMessageId, DateUtil.currentEpoch());
    }

    let pipeline = await this.tagsStateRepository.pushTag(tgUpdate.chatId, tag, {startTransaction: true});
    pipeline = await this.chatStateRepository.setStateByChatId(tgUpdate.chatId, {
      command: '/start',
      step: START_COMMAND_STATE.steps.TAGS_SETUP,
      started: tgUpdate.dateSent,
      lastAction: tgUpdate.dateSent,
    }, {usePipeline: pipeline}) as Pipeline;
    await pipeline.exec();
    const relatedTags = await this.tagsService.getRelated(tag, 9);

    const addedTagWalkthroughMessageData = {
      content: await this.templateProvider.build(templates.ADDED_TAG_WALKTHROUGH, {
        tag,
        remainingCount: this.setupTagCount - 1,
        suggestion: true,
      }),
      category: MESSAGE_CATEGORIES.REGULAR,
      chatId: chat.privateId,
    };
    const addedTagWalkthroughMessageId = await this.messageService.createMessage(addedTagWalkthroughMessageData);
    await this.messageScheduler.scheduleMessage({
      ...addedTagWalkthroughMessageData,
      customKeyboard: _.chunk(relatedTags, 3),
      privateChatId: chat.privateId,
      telegramChatId: chat.telegramId,
    }, addedTagWalkthroughMessageId, DateUtil.currentEpoch());
  }

  private async handleTagsSetup(tgUpdate: TelegramUpdate) {
    const tag = await this.validateTag(tgUpdate.originalText);
    const chat = await this.chatRepository.getByTelegramId(tgUpdate.chatId);
    if (!tag) {
      const tagNotFoundMessageData = {
        content: await this.templateProvider.build(templates.TAG_NOT_FOUND, {tag: tgUpdate.originalText}),
        category: MESSAGE_CATEGORIES.REGULAR,
        chatId: chat.privateId,
      };
      const tagNotFoundMessageId = await this.messageService.createMessage(tagNotFoundMessageData);
      return void await this.messageScheduler.scheduleMessage({
        ...tagNotFoundMessageData,
        privateChatId: chat.privateId,
        telegramChatId: chat.telegramId,
      }, tagNotFoundMessageId, DateUtil.currentEpoch());
    }

    const currentTags = await this.tagsStateRepository.getAllTags(tgUpdate.chatId);
    if (currentTags.includes(tag)) {
      const tagAlreadyAddedMessageData = {
        content: await this.templateProvider.build(templates.TAG_ALREADY_ADDED, {tag}),
        category: MESSAGE_CATEGORIES.REGULAR,
        chatId: chat.privateId,
      };
      const tagAlreadyAddedMessageId = await this.messageService.createMessage(tagAlreadyAddedMessageData);

      return void await this.messageScheduler.scheduleMessage({
        ...tagAlreadyAddedMessageData,
        privateChatId: chat.privateId,
        telegramChatId: chat.telegramId,
      }, tagAlreadyAddedMessageId, DateUtil.currentEpoch());
    }

    let pipeline = await this.tagsStateRepository.pushTag(tgUpdate.chatId, tag, {startTransaction: true});
    pipeline = await this.chatStateRepository.updateStateByChatId(tgUpdate.chatId, {
      lastAction: tgUpdate.dateSent,
    }, {usePipeline: pipeline});
    await pipeline.exec();

    const tagsCount = await this.tagsStateRepository.getCount(tgUpdate.chatId);
    if (tagsCount === this.setupTagCount) {
      const allTags = await this.tagsStateRepository.getAllTags(tgUpdate.chatId);
      await this.preferenceRepository.create({id: UuidUtil.v4(), tags: allTags, chatId: chat.privateId});
      let deletePipeline = await this.chatStateRepository.deleteStateByChatId(tgUpdate.chatId, {startTransaction: true}) as Pipeline;
      deletePipeline = await this.tagsStateRepository.deleteTagsByChatId(tgUpdate.chatId, {usePipeline: deletePipeline}) as Pipeline;
      deletePipeline = await this.tagsStateRepository.deleteTagSynonymsByChatId(tgUpdate.chatId, {usePipeline: deletePipeline}) as Pipeline;
      await deletePipeline.exec();
      const afterWalkthroughMessageData = {
        content: await this.templateProvider.build(templates.AFTER_WALKTHROUGH, {tag}),
        category: MESSAGE_CATEGORIES.REGULAR,
        chatId: chat.privateId,
      };
      const afterWalkthroughMessageId = await this.messageService.createMessage(afterWalkthroughMessageData);
      await this.messageScheduler.scheduleMessage({
        ...afterWalkthroughMessageData,
        customKeyboard: _.chunk(_.values(MAIN_MENU), 3),
        privateChatId: chat.privateId,
        telegramChatId: chat.telegramId,
      }, afterWalkthroughMessageId, DateUtil.currentEpoch());

      const litList = await this.buildLitList(allTags);
      litList.guruAnswers.forEach((answer) => {
        answer.id = UuidUtil.v4();
        answer.analytics_link = `${this.analyticsEndpoint}/${answer.id}`;
        answer.clicked = false;
      });
      litList.answeredQuestions.forEach((question) => {
        question.id = UuidUtil.v4();
        question.analytics_link = `${this.analyticsEndpoint}/${question.id}`;
        question.clicked = false;
      });
      litList.nonAnsweredQuestions.forEach((question) => {
        question.id = UuidUtil.v4();
        question.analytics_link = `${this.analyticsEndpoint}/${question.id}`;
        question.clicked = false;
      });

      const litListMessageData = {
        content: await this.templateProvider.build(templates.LIT_LIST, litList),
        category: MESSAGE_CATEGORIES.LIT_LIST,
        privateChatId: chat.privateId,
        chatId: chat.privateId,
      };
      const linksByType: LinksByType = {
        guruAnswers: litList.guruAnswers.map((answer) => ({
          id: answer.id,
          address: answer.share_link,
          tags: answer.tags,
          title: answer.title,
        })),
        answeredQuestions: litList.answeredQuestions.map((question) => ({
          id: question.id,
          address: question.link,
          tags: question.tags,
          title: question.title,
        })),
        nonAnsweredQuestions: litList.nonAnsweredQuestions.map((question) => ({
          id: question.id,
          address: question.link,
          tags: question.tags,
          title: question.title,
        })),
      };
      const litListMessageId = await this.messageService.createLitListMessage(litListMessageData, linksByType);
      await this.messageScheduler.scheduleMessage({
        ...litListMessageData,
        privateChatId: chat.privateId,
        telegramChatId: chat.telegramId,
      }, litListMessageId, DateUtil.currentEpoch());

      const useMenuMessageData = {
        content: await this.templateProvider.build(templates.USE_MENU, {}),
        category: MESSAGE_CATEGORIES.REGULAR,
        chatId: chat.privateId,
      };
      const useMenuMessageId = await this.messageService.createMessage(useMenuMessageData);
      await this.messageScheduler.scheduleMessage({
        ...useMenuMessageData,
        privateChatId: chat.privateId,
        telegramChatId: chat.telegramId,
      }, useMenuMessageId, DateUtil.currentEpoch());
    } else {
      const relatedTags = await this.tagsService.getRelated(tag, 12);
      _.remove(relatedTags, (item) => currentTags.concat(tag).includes(item)); // remove tags from suggestions if they are already followed
      const keyboard = _.chunk(_.slice(relatedTags, 0, 11), 3);

      const addedTagWalkthroughMessageData = {
        content: await this.templateProvider.build(templates.ADDED_TAG_WALKTHROUGH, {
          tag,
          remainingCount: this.setupTagCount - tagsCount,
        }),
        category: MESSAGE_CATEGORIES.REGULAR,
        chatId: chat.privateId,
      };
      const messageId = await this.messageService.createMessage(addedTagWalkthroughMessageData);

      await this.messageScheduler.scheduleMessage({
        ...addedTagWalkthroughMessageData,
        customKeyboard: keyboard,
        privateChatId: chat.privateId,
        telegramChatId: chat.telegramId,
      }, messageId, DateUtil.currentEpoch());
    }
  }

  private async buildLitList(tags: string[]) {
    const twoDaysBack: Date = DateUtil.oneDayBack();
    const oneDayBack: Date = DateUtil.now();

    const questions: Question[] = await this.questionsRepository.getQuestionsByTagsAndDate(
      tags,
      twoDaysBack,
      oneDayBack,
    );

    const guruAnswers: AnswerAggregationResult[] = await this.answersRepository.getTopByTagsAndOwnerReputation(
      tags,
      twoDaysBack,
      oneDayBack,
      50000,
      3,
    );

    let result;

    const pool = Pool(() => spawn(new Worker('../services/questions-sorter.service.exposable')), 1);
    pool.queue(async (questionsSorterModule) => {
      const answeredQuestions = await questionsSorterModule.topWithAnswers(
        questions,
        3,
      );

      const nonAnsweredQuestions = await questionsSorterModule.topWithoutAnswers(
        questions,
        3,
      );

      result = {
        answeredQuestions,
        nonAnsweredQuestions,
        guruAnswers,
      };
    });

    await pool.completed();
    await pool.terminate();

    return result;
  }
}
