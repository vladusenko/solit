import { TelegramUpdate } from '../tg-webhook-update-parser';
import { State } from '../repositories/states/chat-state.repository';

export abstract class MessageHandler {
  public abstract handle(tgUpdate: TelegramUpdate, state: State);
}
