import {Test, TestingModule} from '@nestjs/testing';
import {ChatRepository} from '../repositories/models/chat.repository';
import {TemplateProvider, templates} from '../template-provider';
import {MessageScheduler} from '../message-scheduler/message-scheduler';
import {MessageSchedulerRepository} from '../repositories/message-scheduler.repository';
import {MenuMessageHandler} from './menu.message-handler';
import * as sinon from 'sinon';
import {MAIN_MENU} from '../dictionaries/main-menu';
import {MESSAGE_CATEGORIES} from '../dictionaries/message-categories';
import {DateUtil} from '../../utils/date.util';
import {MessageService} from '../services/message.service';
import {MessageRepository} from '../repositories/models/message.repository';
import {LinkRepository} from '../repositories/models/link.repository';
import {TelegramClient} from '../services/telegram-client.service';
import {LoggerModule} from 'nestjs-pino/dist';

describe('MenuMessageHandler unit tests', () => {
  let menuMessageHandler: MenuMessageHandler;
  let messageScheduler: MessageScheduler;
  let templateProvider: TemplateProvider;
  let chatRepository: ChatRepository;
  let messageService: MessageService;

  beforeAll(() => {
    sinon.stub(DateUtil, 'currentEpoch').callsFake(() => 12345);
  });

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [
        LoggerModule.forRoot(),
      ],
      providers: [MenuMessageHandler, ChatRepository, TemplateProvider, MessageScheduler, MessageSchedulerRepository,
        MessageService, MessageRepository, LinkRepository, TelegramClient,
        {
          provide: 'SequelizeToken',
          useValue: {
            transaction: (options, fun) => {
              return fun();
            },
          },
        },
        {
          provide: 'RedisToken',
          useValue: {},
        },
        {
          provide: 'TELEGRAM_TOKEN',
          useValue: 'tg_token',
        },
        {
          provide: 'TELEGRAM_API_HOST',
          useValue: 'tghost',
        },
        {
          provide: 'ANALYTICS_BASE_URL',
          useValue: 'analytics_base_url',
        },
        {
          provide: 'LinkModel',
          useValue: {},
        },
        {
          provide: 'MessageModel',
          useValue: {},
        },
        {
          provide: 'ChatModel',
          useValue: {},
        },
        {
          provide: 'PreferenceModel',
          useValue: {},
        }],
    }).compile();

    chatRepository = app.get<ChatRepository>(ChatRepository);
    templateProvider = app.get<TemplateProvider>(TemplateProvider);
    messageScheduler = app.get<MessageScheduler>(MessageScheduler);
    menuMessageHandler = app.get<MenuMessageHandler>(MenuMessageHandler);
    messageService = app.get(MessageService);
  });
  describe('My Subscriptions command', () => {
    it('should send a message with tags that user is subscribed to sorted alphabetically', async () => {
      sinon.stub(chatRepository, 'getChatWithPreferencesByTelegramId').callsFake(() => Promise.resolve({
        id: 'id',
        privateId: 1,
        telegramId: 12345,
        preferences: {
          id: 'another id',
          privateId: 1,
          chatId: 1,
          tags: ['node.js', 'c++', 'java'],
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      }));
      const scheduleMessageSpy = sinon.stub(messageScheduler, 'scheduleMessage').callsFake(() => Promise.resolve());
      const createMessageSpy = sinon.stub(messageService, 'createMessage').callsFake(() => Promise.resolve(54321));

      await menuMessageHandler.handle(1, MAIN_MENU.MY_SUBSCRIPTIONS);

      expect(createMessageSpy.firstCall.args).toEqual([
        {
          content: await templateProvider.build(templates.MY_SUBSCRIPTIONS, {
            tags: [{name: 'c++'}, {name: 'java'}, {name: 'node.js'}],
            total: 3,
          }),
          category: 'regular',
          chatId: 1,
        },
      ]);
      expect(scheduleMessageSpy.firstCall.args).toEqual([{
          content: await templateProvider.build(templates.MY_SUBSCRIPTIONS, {
            tags: [{name: 'c++'}, {name: 'java'}, {name: 'node.js'}],
            total: 3,
          }),
          category: MESSAGE_CATEGORIES.REGULAR,
          chatId: 1,
          privateChatId: 1,
          telegramChatId: 12345,
        }, 54321, DateUtil.currentEpoch()]);
    });

    it('should send a sorry message if user has no subscriptions', async () => {
      sinon.stub(chatRepository, 'getChatWithPreferencesByTelegramId').callsFake(() => Promise.resolve({
        id: 'id',
        privateId: 1,
        telegramId: 12345,
        preferences: {
          id: 'another id',
          privateId: 1,
          chatId: 1,
          tags: [],
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      }));
      const scheduleMessageSpy = sinon.stub(messageScheduler, 'scheduleMessage').callsFake(() => Promise.resolve());
      const createMessageSpy = sinon.stub(messageService, 'createMessage').callsFake(() => Promise.resolve(54321));

      await menuMessageHandler.handle(1, MAIN_MENU.MY_SUBSCRIPTIONS);

      expect(createMessageSpy.firstCall.args).toEqual([
        {
          content: await templateProvider.build(templates.MY_SUBSCRIPTIONS_NO_TAGS, {
            editSubscriptionsOptionName: MAIN_MENU.EDIT_SUBSCRIPTIONS,
          }),
          category: 'regular',
          chatId: 1,
        },
      ]);
      expect(scheduleMessageSpy.firstCall.args).toEqual([{
        content: await templateProvider.build(templates.MY_SUBSCRIPTIONS_NO_TAGS, {
          editSubscriptionsOptionName: MAIN_MENU.EDIT_SUBSCRIPTIONS,
        }),
        category: MESSAGE_CATEGORIES.REGULAR,
        chatId: 1,
        privateChatId: 1,
        telegramChatId: 12345,
      }, 54321, DateUtil.currentEpoch()]);
    });
  });
});
