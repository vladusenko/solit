import { Test, TestingModule } from '@nestjs/testing';
import { ChatRepository } from '../repositories/models/chat.repository';
import { ChatStateRepository, State } from '../repositories/states/chat-state.repository';
import { TelegramClient } from '../services/telegram-client.service';
import { Sequelize } from 'sequelize';
import { StartCommandStatesMessageHandler } from './start-command-states.message-handler';
import { TagsStateRepository } from '../repositories/states/tags-state.repository';
import { PreferenceRepository } from '../repositories/models/preference.repository';
import { SeClient, SeQuestion } from '../se-client';
import * as sinon from 'sinon';
import { TelegramUpdate } from '../tg-webhook-update-parser';
import { START_COMMAND_STATE } from '../states/states';
import { UnsupportedStateStepError } from '../errors/unsupported-state-step.error';
import { TemplateProvider, templates } from '../template-provider';
import {MessageScheduler} from '../message-scheduler/message-scheduler';
import {MessageSchedulerRepository} from '../repositories/message-scheduler.repository';
import {DateUtil} from '../../utils/date.util';
import {MESSAGE_CATEGORIES} from '../dictionaries/message-categories';
import {formatTagWithDashes, formatTagWithDots} from '../../utils/generic-utils';
import {TagsService} from '../services/tags.service';
import {AnswerRepository} from '../repositories/models/answer.repository';
import {QuestionRepository} from '../repositories/models/question.repository';
import {MessageService} from '../services/message.service';
import {LinkRepository} from '../repositories/models/link.repository';
import {MessageRepository} from '../repositories/models/message.repository';
import {LoggerModule} from 'nestjs-pino/dist';

const tgUpdate: TelegramUpdate = {
  updateId: 12345,
  chatId: 12345,
  originalText: 'some tag',
  dateSent: 123456789,
};

const afterWelcomeState: State = {
  command: '/start',
  step: START_COMMAND_STATE.steps.AFTER_WELCOME,
  started: 123456789,
  lastAction: 123456789,
};

const tagsSetupState: State = {
  command: '/start',
  step: START_COMMAND_STATE.steps.TAGS_SETUP,
  started: 123456789,
  lastAction: 123456789,
};

const unknownStepState: State = {
  command: '/start',
  step: 'unknown',
  started: 123456789,
  lastAction: 123456789,
};

const nodeJsAndJsQuestion: SeQuestion = {
  tags: [
    'node.js',
    'javascript',
  ],
  owner: {
    reputation: 1505,
    user_id: 10064334,
    user_type: 'registered',
    profile_image: 'https://i.stack.imgur.com/CTqAD.jpg?s=128&g=1',
    display_name: 'MichaelB',
    link: 'https://stackoverflow.com/users/10064334/michaelb',
  },
  is_answered: false,
  view_count: 56,
  closed_date: 1564116993,
  answer_count: 1,
  score: 3,
  last_activity_date: 1564149646,
  creation_date: 1564064052,
  last_edit_date: 1564149646,
  question_id: 57204092,
  link: 'https://link.com',
  closed_reason: 'duplicate',
  title: 'Node.js and Javascript question',
};
const javaQuestion: SeQuestion = {
  tags: [
    'java',
  ],
  owner: {
    reputation: 1505,
    user_id: 10064334,
    user_type: 'registered',
    profile_image: 'https://i.stack.imgur.com/CTqAD.jpg?s=128&g=1',
    display_name: 'MichaelB',
    link: 'https://stackoverflow.com/users/10064334/michaelb',
  },
  is_answered: false,
  view_count: 40,
  answer_count: 1,
  score: 2,
  last_activity_date: 1564149646,
  creation_date: 1564064052,
  last_edit_date: 1564149646,
  question_id: 57204093,
  link: 'https://link.com',
  title: 'Java questions',
};

const jsQuestion: SeQuestion = {
  tags: [
    'javascript',
  ],
  owner: {
    reputation: 1505,
    user_id: 10064334,
    user_type: 'registered',
    profile_image: 'https://i.stack.imgur.com/CTqAD.jpg?s=128&g=1',
    display_name: 'MichaelB',
    link: 'https://stackoverflow.com/users/10064334/michaelb',
  },
  is_answered: false,
  view_count: 37,
  answer_count: 2,
  score: 1,
  last_activity_date: 1564149646,
  creation_date: 1564064052,
  last_edit_date: 1564149646,
  question_id: 57204094,
  link: 'https://link.com',
  title: 'Javascript question',
};
const nodeJsQuestion: SeQuestion = {
  tags: [
    'node.js',
  ],
  owner: {
    reputation: 1505,
    user_id: 10064334,
    user_type: 'registered',
    profile_image: 'https://i.stack.imgur.com/CTqAD.jpg?s=128&g=1',
    display_name: 'MichaelB',
    link: 'https://stackoverflow.com/users/10064334/michaelb',
  },
  is_answered: false,
  view_count: 67,
  answer_count: 3,
  score: 4,
  last_activity_date: 1564149646,
  creation_date: 1564064052,
  last_edit_date: 1564149646,
  question_id: 57204095,
  link: 'https://link.com',
  title: 'Node.js question',
};

describe('StartCommandStatesMessageHandler unit tests', () => {
  let chatRepository: ChatRepository;
  let messageScheduler: MessageScheduler;
  let sequelize: Sequelize;
  let chatStateRepository: ChatStateRepository;
  let startCommandStatesMessageHandler: StartCommandStatesMessageHandler;
  let tagsStateRepository: TagsStateRepository;
  let preferenceRepository: PreferenceRepository;
  let seClient: SeClient;
  let templateProvider: TemplateProvider;
  let tagsService: TagsService;
  let answerRepository: AnswerRepository;
  let questionRepository: QuestionRepository;
  let messageService: MessageService;
  let analyticsBaseUrl: string;

  beforeAll(() => {
    sinon.stub(DateUtil, 'currentEpoch').callsFake(() => 12345);
  });

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [
        LoggerModule.forRoot(),
      ],
      providers: [ChatRepository, ChatStateRepository, TelegramClient, TagsStateRepository, PreferenceRepository, SeClient, TemplateProvider,
        MessageScheduler, MessageSchedulerRepository, TagsService, AnswerRepository, QuestionRepository, MessageService,
        LinkRepository, MessageRepository,
        {
          provide: 'SequelizeToken',
          useValue: {
            transaction: (options, fun) => {
              return fun();
            },
          },
        },
        {
          provide: 'RedisToken',
          useValue: {},
        },
        {
          provide: 'TELEGRAM_TOKEN',
          useValue: 'tg_token',
        },
        {
          provide: 'TELEGRAM_API_HOST',
          useValue: 'tghost',
        },
        {
          provide: 'SE_API_URL',
          useValue: 'se_api',
        },
        {
          provide: 'SE_API_KEY',
          useValue: 'se_api_key',
        },
        {
          provide: 'SE_FILTER_NAME',
          useValue: 'se_filter_name',
        },
        {
          provide: 'ANALYTICS_BASE_URL',
          useValue: 'base_url',
        },
        {
          provide: 'ChatModel',
          useValue: {},
        }, {
          provide: 'PreferenceModel',
          useValue: {},
        },
        {
          provide: 'AnswerModel',
          useValue: {},
        },
        {
          provide: 'QuestionModel',
          useValue: {},
        },
        {
          provide: 'LinkModel',
          useValue: {},
        },
        {
          provide: 'MessageModel',
          useValue: {},
        }],
    }).compile();

    chatRepository = app.get<ChatRepository>(ChatRepository);
    messageScheduler = app.get<MessageScheduler>(MessageScheduler);
    sequelize = app.get<Sequelize>('SequelizeToken');
    chatStateRepository = app.get<ChatStateRepository>(ChatStateRepository);
    tagsStateRepository = app.get<TagsStateRepository>(TagsStateRepository);
    preferenceRepository = app.get<PreferenceRepository>(PreferenceRepository);
    seClient = app.get<SeClient>(SeClient);
    templateProvider = app.get<TemplateProvider>(TemplateProvider);
    tagsService = app.get<TagsService>(TagsService);
    answerRepository = app.get<AnswerRepository>(AnswerRepository);
    questionRepository = app.get<QuestionRepository>(QuestionRepository);
    messageService = app.get(MessageService);
    analyticsBaseUrl = app.get('ANALYTICS_BASE_URL');

    startCommandStatesMessageHandler = new StartCommandStatesMessageHandler(
      chatRepository,
      chatStateRepository,
      tagsStateRepository,
      preferenceRepository,
      questionRepository,
      answerRepository,
      seClient,
      messageScheduler,
      templateProvider,
      tagsService,
      messageService,
      analyticsBaseUrl,
    );
  });

  describe('handle tests', () => {
    it ('should throw an instance of UnsupportedStateStepError if step is not supported by handler', async () => {
      let error: UnsupportedStateStepError;

      try {
        await startCommandStatesMessageHandler.handle(tgUpdate, unknownStepState);
      } catch (err) {
        error = err;
      }

      expect(error).toBeInstanceOf(UnsupportedStateStepError);
    });
  });

  describe('handleAfterWelcome tests', () => {
    it('should successfully handle after welcome state message', async () => {
      sinon.stub(chatRepository, 'getByTelegramId').callsFake(() => Promise.resolve({
        telegramId: tgUpdate.chatId,
        privateId: 1,
      }));
      const seClientValidateTagSpy = sinon.stub(seClient, 'validateTag').callsFake((tag) => Promise.resolve(tag));
      const tagsServiceGetRelatedSpy = sinon.stub(tagsService, 'getRelated')
        .callsFake(() => Promise.resolve(['tag1', 'tag2', 'tag3', 'tag4']));
      const createMessageSpy = sinon.stub(messageService, 'createMessage').callsFake(() => Promise.resolve(12345));
      const messageSchedulerSpy = sinon.stub(messageScheduler, 'scheduleMessage').callsFake(() => Promise.resolve());
      const tagsStateRepositoryPushTagSpy = sinon.stub(tagsStateRepository, 'pushTag').callsFake(() => Promise.resolve('fake_pipeline'));
      const chatStateRepositorySetStateByChatIdSpy = sinon.stub(chatStateRepository, 'setStateByChatId')
      // tslint:disable-next-line:no-empty
        .callsFake(() => Promise.resolve({exec: () => {}}));

      await startCommandStatesMessageHandler.handle(tgUpdate, afterWelcomeState);

      expect(seClientValidateTagSpy.calledOnceWith(tgUpdate.originalText)).toEqual(true);
      expect(tagsServiceGetRelatedSpy.calledOnceWith(tgUpdate.originalText)).toEqual(true);
      expect(createMessageSpy.calledOnceWith({
        content: await templateProvider.build(templates.ADDED_TAG_WALKTHROUGH, {
          tag: tgUpdate.originalText,
          remainingCount: 4,
          suggestion: true,
        }),
        category: 'regular',
        chatId: 1,
      })).toEqual(true);
      expect(messageSchedulerSpy.calledOnceWith({
          content: await templateProvider.build(templates.ADDED_TAG_WALKTHROUGH, {
            tag: tgUpdate.originalText,
            remainingCount: 4,
            suggestion: true,
          }),
          customKeyboard: [['tag1', 'tag2', 'tag3'], ['tag4']],
          telegramChatId: tgUpdate.chatId,
          privateChatId: 1,
          chatId: 1,
          category: MESSAGE_CATEGORIES.REGULAR,
        },
        12345,
        DateUtil.currentEpoch(),
      )).toEqual(true);
      expect(tagsStateRepositoryPushTagSpy.calledOnceWith(tgUpdate.chatId, tgUpdate.originalText, {startTransaction: true})).toEqual(true);
      expect(chatStateRepositorySetStateByChatIdSpy.calledOnceWith(tgUpdate.chatId, {
        command: '/start',
        step: START_COMMAND_STATE.steps.TAGS_SETUP,
        started: tgUpdate.dateSent,
        lastAction: tgUpdate.dateSent,
      }, {usePipeline: 'fake_pipeline'})).toEqual(true);
    });

    it('should send validation error message to user in case provided tag is invalid', async () => {
      sinon.stub(chatRepository, 'getByTelegramId').callsFake(() => Promise.resolve({
        telegramId: tgUpdate.chatId,
        privateId: 1,
      }));
      const seClientSpy = sinon.stub(seClient, 'validateTag').callsFake(() => Promise.resolve(null));
      const createMessageSpy = sinon.stub(messageService, 'createMessage').callsFake(() => Promise.resolve(12345));
      const messageSchedulerSpy = sinon.stub(messageScheduler, 'scheduleMessage').callsFake(() => Promise.resolve());
      const tagsStateRepositoryPushTagSpy = sinon.stub(tagsStateRepository, 'pushTag').callsFake(() => Promise.resolve('fake_pipeline'));
      const chatStateRepositorySetStateByChatIdSpy = sinon.stub(chatStateRepository, 'setStateByChatId')
      // tslint:disable-next-line:no-empty
        .callsFake(() => Promise.resolve({exec: () => {}}));

      await startCommandStatesMessageHandler.handle(tgUpdate, afterWelcomeState);

      expect(seClientSpy.calledWith(tgUpdate.originalText)).toEqual(true);
      expect(seClientSpy.calledWith(formatTagWithDashes(tgUpdate.originalText))).toEqual(true);
      expect(seClientSpy.calledWith(formatTagWithDots(tgUpdate.originalText))).toEqual(true);
      expect(seClientSpy.calledThrice).toEqual(true);
      expect(createMessageSpy.calledOnceWith({
        content:  await templateProvider.build(templates.TAG_NOT_FOUND, {tag: tgUpdate.originalText}),
        category: 'regular',
        chatId: 1,
      })).toEqual(true);
      expect(messageSchedulerSpy.calledOnceWith({
          content: await templateProvider.build(templates.TAG_NOT_FOUND, {tag: tgUpdate.originalText}),
          category: MESSAGE_CATEGORIES.REGULAR,
          telegramChatId: tgUpdate.chatId,
          chatId: 1,
          privateChatId: 1,
        },
        12345,
        DateUtil.currentEpoch()),
      ).toEqual(true);
      expect(tagsStateRepositoryPushTagSpy.notCalled).toEqual(true);
      expect(chatStateRepositorySetStateByChatIdSpy.notCalled).toEqual(true);
    });
  });

  describe('handleTagsSetup tests', () => {
    it('should successfully handle message on tags setup step', async () => {
      sinon.stub(chatRepository, 'getByTelegramId').callsFake(() => Promise.resolve({
        telegramId: tgUpdate.chatId,
        privateId: 1,
      }));
      const seClientSpy = sinon.stub(seClient, 'validateTag').callsFake((tag) => Promise.resolve(tag));
      const createMessageSpy = sinon.stub(messageService, 'createMessage').callsFake(() => Promise.resolve(12345));
      const messageSchedulerSpy = sinon.stub(messageScheduler, 'scheduleMessage').callsFake(() => Promise.resolve());
      const tagsServiceGetRelatedSpy = sinon.stub(tagsService, 'getRelated')
        .callsFake(() => Promise.resolve(['related1', 'related2', 'related3', 'related4']));
      const tagsStateRepositoryPushTagSpy = sinon.stub(tagsStateRepository, 'pushTag').callsFake(() => Promise.resolve('fake_pipeline'));
      const tagsStateRepositoryGetAllTagsSpy = sinon.stub(tagsStateRepository, 'getAllTags').callsFake(() => Promise.resolve([]));
      const chatStateRepositoryUpdateStateByChatIdSpy = sinon.stub(chatStateRepository, 'updateStateByChatId')
      // tslint:disable-next-line:no-empty
        .callsFake(() => Promise.resolve({exec: () => {}}));
      const tagsStateRepositoryGetCountSpy = sinon.stub(tagsStateRepository, 'getCount').callsFake(() => Promise.resolve(1));

      await startCommandStatesMessageHandler.handle(tgUpdate, tagsSetupState);

      expect(seClientSpy.calledOnceWith(tgUpdate.originalText)).toEqual(true);
      expect(tagsServiceGetRelatedSpy.calledOnceWith(tgUpdate.originalText, 12)).toEqual(true);
      expect(tagsStateRepositoryGetAllTagsSpy.calledOnceWith(tgUpdate.chatId)).toEqual(true);
      expect(tagsStateRepositoryPushTagSpy.calledOnceWith(tgUpdate.chatId, tgUpdate.originalText, {startTransaction: true})).toEqual(true);
      expect(tagsStateRepositoryGetCountSpy.calledOnceWith(tgUpdate.chatId)).toEqual(true);
      expect(chatStateRepositoryUpdateStateByChatIdSpy.calledOnceWith(tgUpdate.chatId, {
        lastAction: tgUpdate.dateSent,
      }, {usePipeline: 'fake_pipeline'}));
      expect(createMessageSpy.calledOnceWith({
        content: await templateProvider.build(templates.ADDED_TAG_WALKTHROUGH, {
          tag: tgUpdate.originalText,
          remainingCount: 4,
        }),
        chatId: 1,
        category: MESSAGE_CATEGORIES.REGULAR,
      })).toBe(true);
      expect(messageSchedulerSpy.calledOnceWith(
        {
          content: await templateProvider.build(templates.ADDED_TAG_WALKTHROUGH, {
            tag: tgUpdate.originalText,
            remainingCount: 4,
          }),
          customKeyboard: [['related1', 'related2', 'related3'], ['related4']],
          telegramChatId: tgUpdate.chatId,
          privateChatId: 1,
          chatId: 1,
          category: MESSAGE_CATEGORIES.REGULAR,
        }, 12345, DateUtil.currentEpoch())).toBe(true);
    });

    it('should apply dashes if original tag was not found on SE', async () => {
      const dashedTag = formatTagWithDashes(tgUpdate.originalText);
      sinon.stub(chatRepository, 'getByTelegramId').callsFake(() => Promise.resolve({
        telegramId: tgUpdate.chatId,
        privateId: 1,
      }));
      const seClientSpy = sinon.stub(seClient, 'validateTag')
        .callsFake((tag) => Promise.resolve(formatTagWithDashes(tag)))
        .onFirstCall().returns(Promise.resolve(null));
      const createMessageSpy = sinon.stub(messageService, 'createMessage').callsFake(() => Promise.resolve(12345));
      const messageSchedulerSpy = sinon.stub(messageScheduler, 'scheduleMessage').callsFake(() => Promise.resolve());
      const tagsServiceGetRelatedSpy = sinon.stub(tagsService, 'getRelated')
        .callsFake(() => Promise.resolve(['related1', 'related2', 'related3', 'related4']));
      const tagsStateRepositoryPushTagSpy = sinon.stub(tagsStateRepository, 'pushTag').callsFake(() => Promise.resolve('fake_pipeline'));
      const tagsStateRepositoryGetAllTagsSpy = sinon.stub(tagsStateRepository, 'getAllTags').callsFake(() => Promise.resolve([]));
      const chatStateRepositoryUpdateStateByChatIdSpy = sinon.stub(chatStateRepository, 'updateStateByChatId')
      // tslint:disable-next-line:no-empty
        .callsFake(() => Promise.resolve({exec: () => {}}));
      const tagsStateRepositoryGetCountSpy = sinon.stub(tagsStateRepository, 'getCount').callsFake(() => Promise.resolve(1));

      await startCommandStatesMessageHandler.handle(tgUpdate, tagsSetupState);

      expect(seClientSpy.calledTwice).toEqual(true);
      expect(tagsServiceGetRelatedSpy.calledOnceWith(dashedTag, 12)).toEqual(true);
      expect(tagsStateRepositoryGetAllTagsSpy.calledOnceWith(tgUpdate.chatId)).toEqual(true);
      expect(tagsStateRepositoryPushTagSpy.calledOnceWith(tgUpdate.chatId, dashedTag, {startTransaction: true})).toEqual(true);
      expect(tagsStateRepositoryGetCountSpy.calledOnceWith(tgUpdate.chatId)).toEqual(true);
      expect(chatStateRepositoryUpdateStateByChatIdSpy.calledOnceWith(tgUpdate.chatId, {
        lastAction: tgUpdate.dateSent,
      }, {usePipeline: 'fake_pipeline'}));
      expect(createMessageSpy.calledOnceWith({
        content: await templateProvider.build(templates.ADDED_TAG_WALKTHROUGH, {
          tag: dashedTag,
          remainingCount: 4,
        }),
        category: MESSAGE_CATEGORIES.REGULAR,
        chatId: 1,
      })).toBe(true);
      expect(messageSchedulerSpy.calledOnceWith(
        {
          content: await templateProvider.build(templates.ADDED_TAG_WALKTHROUGH, {
            tag: dashedTag,
            remainingCount: 4,
          }),
          customKeyboard: [['related1', 'related2', 'related3'], ['related4']],
          telegramChatId: tgUpdate.chatId,
          privateChatId: 1,
          chatId: 1,
          category: MESSAGE_CATEGORIES.REGULAR,
        }, 12345, DateUtil.currentEpoch())).toBe(true);
    });

    it('should apply dots if neither dashed nor original tag were not found on SE', async () => {
      const dottedTag = formatTagWithDots(tgUpdate.originalText);
      sinon.stub(chatRepository, 'getByTelegramId').callsFake(() => Promise.resolve({
        telegramId: tgUpdate.chatId,
        privateId: 1,
      }));
      const seClientSpy = sinon.stub(seClient, 'validateTag')
        .callsFake((tag) => Promise.resolve(formatTagWithDashes(tag)))
        .onFirstCall().returns(Promise.resolve(null))
        .onSecondCall().returns(Promise.resolve(null));
      const createMessageSpy = sinon.stub(messageService, 'createMessage').callsFake(() => Promise.resolve(12345));
      const messageSchedulerSpy = sinon.stub(messageScheduler, 'scheduleMessage').callsFake(() => Promise.resolve());
      const tagsServiceGetRelatedSpy = sinon.stub(tagsService, 'getRelated')
        .callsFake(() => Promise.resolve(['related1', 'related2', 'related3', 'related4']));
      const tagsStateRepositoryPushTagSpy = sinon.stub(tagsStateRepository, 'pushTag').callsFake(() => Promise.resolve('fake_pipeline'));
      const tagsStateRepositoryGetAllTagsSpy = sinon.stub(tagsStateRepository, 'getAllTags').callsFake(() => Promise.resolve([]));
      const chatStateRepositoryUpdateStateByChatIdSpy = sinon.stub(chatStateRepository, 'updateStateByChatId')
      // tslint:disable-next-line:no-empty
        .callsFake(() => Promise.resolve({exec: () => {}}));
      const tagsStateRepositoryGetCountSpy = sinon.stub(tagsStateRepository, 'getCount').callsFake(() => Promise.resolve(1));

      await startCommandStatesMessageHandler.handle(tgUpdate, tagsSetupState);

      expect(seClientSpy.calledThrice).toEqual(true);
      expect(tagsServiceGetRelatedSpy.calledOnceWith(dottedTag, 12)).toEqual(true);
      expect(tagsStateRepositoryGetAllTagsSpy.calledOnceWith(tgUpdate.chatId)).toEqual(true);
      expect(tagsStateRepositoryPushTagSpy.calledOnceWith(tgUpdate.chatId, dottedTag, {startTransaction: true})).toEqual(true);
      expect(tagsStateRepositoryGetCountSpy.calledOnceWith(tgUpdate.chatId)).toEqual(true);
      expect(chatStateRepositoryUpdateStateByChatIdSpy.calledOnceWith(tgUpdate.chatId, {
        lastAction: tgUpdate.dateSent,
      }, {usePipeline: 'fake_pipeline'}));
      expect(createMessageSpy.calledOnceWith({
        content: await templateProvider.build(templates.ADDED_TAG_WALKTHROUGH, {
          tag: dottedTag,
          remainingCount: 4,
        }),
        category: MESSAGE_CATEGORIES.REGULAR,
        chatId: 1,
      })).toBe(true);
      expect(messageSchedulerSpy.calledOnceWith(
        {
          content: await templateProvider.build(templates.ADDED_TAG_WALKTHROUGH, {
            tag: dottedTag,
            remainingCount: 4,
          }),
          customKeyboard: [['related1', 'related2', 'related3'], ['related4']],
          telegramChatId: tgUpdate.chatId,
          privateChatId: 1,
          chatId: 1,
          category: MESSAGE_CATEGORIES.REGULAR,
        }, 12345, DateUtil.currentEpoch())).toBe(true);
    });

    it('should remove tags from suggestions if they are already followed', async () => {
      const dashedTag = formatTagWithDashes(tgUpdate.originalText);
      sinon.stub(chatRepository, 'getByTelegramId').callsFake(() => Promise.resolve({
        telegramId: tgUpdate.chatId,
        privateId: 1,
      }));
      const seClientSpy = sinon.stub(seClient, 'validateTag')
        .callsFake((tag) => Promise.resolve(formatTagWithDashes(tag)))
        .onFirstCall().returns(Promise.resolve(null));
      const createMessageSpy = sinon.stub(messageService, 'createMessage').callsFake(() => Promise.resolve(12345));
      const messageSchedulerSpy = sinon.stub(messageScheduler, 'scheduleMessage').callsFake(() => Promise.resolve());
      const tagsServiceGetRelatedSpy = sinon.stub(tagsService, 'getRelated')
        .callsFake(() => Promise.resolve(['related1', 'related2', 'related3', 'related4']));
      const tagsStateRepositoryPushTagSpy = sinon.stub(tagsStateRepository, 'pushTag').callsFake(() => Promise.resolve('fake_pipeline'));
      const tagsStateRepositoryGetAllTagsSpy = sinon.stub(tagsStateRepository, 'getAllTags')
        .callsFake(() => Promise.resolve(['related4']));
      const chatStateRepositoryUpdateStateByChatIdSpy = sinon.stub(chatStateRepository, 'updateStateByChatId')
      // tslint:disable-next-line:no-empty
        .callsFake(() => Promise.resolve({exec: () => {}}));
      const tagsStateRepositoryGetCountSpy = sinon.stub(tagsStateRepository, 'getCount').callsFake(() => Promise.resolve(1));

      await startCommandStatesMessageHandler.handle(tgUpdate, tagsSetupState);

      expect(seClientSpy.calledTwice).toEqual(true);
      expect(tagsServiceGetRelatedSpy.calledOnceWith(dashedTag, 12)).toEqual(true);
      expect(tagsStateRepositoryGetAllTagsSpy.calledOnceWith(tgUpdate.chatId)).toEqual(true);
      expect(tagsStateRepositoryPushTagSpy.calledOnceWith(tgUpdate.chatId, dashedTag, {startTransaction: true})).toEqual(true);
      expect(tagsStateRepositoryGetCountSpy.calledOnceWith(tgUpdate.chatId)).toEqual(true);
      expect(chatStateRepositoryUpdateStateByChatIdSpy.calledOnceWith(tgUpdate.chatId, {
        lastAction: tgUpdate.dateSent,
      }, {usePipeline: 'fake_pipeline'}));
      expect(createMessageSpy.calledOnceWith({
        content: await templateProvider.build(templates.ADDED_TAG_WALKTHROUGH, {
          tag: dashedTag,
          remainingCount: 4,
        }),
        category: MESSAGE_CATEGORIES.REGULAR,
        chatId: 1,
      })).toBe(true);
      expect(messageSchedulerSpy.calledOnceWith(
        {
          content: await templateProvider.build(templates.ADDED_TAG_WALKTHROUGH, {
            tag: dashedTag,
            remainingCount: 4,
          }),
          customKeyboard: [['related1', 'related2', 'related3']],
          telegramChatId: tgUpdate.chatId,
          chatId: 1,
          privateChatId: 1,
          category: MESSAGE_CATEGORIES.REGULAR,
        }, 12345, DateUtil.currentEpoch())).toBe(true);
    });

    it('should fail to validate tag and send a message about it', async () => {
      sinon.stub(chatRepository, 'getByTelegramId').callsFake(() => Promise.resolve({
        telegramId: tgUpdate.chatId,
        privateId: 1,
      }));
      const seClientSpy = sinon.stub(seClient, 'validateTag').callsFake(() => Promise.resolve(null));
      const createMessageSpy = sinon.stub(messageService, 'createMessage').callsFake(() => Promise.resolve(12345));
      const messageSchedulerSpy = sinon.stub(messageScheduler, 'scheduleMessage').callsFake(() => Promise.resolve());

      await startCommandStatesMessageHandler.handle(tgUpdate, tagsSetupState);

      expect(seClientSpy.calledWith(tgUpdate.originalText)).toEqual(true);
      expect(seClientSpy.calledWith(formatTagWithDashes(tgUpdate.originalText))).toEqual(true);
      expect(seClientSpy.calledWith(formatTagWithDots(tgUpdate.originalText))).toEqual(true);
      expect(seClientSpy.calledThrice).toEqual(true);
      expect(createMessageSpy.calledOnceWith(
        {
          content: await templateProvider.build(templates.TAG_NOT_FOUND, {tag: tgUpdate.originalText}),
          chatId: 1,
          category: MESSAGE_CATEGORIES.REGULAR,
        },
      ));
      expect(messageSchedulerSpy.calledOnceWith(
        {
          content: await templateProvider.build(templates.TAG_NOT_FOUND, {tag: tgUpdate.originalText}),
          telegramChatId: tgUpdate.chatId,
          chatId: 1,
          privateChatId: 1,
          category: MESSAGE_CATEGORIES.REGULAR,
        }, 12345, DateUtil.currentEpoch())).toBe(true);
    });

    it('should find a duplicate tag if it already added and send a message about it', async () => {
      sinon.stub(chatRepository, 'getByTelegramId').callsFake(() => Promise.resolve({
        telegramId: tgUpdate.chatId,
        privateId: 1,
      }));
      const seClientSpy = sinon.stub(seClient, 'validateTag').callsFake((tag) => Promise.resolve(tag));
      const createMessageSpy = sinon.stub(messageService, 'createMessage').callsFake(() => Promise.resolve(12345));
      const messageSchedulerSpy = sinon.stub(messageScheduler, 'scheduleMessage').callsFake(() => Promise.resolve());
      const tagsStateRepositoryGetAllTagsSpy = sinon.stub(tagsStateRepository, 'getAllTags')
        .callsFake(() => Promise.resolve([tgUpdate.originalText]));

      await startCommandStatesMessageHandler.handle(tgUpdate, tagsSetupState);

      expect(seClientSpy.calledOnceWith(tgUpdate.originalText)).toEqual(true);
      expect(tagsStateRepositoryGetAllTagsSpy.calledOnceWith(tgUpdate.chatId)).toEqual(true);
      expect(createMessageSpy.calledOnceWith({
        content: await templateProvider.build(templates.TAG_ALREADY_ADDED, {tag: tgUpdate.originalText}),
        chatId: 1,
        category: MESSAGE_CATEGORIES.REGULAR,
      })).toBe(true);
      expect(messageSchedulerSpy.calledOnceWith({
        content: await templateProvider.build(templates.TAG_ALREADY_ADDED, {tag: tgUpdate.originalText}),
        privateChatId: 1,
        telegramChatId: tgUpdate.chatId,
        chatId: 1,
        category: MESSAGE_CATEGORIES.REGULAR,
      },
        12345,
        DateUtil.currentEpoch(),
      )).toEqual(true);
    });

    it.skip('should send a "You\'re all set!" message when user added third tag, provide sample questions w/o duplicates and sorted', async () => {
      sinon.stub(chatRepository, 'getByTelegramId').callsFake(() => Promise.resolve({
        telegramId: tgUpdate.chatId,
        privateId: 1,
      }));
      const seClientValidateTagSpy = sinon.stub(seClient, 'validateTag')
        .callsFake((tag) => Promise.resolve(tag));
      /*const seClientSearchMostRelevantQuestionsSpy = sinon.stub(seClient, 'searchMostRelevantQuestions')
        .callsFake(() => Promise.resolve([nodeJsAndJsQuestion, javaQuestion, jsQuestion, nodeJsQuestion, nodeJsQuestion]));*/
      const messageSchedulerSpy = sinon.stub(messageScheduler, 'scheduleMessage').callsFake(() => Promise.resolve());
      const tagsServiceAggregateSynonymsSpy = sinon.stub(tagsService, 'aggregateSynonyms')
        .callsFake(() => Promise.resolve(['synonym1', 'synonym2', 'synonym3']));
      const tagsStateRepositoryPushTagSpy = sinon.stub(tagsStateRepository, 'pushTag').callsFake(() => Promise.resolve('fake_pipeline'));
      const tagsStateRepositoryPushTagSynonymsSpy = sinon.stub(tagsStateRepository, 'pushTagSynonyms')
        .callsFake(() => Promise.resolve('fake_pipeline'));
      const chatStateRepositoryUpdateStateByChatIdSpy = sinon.stub(chatStateRepository, 'updateStateByChatId')
      // tslint:disable-next-line:no-empty
        .callsFake(() => Promise.resolve({exec: () => {}}));
      const tagsStateRepositoryGetAllTagsSpy = sinon.stub(tagsStateRepository, 'getAllTags')
        .callsFake(() => Promise.resolve(['javascript', 'node.js', 'java', 'jvm']))
        .onSecondCall().returns(Promise.resolve(['javascript', 'node.js', 'java', 'jvm', 'typescript']));
      const tagsStateRepositoryGetCountSpy = sinon.stub(tagsStateRepository, 'getCount').callsFake(() => Promise.resolve(5));
      const tagsStateRepositoryGetSynonymsByTagSpy = sinon.stub(tagsStateRepository, 'getSynonymsByTag')
        .callsFake(() => Promise.resolve(['synonym1', 'synonym2', 'synonym3']));
      const preferenceRepositoryCreateSpy = sinon.stub(preferenceRepository, 'create').callsFake(() => Promise.resolve());
      const chatStateRepositoryDeleteStateByChatIdSpy = sinon.stub(chatStateRepository, 'deleteStateByChatId')
        .callsFake(() => Promise.resolve('fake_pipeline'));
      const tagsStateRepositoryDeleteTagsByChatIdSpy = sinon.stub(tagsStateRepository, 'deleteTagsByChatId')
      // tslint:disable-next-line:no-empty
        .callsFake(() => Promise.resolve('fake_pipeline'));
      const tagsStateRepositoryDeleteTagSynonymsByChatIdSpy = sinon.stub(tagsStateRepository, 'deleteTagSynonymsByChatId')
      // tslint:disable-next-line:no-empty
        .callsFake(() => Promise.resolve({exec: () => {}}));
      const getQuestionsByTagsAndDateSpy = sinon.stub(questionRepository, 'getQuestionsByTagsAndDate')
        .callsFake(() => Promise.resolve([ javaQuestion, jsQuestion, nodeJsQuestion, nodeJsQuestion]));

      await startCommandStatesMessageHandler.handle(tgUpdate, tagsSetupState);

      expect(seClientValidateTagSpy.calledOnceWith(tgUpdate.originalText)).toEqual(true);
      expect(tagsServiceAggregateSynonymsSpy.calledOnceWith(tgUpdate.originalText)).toEqual(true);
      expect(tagsStateRepositoryGetAllTagsSpy.calledTwice).toEqual(true);
      expect(tagsStateRepositoryGetAllTagsSpy.calledWith(tgUpdate.chatId)).toEqual(true);
      expect(tagsStateRepositoryPushTagSpy.calledOnceWith(tgUpdate.chatId, tgUpdate.originalText, {startTransaction: true})).toEqual(true);
      expect(tagsStateRepositoryPushTagSynonymsSpy.calledOnceWith(
        tgUpdate.chatId,
        tgUpdate.originalText,
        ['synonym1', 'synonym2', 'synonym3'],
        {usePipeline: 'fake_pipeline'},
      ));
      expect(tagsStateRepositoryGetCountSpy.calledOnceWith(tgUpdate.chatId)).toEqual(true);
      expect(chatStateRepositoryUpdateStateByChatIdSpy.calledOnceWith(tgUpdate.chatId, {
        lastAction: tgUpdate.dateSent,
      }, {usePipeline: 'fake_pipeline'})).toEqual(true);
      expect(tagsStateRepositoryGetSynonymsByTagSpy.callCount).toEqual(5);
      expect(preferenceRepositoryCreateSpy.calledOnce).toEqual(true);
      expect(chatStateRepositoryDeleteStateByChatIdSpy.calledOnceWith(tgUpdate.chatId, {startTransaction: true})).toEqual(true);
      expect(tagsStateRepositoryDeleteTagsByChatIdSpy.calledOnceWith(tgUpdate.chatId, {usePipeline: 'fake_pipeline'})).toEqual(true);
      expect(tagsStateRepositoryDeleteTagSynonymsByChatIdSpy.calledOnceWith(tgUpdate.chatId, {usePipeline: 'fake_pipeline'})).toEqual(true);
      expect(getQuestionsByTagsAndDateSpy.calledOnce).toEqual(true);
      const litListMessage = await templateProvider.build(templates.LIT_LIST, {questions: [
          nodeJsQuestion,
          nodeJsAndJsQuestion,
          javaQuestion,
          jsQuestion,
        ]});
      expect(messageSchedulerSpy.calledWithExactly({
        content: litListMessage,
        category: MESSAGE_CATEGORIES.LIT_LIST,
        telegramId: tgUpdate.chatId,
        privateChatId: 1,
      },
        DateUtil.currentEpoch(),
      ))
        .toEqual(true);
    });
  });
});
