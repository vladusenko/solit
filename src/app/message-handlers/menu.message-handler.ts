import {MAIN_MENU} from '../dictionaries/main-menu';
import * as _ from 'lodash';
import {MessageScheduler} from '../message-scheduler/message-scheduler';
import {TemplateProvider, templates} from '../template-provider';
import {MESSAGE_CATEGORIES} from '../dictionaries/message-categories';
import {ChatRepository} from '../repositories/models/chat.repository';
import {DateUtil} from '../../utils/date.util';
import {Injectable} from '@nestjs/common';
import {MessageService} from '../services/message.service';

@Injectable()
export class MenuMessageHandler {
  constructor(
    private readonly messageScheduler: MessageScheduler,
    private readonly templateProvider: TemplateProvider,
    private readonly chatRepository: ChatRepository,
    private readonly messageService: MessageService,
  ) {}

  public async handle(chatId: number, option: string) {
    switch (option) {
      case MAIN_MENU.MY_SUBSCRIPTIONS:
        await this.handleMySubscriptions(chatId);
        break;
      default: return;
    }
    return;
  }

  private async handleMySubscriptions(chatId: number) {
    const chatWithPreferences = await this.chatRepository.getChatWithPreferencesByTelegramId(chatId);
    const tags = _.get(chatWithPreferences, 'preferences.tags', [])
      .sort()
      .map((tag) => ({name: tag}));
    if (tags.length === 0) {
      const noTagsMessageData = {
        content: await this.templateProvider.build(templates.MY_SUBSCRIPTIONS_NO_TAGS, {
          editSubscriptionsOptionName: MAIN_MENU.EDIT_SUBSCRIPTIONS,
        }),
        category: MESSAGE_CATEGORIES.REGULAR,
        chatId: chatWithPreferences.privateId,
      };
      const noTagsMessageId = await this.messageService.createMessage(noTagsMessageData);
      await this.messageScheduler.scheduleMessage({
        ...noTagsMessageData,
        privateChatId: chatWithPreferences.privateId,
        telegramChatId: chatWithPreferences.telegramId,
      }, noTagsMessageId, DateUtil.currentEpoch());
    } else {
      const mySubscriptionsMessageData = {
        content: await this.templateProvider.build(templates.MY_SUBSCRIPTIONS, {tags, total: tags.length}),
        category: MESSAGE_CATEGORIES.REGULAR,
        chatId: chatWithPreferences.privateId,
      };
      const mySubscriptionsMessageId = await this.messageService.createMessage(mySubscriptionsMessageData);
      await this.messageScheduler.scheduleMessage({
        ...mySubscriptionsMessageData,
        privateChatId: chatWithPreferences.privateId,
        telegramChatId: chatWithPreferences.telegramId,
      }, mySubscriptionsMessageId, DateUtil.currentEpoch());
    }
  }
}
