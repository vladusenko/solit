import {ExceptionFilter, Catch, ArgumentsHost} from '@nestjs/common';
import {AppError} from '../errors/app.error';
import {EntityNotFoundError} from '../errors/entity-not-found.error';

@Catch(AppError)
export class AppErrorFilter implements ExceptionFilter {
    catch(error: AppError, host: ArgumentsHost) {
        const response = host.switchToHttp().getResponse();

        if (error instanceof EntityNotFoundError) {
            return response.status(404).json({
                statusCode: 404,
                message: `${error.entityName} was not found`,
                code: `${error.entityName}NotFound`,
            });
        }

        response.status(500).json({
            statusCode: 500,
            message: 'Internal Server Error',
        });
    }
}
