import {Test, TestingModule} from '@nestjs/testing';
import {MessageSchedulerRepository, ScheduledMessagePack} from '../repositories/message-scheduler.repository';
import {MailmanCronStateRepository} from '../repositories/crons/mailman-cron-state.repository';
import {TelegramClient} from '../services/telegram-client.service';
import {MessageRepository} from '../repositories/models/message.repository';
import {MailmanCron} from './mailman.cron';
import * as sinon from 'sinon';
import {MESSAGE_CATEGORIES} from '../dictionaries/message-categories';
import {UuidUtil} from '../../utils/uuid.util';
import {LoggerModule, PinoLogger} from 'nestjs-pino/dist';

const firstSampleScheduledMessage: {scheduledMessagePack: ScheduledMessagePack, rank: number} = {
  scheduledMessagePack: {
    id: '1',
    attempt: 0,
    scheduledMessage: {
      content: 'content',
      telegramChatId: 12345,
      privateChatId: 1,
      category: MESSAGE_CATEGORIES.REGULAR,
    },
  },
  rank: new Date().getTime() / 1000,
};

const secondSampleScheduledMessage: {scheduledMessagePack: ScheduledMessagePack, rank: number} = {
  scheduledMessagePack: {
    id: '2',
    attempt: 0,
    scheduledMessage: {
      content: 'content',
      telegramChatId: 123456,
      privateChatId: 2,
      category: MESSAGE_CATEGORIES.REGULAR,
    },
  },
  rank: new Date().getTime() / 1000,
};

const thirdSampleScheduledMessage = {
  scheduledMessagePack: {
    id: '3',
    attempt: 0,
    scheduledMessage: {
      content: 'content',
      telegramId: 123457,
      privateChatId: 3,
      category: MESSAGE_CATEGORIES.REGULAR,
    },
  },
  rank: new Date().getTime() / 1000,
};
// for commented out test case
/*
const fourthSampleScheduleMessage = {
  scheduledMessage: {
    content: 'content',
    telegramId: 123457,
    privateChatId: 3,
    category: MESSAGE_CATEGORIES.REGULAR,
  },
  rank: new Date().getTime() / 1000,
};*/

describe('MailmanCron unit tests', () => {
  let messageSchedulerRepository: MessageSchedulerRepository;
  let messageRepository: MessageRepository;
  let telegramClient: TelegramClient;
  let mailmanCron: MailmanCron;

  beforeAll(() => {
    sinon.stub(UuidUtil, 'v4').callsFake(() => 'fake_uuid');
  });
  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [
        LoggerModule.forRoot(),
      ],
      providers: [
        MessageSchedulerRepository, MailmanCronStateRepository,
        TelegramClient, MessageRepository,
        {
          provide: 'SequelizeToken',
          useValue: {
            transaction: (options, fun) => {
              return fun();
            },
          },
        },
        {
          provide: 'RedisToken',
          useValue: {},
        },
        {
          provide: 'TELEGRAM_TOKEN',
          useValue: 'tg_token',
        },
        {
          provide: 'TELEGRAM_API_HOST',
          useValue: 'tghost',
        },
        {
          provide: 'SE_API_URL',
          useValue: 'se_api',
        },
        {
          provide: 'SE_API_KEY',
          useValue: 'se_api_key',
        },
        {
          provide: 'MessageModel',
          useValue: {},
        },
        {
          provide: 'LinkModel',
          useValue: {},
        },
        {
          provide: 'ChatModel',
          useValue: {},
        },
        {
          provide: 'RedlockToken',
          useValue: {
            lock: async (key: string, ttl: number, cb: (err, lock) => Promise<void>) => {
              await cb(null, {unlock: (callback) => callback()});
            },
            on: () => null,
          },
        },
        {
          provide: 'THREADPOOL_MAX_SIZE',
          useValue: 2,
        },
        {
          provide: MailmanCron,
          useFactory:  (
            // tslint:disable-next-line:no-shadowed-variable
            redlock, messageSchedulerRepository: MessageSchedulerRepository,
            // tslint:disable-next-line:no-shadowed-variable
            mailmanCronStateRepository: MailmanCronStateRepository, messageSender: TelegramClient,
            // tslint:disable-next-line:no-shadowed-variable
            messageRepository: MessageRepository,
            logger: PinoLogger,
          ) => {
            logger.setContext(MailmanCron.name);
            return new MailmanCron(
              redlock, messageSchedulerRepository, mailmanCronStateRepository,
              messageSender, messageRepository, logger,
            );
          },
          inject: [
            'RedlockToken', MessageSchedulerRepository, MailmanCronStateRepository,
            TelegramClient, MessageRepository, PinoLogger,
          ],
        },
      ],
    }).compile();

    messageSchedulerRepository = app.get<MessageSchedulerRepository>(MessageSchedulerRepository);
    messageRepository = app.get<MessageRepository>(MessageRepository);
    telegramClient = app.get<TelegramClient>(TelegramClient);
    mailmanCron = app.get<MailmanCron>(MailmanCron);
  });

  describe('MailmanCron.run tests', () => {
    it('should get up to 30 latest messages in the queue and send them in parallel', async () => {
      const schedulerRepoGetMessagesSpy = sinon.stub(messageSchedulerRepository, 'getMessages')
        .callsFake(() => Promise.resolve([
          firstSampleScheduledMessage, secondSampleScheduledMessage, thirdSampleScheduledMessage,
        ]));
      const messageSenderSpy = sinon.stub(telegramClient, 'sendMessage').callsFake(() => Promise.resolve(12345));
      const setSentSpy = sinon.stub(messageRepository, 'setSentById').callsFake(() => Promise.resolve());
      const schedulerRepoScheduleSpy = sinon.spy(messageSchedulerRepository, 'scheduleMessage');

      await mailmanCron.run();

      expect(schedulerRepoGetMessagesSpy.calledOnce).toBe(true);
      expect(setSentSpy.calledWithExactly(firstSampleScheduledMessage.scheduledMessagePack.id, 12345)).toBe(true);
      expect(setSentSpy.calledWithExactly(secondSampleScheduledMessage.scheduledMessagePack.id, 12345)).toBe(true);
      expect(setSentSpy.calledWithExactly(thirdSampleScheduledMessage.scheduledMessagePack.id, 12345)).toBe(true);
      expect(messageSenderSpy.calledThrice).toBe(true);
      expect(schedulerRepoScheduleSpy.notCalled).toBe(true);
    });

    it('should not send and store any message if no message were scheduled', async () => {
      const schedulerRepoGetMessagesSpy = sinon.stub(messageSchedulerRepository, 'getMessages').callsFake(() => Promise.resolve([]));
      const setSentSpy = sinon.spy(messageRepository, 'setSentById');
      const messageSenderSpy = sinon.spy(telegramClient, 'sendMessage');
      const schedulerRepoScheduleSpy = sinon.spy(messageSchedulerRepository, 'scheduleMessage');

      await mailmanCron.run();

      expect(schedulerRepoGetMessagesSpy.calledOnce).toBe(true);
      expect(setSentSpy.notCalled).toBe(true);
      expect(messageSenderSpy.notCalled).toBe(true);
      expect(schedulerRepoScheduleSpy.notCalled).toBe(true);
    });

    it('should reschedule any failed messages with their original rank', async () => {
      const schedulerRepoGetMessagesSpy = sinon.stub(messageSchedulerRepository, 'getMessages')
        .callsFake(() => Promise.resolve([
          firstSampleScheduledMessage, secondSampleScheduledMessage, thirdSampleScheduledMessage,
        ]));
      const setSentSpy = sinon.stub(messageRepository, 'setSentById').callsFake(() => Promise.resolve());
      const messageSenderSpy = sinon.stub(telegramClient, 'sendMessage')
        .callsFake(() => Promise.resolve(12345))
        .onSecondCall().returns(Promise.reject({message: 'Error while sending message'}));
      const schedulerRepoScheduleSpy = sinon.stub(messageSchedulerRepository, 'scheduleMessage').callsFake(() => Promise.resolve());

      await mailmanCron.run();

      expect(schedulerRepoGetMessagesSpy.calledOnce).toBe(true);
      expect(setSentSpy.calledWithExactly(firstSampleScheduledMessage.scheduledMessagePack.id, 12345)).toBe(true);
      expect(setSentSpy.calledWithExactly(secondSampleScheduledMessage.scheduledMessagePack.id, 12345)).toBe(false);
      expect(setSentSpy.calledWithExactly(thirdSampleScheduledMessage.scheduledMessagePack.id, 12345)).toBe(true);
      expect(messageSenderSpy.calledThrice).toBe(true);
      expect(schedulerRepoScheduleSpy.calledOnceWithExactly(
        secondSampleScheduledMessage.scheduledMessagePack.id,
        secondSampleScheduledMessage.scheduledMessagePack.attempt++,
        secondSampleScheduledMessage.scheduledMessagePack.scheduledMessage,
        secondSampleScheduledMessage.rank,
      )).toBe(true);
    });

    it('should not reschedule message if attempts exceed the default of 3', async () => {
      const schedulerRepoGetMessagesSpy = sinon.stub(messageSchedulerRepository, 'getMessages')
        .callsFake(() => Promise.resolve([
          firstSampleScheduledMessage,
        ]));

      firstSampleScheduledMessage.scheduledMessagePack.attempt = 2;
      const setSentSpy = sinon.stub(messageRepository, 'setSentById').callsFake(() => Promise.resolve());
      const setStatusSpy = sinon.stub(messageRepository, 'setStatusById').callsFake(() => Promise.resolve());
      const messageSenderSpy = sinon.stub(telegramClient, 'sendMessage')
        .callsFake(() => Promise.reject({message: 'Error while sending message'}));
      const schedulerRepoScheduleSpy = sinon.stub(messageSchedulerRepository, 'scheduleMessage').callsFake(() => Promise.resolve());

      await mailmanCron.run();

      expect(schedulerRepoGetMessagesSpy.calledOnce).toBe(true);
      expect(setSentSpy.notCalled).toBe(true);
      expect(setStatusSpy.calledOnceWith(firstSampleScheduledMessage.scheduledMessagePack.id, 'failed_to_send')).toBe(true);
      expect(messageSenderSpy.calledOnce).toBe(true);
      expect(schedulerRepoScheduleSpy.notCalled).toBe(true);
    });

    // task #69
    /*it('should group messages by telegramId and grouped ones sequentially', async () => {
      const schedulerRepoGetMessagesSpy = sinon.stub(messageSchedulerRepository, 'getMessages')
        .callsFake((): Promise<Array<{scheduledMessage: ScheduledMessage, rank: number}>> => Promise.resolve([
          thirdSampleScheduledMessage, firstSampleScheduledMessage, secondSampleScheduledMessage,
          fourthSampleScheduleMessage,
        ]));
      const messageRepoBulkCreateSpy = sinon.stub(messageRepository, 'bulkCreate').callsFake(() => Promise.resolve());
      const messageSenderSpy = sinon.stub(messageSender, 'send').callsFake(() => Promise.resolve());
      const schedulerRepoScheduleSpy = sinon.stub(messageSchedulerRepository, 'scheduleMessage').callsFake(() => Promise.resolve());

      await mailmanCron.run();

      expect(schedulerRepoGetMessagesSpy.calledOnce).toBe(true);
      expect(messageRepoBulkCreateSpy.calledOnceWithExactly([
        {...thirdSampleScheduledMessage.scheduledMessage, id: UuidUtil.v4()},
        {...firstSampleScheduledMessage.scheduledMessage, id: UuidUtil.v4()},
        {...secondSampleScheduledMessage.scheduledMessage, id: UuidUtil.v4()},
        {...fourthSampleScheduleMessage.scheduledMessage, id: UuidUtil.v4()},
      ])).toBe(true);
      expect(schedulerRepoScheduleSpy.notCalled).toBe(true);
    });*/
  });
});
