import {Injectable} from '@nestjs/common';
import {MessageSchedulerRepository} from '../repositories/message-scheduler.repository';
import {MailmanCronStateRepository} from '../repositories/crons/mailman-cron-state.repository';
import {TelegramClient} from '../services/telegram-client.service';
import {MessageRepository} from '../repositories/models/message.repository';
import * as _ from 'lodash';
import {PinoLogger} from 'nestjs-pino/dist';

@Injectable()
export class MailmanCron {
  private readonly lockTtl = 5000; // 5 seconds
  private readonly MAX_RESCHEDULING_ATTEMPTS = 3;

  constructor(
    private readonly redlock, // manually injected
    private readonly messageSchedulerRepository: MessageSchedulerRepository,
    private readonly mailmanCronStateRepository: MailmanCronStateRepository,
    private readonly telegramClient: TelegramClient,
    private readonly messageRepository: MessageRepository,
    private readonly logger: PinoLogger,
  ) {}

  public async run() {
    return new Promise((resolve) => {
      this.redlock.lock(this.mailmanCronStateRepository.lockResourceKey, this.lockTtl, async (err, lock) => {
        try {
          if (err) {
            this.logger.warn(`Failed to acquire lock: ${err}`);
            return resolve();
          }

          const messages = await this.messageSchedulerRepository.getMessages(30);
          if (messages.length === 0) {
            return void lock.unlock((unlockErr) => {
              if (unlockErr) {
                this.logger.error(`Failed to release lock: ${unlockErr}`);
              }
              return resolve();
            });
          }
          const promises = [];
          const groupedByTelegramId = _.groupBy(messages, 'telegramId'); // messages to the same chat should be sent sequentially
          const received = messages.length;
          let sent = 0;
          let rescheduled = 0;

          // tslint:disable-next-line:forin
          for (const telegramId in groupedByTelegramId) {
            let chain: Promise<void | number> = Promise.resolve();
            if (groupedByTelegramId.hasOwnProperty(telegramId)) {
              for (const message of groupedByTelegramId[telegramId]) {
                chain = chain
                  .then(async () => {
                    const telegramMessageId = await this.telegramClient.sendMessage(
                      message.scheduledMessagePack.scheduledMessage.content,
                      message.scheduledMessagePack.scheduledMessage.telegramChatId,
                      message.scheduledMessagePack.scheduledMessage.customKeyboard,
                    );
                    sent++;
                    return telegramMessageId;
                  })
                  .then((telegramMessageId) => {
                    return this.messageRepository.setSentById(message.scheduledMessagePack.id, telegramMessageId);
                  })
                  .catch(async (error) => {
                    this.logger.error(error);
                    message.scheduledMessagePack.attempt++;
                    if (message.scheduledMessagePack.attempt === this.MAX_RESCHEDULING_ATTEMPTS) {
                      return this.messageRepository.setStatusById(message.scheduledMessagePack.id, 'failed_to_send');
                    }
                    await this.messageSchedulerRepository.scheduleMessage(
                      message.scheduledMessagePack.id,
                      message.scheduledMessagePack.attempt,
                      message.scheduledMessagePack.scheduledMessage,
                      message.rank,
                    );
                    rescheduled++;
                  });
              }
              promises.push(chain);
            }
          }

          await Promise.all(promises);

          this.logger.info({received, sent, rescheduled});

          lock.unlock((unlockErr) => {
            if (unlockErr) {
              this.logger.error(unlockErr);
            }
            return resolve();
          });
        } catch (err) {
          this.logger.error(err);
          lock.unlock((unlockErr) => {
            if (unlockErr) {
              this.logger.error(unlockErr);
            }
            return resolve();
          });
        }
      });
    });
  }
}
