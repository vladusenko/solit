import {Inject, Injectable} from '@nestjs/common';
import {SeClient} from '../se-client';
import {Question, QuestionRepository} from '../repositories/models/question.repository';
import {ChatRepository} from '../repositories/models/chat.repository';
import {spawn, Pool, Worker} from 'threads';
import {TemplateProvider, templates} from '../template-provider';
import {DateUtil} from '../../utils/date.util';
import {MessageScheduler} from '../message-scheduler/message-scheduler';
import {MESSAGE_CATEGORIES} from '../dictionaries/message-categories';
import * as _ from 'lodash';
import {AnswerAggregationResult, AnswerRepository} from '../repositories/models/answer.repository';
import {LinksByType, MessageService} from '../services/message.service';
import {Message} from '../repositories/models/message.repository';
import {UuidUtil} from '../../utils/uuid.util';
import {InjectPinoLogger, PinoLogger} from 'nestjs-pino/dist';

@Injectable()
export class ReaperCron {
  constructor(
    private readonly seClient: SeClient,
    private readonly questionRepository: QuestionRepository,
    private readonly chatRepository: ChatRepository,
    @Inject('THREADPOOL_MAX_SIZE') private readonly threadPoolMaxSize: number,
    private readonly messageScheduler: MessageScheduler,
    private readonly templateProvider: TemplateProvider,
    private readonly answerRepository: AnswerRepository,
    private readonly messageService: MessageService,
    @Inject('ANALYTICS_BASE_URL') private readonly analyticsEndpoint: string,
    @InjectPinoLogger(ReaperCron.name) private readonly logger: PinoLogger,
  ) {
    this.logger.info(`THREADPOOL_MAX_SIZE: ${this.threadPoolMaxSize}`);
  }

  public async run() {
    const chatsWithPreferences = await this.chatRepository.getChatsWithPreferences();
    if (chatsWithPreferences.length === 0) {
      this.logger.info('No chats found');
      return;
    }
    const questionsPromises = [];
    const answersPromises = [];
    const telegramIdToQuestionsMap = new Map<number, Question[]>();
    const telegramIdToAnswersMap = new Map<number, AnswerAggregationResult[]>();
    for (const chat of chatsWithPreferences) {
      if (_.get(chat, 'preferences.tags.length', 0) === 0) {
        continue;
      }
      questionsPromises.push(
        this.questionRepository.getQuestionsByTagsAndDate(
          chat.preferences.tags,
          DateUtil.oneDayBack(),
          DateUtil.now(),
        )
          .then((questions) => telegramIdToQuestionsMap.set(chat.telegramId, questions)),
      );
      answersPromises.push(
        this.answerRepository.getTopByTagsAndOwnerReputation(
          chat.preferences.tags,
          DateUtil.oneDayBack(),
          DateUtil.now(),
          50000,
          3,
        )
          .then((answers) => telegramIdToAnswersMap.set(chat.telegramId, answers)),
      );
    }

    await Promise.all(questionsPromises);
    await Promise.all(answersPromises);

    const pool = Pool(() => spawn(new Worker('../services/questions-sorter.service.exposable')), this.threadPoolMaxSize);
    for (const [telegramId, questions] of telegramIdToQuestionsMap) {
      pool.queue(async (questionsSorterModule) => {
        const answeredQuestions = await questionsSorterModule.topWithAnswers(
          questions,
          3,
        );

        const nonAnsweredQuestions = await questionsSorterModule.topWithoutAnswers(
          questions,
          3,
        );

        const chat = await this.chatRepository.getByTelegramId(telegramId);
        const renderableAnsweredQuestions = answeredQuestions.map((question) => {
          const id = UuidUtil.v4();
          return {
            ...question,
            id,
            clicked: false,
            analytics_link: `${this.analyticsEndpoint}/${id}`,
          };
        });
        const renderableNonAnsweredQuestions = nonAnsweredQuestions.map((question) => {
          const id = UuidUtil.v4();
          return {
            ...question,
            id,
            clicked: false,
            analytics_link: `${this.analyticsEndpoint}/${id}`,
          };
        });
        const renderableGuruAnswers = telegramIdToAnswersMap.get(telegramId).map((answer) => {
          const id = UuidUtil.v4();
          return {
            ...answer,
            id,
            clicked: false,
            analytics_link: `${this.analyticsEndpoint}/${id}`,
          };
        });
        const messageData: Partial<Message> = {
          content: await this.templateProvider.build(templates.LIT_LIST, {
            answeredQuestions: renderableAnsweredQuestions,
            nonAnsweredQuestions: renderableNonAnsweredQuestions,
            guruAnswers: renderableGuruAnswers,
          }),
          category: MESSAGE_CATEGORIES.LIT_LIST,
          chatId: chat.privateId,
        };

        const linksByType: LinksByType = {
          guruAnswers: renderableGuruAnswers.map((answer) => ({
            id: answer.id,
            address: answer.share_link,
            tags: answer.tags,
            title: answer.title,
          })),
          answeredQuestions: renderableAnsweredQuestions.map((question) => ({
            id: question.id,
            address: question.link,
            tags: question.tags,
            title: question.title,
          })),
          nonAnsweredQuestions: renderableNonAnsweredQuestions.map((question) => ({
            id: question.id,
            address: question.link,
            tags: question.tags,
            title: question.title,
          })),
        };

        const messageId = await this.messageService.createLitListMessage(messageData, linksByType);

        await this.messageScheduler.scheduleMessage({
            ...messageData as Message,
            privateChatId: messageData.chatId,
            telegramChatId: chat.telegramId,
          },
          messageId,
          DateUtil.currentEpoch(),
        );
      });
    }

    await pool.completed();
    await pool.terminate();
    this.logger.info({chatsAmount: telegramIdToQuestionsMap.size});
  }
}
