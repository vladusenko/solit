import {Test, TestingModule} from '@nestjs/testing';
import {SeClient} from '../se-client';
import {Question, QuestionRepository} from '../repositories/models/question.repository';
import {ChatRepository} from '../repositories/models/chat.repository';
import {ReaperCronStateRepository} from '../repositories/crons/reaper-cron.state.repository';
import {TemplateProvider} from '../template-provider';
import {ReaperCron} from './reaper.cron';
import * as sinon from 'sinon';
import {MessageScheduler} from '../message-scheduler/message-scheduler';
import {MessageSchedulerRepository} from '../repositories/message-scheduler.repository';
import {AnswerAggregationResult, AnswerRepository} from '../repositories/models/answer.repository';
import {MessageService} from '../services/message.service';
import {MessageRepository} from '../repositories/models/message.repository';
import {LinkRepository} from '../repositories/models/link.repository';
import {TelegramClient} from '../services/telegram-client.service';
import {UuidUtil} from '../../utils/uuid.util';
import {DateUtil} from '../../utils/date.util';
import {LoggerModule} from 'nestjs-pino/dist';

const questionsList: Array<Partial<Question>> = [{
  tags: ['javascript', 'node.js'],
  title: 'q1',
  link: 'link1',
  score: 0,
  answer_count: 1,
  is_answered: false,
  view_count: 54,
  question_id: 1,
}, {
  tags: ['typescript'],
  title: 'q2',
  link: 'link2',
  score: 1,
  answer_count: 2,
  is_answered: true,
  view_count: 67,
  question_id: 2,
}, {
  tags: ['java'],
  title: 'q3',
  link: 'link3',
  score: 2,
  answer_count: 3,
  is_answered: false,
  view_count: 120,
  question_id: 3,
}, {
  tags: ['python'],
  title: 'q4',
  link: 'link4',
  score: 4,
  answer_count: 5,
  is_answered: true,
  view_count: 110,
  question_id: 4,
}];

const answersList: AnswerAggregationResult[] = [{
  tags: ['javascript', 'node.js'],
  title: 'q1',
  share_link: 'http://linkq1.com',
  score: 1,
  answer_id: 1,
  question_id: 1,
  question_link: 'link1',
  owner: {
    reputation: 50001,
  },
}, {
  tags: ['typescript'],
  title: 'q2',
  share_link: 'http://linkq2.com',
  score: 2,
  answer_id: 2,
  question_id: 2,
  question_link: 'link2',
  owner: {
    reputation: 60000,
  },
}, {
  tags: ['java'],
  title: 'q3',
  share_link: 'http://linkq3.com',
  score: 2,
  answer_id: 3,
  question_id: 3,
  question_link: 'link3',
  owner: {
    reputation: 70000,
  },
}, {
  tags: ['python'],
  title: 'q4',
  share_link: 'http://linkq4.com',
  score: 0,
  answer_id: 4,
  question_id: 4,
  question_link: 'link4',
  owner: {
    reputation: 50001,
  },
}];

describe('ReaperCron unit tests', () => {
  let seClient: SeClient;
  let questionRepository: QuestionRepository;
  let answerRepository: AnswerRepository;
  let chatRepository: ChatRepository;
  let reaperCronStateRepository: ReaperCronStateRepository;
  let messageScheduler: MessageScheduler;
  let templateProvider: TemplateProvider;
  let reaperCron: ReaperCron;
  let messageService: MessageService;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [
        LoggerModule.forRoot(),
      ],
      providers: [
        SeClient, QuestionRepository, ChatRepository, ReaperCronStateRepository,
        TemplateProvider, MessageScheduler, MessageSchedulerRepository, AnswerRepository,
        MessageService, MessageRepository, LinkRepository, TelegramClient, ReaperCron,
        {
          provide: 'SequelizeToken',
          useValue: {
            transaction: (options, fun) => {
              return fun();
            },
          },
        },
        {
          provide: 'RedisToken',
          useValue: {},
        },
        {
          provide: 'TELEGRAM_TOKEN',
          useValue: 'tg_token',
        },
        {
          provide: 'TELEGRAM_API_HOST',
          useValue: 'tghost',
        },
        {
          provide: 'SE_API_URL',
          useValue: 'se_api',
        },
        {
          provide: 'SE_API_KEY',
          useValue: 'se_api_key',
        },
        {
          provide: 'SE_FILTER_NAME',
          useValue: 'se_filter_name',
        },
        {
          provide: 'ANALYTICS_BASE_URL',
          useValue: 'http://some.domain/analytics',
        },
        {
          provide: 'ChatModel',
          useValue: {},
        },
        {
          provide: 'PreferenceModel',
          useValue: {},
        },
        {
          provide: 'QuestionModel',
          useValue: {},
        },
        {
          provide: 'AnswerModel',
          useValue: {},
        },
        {
          provide: 'MessageModel',
          useValue: {},
        },
        {
          provide: 'LinkModel',
          useValue: {},
        },
        {
          provide: 'THREADPOOL_MAX_SIZE',
          useValue: 1, // HOTFIX to avoid race conditions in tests
        },
      ],
    }).compile();

    seClient = app.get(SeClient);
    questionRepository = app.get(QuestionRepository);
    answerRepository = app.get(AnswerRepository);
    chatRepository = app.get(ChatRepository);
    reaperCronStateRepository = app.get(ReaperCronStateRepository);
    messageScheduler = app.get(MessageScheduler);
    templateProvider = app.get(TemplateProvider);
    reaperCron = app.get(ReaperCron);
    messageService = app.get(MessageService);
  });

  describe('ReaperCron.run', () => {
    it('should successfully send messages with processed questions', async () => {
      sinon.stub(UuidUtil, 'v4').callsFake(() => 'fake_uuid');
      sinon.stub(DateUtil, 'currentEpoch').callsFake(() => 12345678);
      sinon.stub(chatRepository, 'getByTelegramId').callsFake((telegramId) => {
        return Promise.resolve({
          telegramId,
          privateId: telegramId === 123456 ? 1 : 2,
        });
      });
      const chatRepoGetChatsSpy = sinon.stub(chatRepository, 'getChatsWithPreferences').callsFake(() => Promise.resolve([
        {
          privateId: 1,
          telegramId: 123456,
          preferences: {
            tags: ['javascript', 'java', 'typescript'],
          },
        },
        {
          privateId: 2,
          telegramId: 654321,
          preferences: {
            tags: ['node.js', 'v8'],
          },
        },
      ]));
      const questionRepoGeQuestionsByTagsAndDateSpy = sinon.stub(questionRepository, 'getQuestionsByTagsAndDate')
        .callsFake(() => Promise.resolve(questionsList));
      const answerRepoGetTopByTagsAndOwnerReputationSpy = sinon.stub(answerRepository, 'getTopByTagsAndOwnerReputation')
        .callsFake(() => Promise.resolve(answersList));
      const createLitListMessageSpy = sinon.stub(messageService, 'createLitListMessage').callsFake(() => Promise.resolve(12345));
      const messageSchedulerSpy = sinon.stub(messageScheduler, 'scheduleMessage').callsFake(() => Promise.resolve());

      await reaperCron.run();

      expect(chatRepoGetChatsSpy.calledOnce).toBe(true);
      expect(questionRepoGeQuestionsByTagsAndDateSpy.callCount).toBe(2);
      expect(answerRepoGetTopByTagsAndOwnerReputationSpy.callCount).toBe(2);
      expect(createLitListMessageSpy.callCount).toBe(2);
      expect(messageSchedulerSpy.callCount).toBe(2);
      expect(messageSchedulerSpy.firstCall.args).toEqual([
        {
          content: '📥 What\'s up? Here\'s the daily top StackOverflow content for you!\n' +
            '\n' +
            '<b>Top 4 guru answers:</b>\n' +
            '<code>[javascript,node.js]</code>\n' +
            '🔥 q1\n' +
            '<a href="http://some.domain/analytics/fake_uuid">🚀 Lit link</a>\n' +
            '\n' +
            '<code>[typescript]</code>\n' +
            '🔥 q2\n' +
            '<a href="http://some.domain/analytics/fake_uuid">🚀 Lit link</a>\n' +
            '\n' +
            '<code>[java]</code>\n' +
            '🔥 q3\n' +
            '<a href="http://some.domain/analytics/fake_uuid">🚀 Lit link</a>\n' +
            '\n' +
            '<code>[python]</code>\n' +
            '🔥 q4\n' +
            '<a href="http://some.domain/analytics/fake_uuid">🚀 Lit link</a>\n' +
            '\n' +
            '<b>Top 2 answered questions:</b>\n' +
            '<code>[python]</code>\n' +
            '🔥 q4\n' +
            '<a href="http://some.domain/analytics/fake_uuid">🚀 Lit link</a>\n' +
            '\n' +
            '<code>[typescript]</code>\n' +
            '🔥 q2\n' +
            '<a href="http://some.domain/analytics/fake_uuid">🚀 Lit link</a>\n' +
            '\n' +
            '<b>Top 2 non answered questions:</b>\n' +
            '<code>[java]</code>\n' +
            '🔥 q3\n' +
            '<a href="http://some.domain/analytics/fake_uuid">🚀 Lit link</a>\n' +
            '\n' +
            '<code>[javascript,node.js]</code>\n' +
            '🔥 q1\n' +
            '<a href="http://some.domain/analytics/fake_uuid">🚀 Lit link</a>\n' +
            '\n' +
            '\n' +
            'See you tomorrow! 👋\n',
          category: 'lit_list',
          telegramChatId: 123456,
          chatId: 1,
          privateChatId: 1,
        },
        12345,
        DateUtil.currentEpoch(),
      ]);
      expect(messageSchedulerSpy.secondCall.args).toEqual([
        {
          content: '📥 What\'s up? Here\'s the daily top StackOverflow content for you!\n' +
            '\n' +
            '<b>Top 4 guru answers:</b>\n' +
            '<code>[javascript,node.js]</code>\n' +
            '🔥 q1\n' +
            '<a href="http://some.domain/analytics/fake_uuid">🚀 Lit link</a>\n' +
            '\n' +
            '<code>[typescript]</code>\n' +
            '🔥 q2\n' +
            '<a href="http://some.domain/analytics/fake_uuid">🚀 Lit link</a>\n' +
            '\n' +
            '<code>[java]</code>\n' +
            '🔥 q3\n' +
            '<a href="http://some.domain/analytics/fake_uuid">🚀 Lit link</a>\n' +
            '\n' +
            '<code>[python]</code>\n' +
            '🔥 q4\n' +
            '<a href="http://some.domain/analytics/fake_uuid">🚀 Lit link</a>\n' +
            '\n' +
            '<b>Top 2 answered questions:</b>\n' +
            '<code>[python]</code>\n' +
            '🔥 q4\n' +
            '<a href="http://some.domain/analytics/fake_uuid">🚀 Lit link</a>\n' +
            '\n' +
            '<code>[typescript]</code>\n' +
            '🔥 q2\n' +
            '<a href="http://some.domain/analytics/fake_uuid">🚀 Lit link</a>\n' +
            '\n' +
            '<b>Top 2 non answered questions:</b>\n' +
            '<code>[java]</code>\n' +
            '🔥 q3\n' +
            '<a href="http://some.domain/analytics/fake_uuid">🚀 Lit link</a>\n' +
            '\n' +
            '<code>[javascript,node.js]</code>\n' +
            '🔥 q1\n' +
            '<a href="http://some.domain/analytics/fake_uuid">🚀 Lit link</a>\n' +
            '\n' +
            '\n' +
            'See you tomorrow! 👋\n',
          category: 'lit_list',
          telegramChatId: 654321,
          chatId: 2,
          privateChatId: 2,
        },
        12345,
        DateUtil.currentEpoch(),
      ]);
    });

    it('should exit if no chats are available', async () => {
      const chatRepoGetChatsSpy = sinon.stub(chatRepository, 'getChatsWithPreferences')
        .callsFake(() => Promise.resolve([]));
      const questionRepoGeQuestionsByTagsAndDateSpy = sinon.spy(questionRepository, 'getQuestionsByTagsAndDate');
      const messageSenderSendSpy = sinon.spy(messageScheduler, 'scheduleMessage');

      await reaperCron.run();

      expect(chatRepoGetChatsSpy.calledOnce).toBe(true);
      expect(questionRepoGeQuestionsByTagsAndDateSpy.notCalled).toBe(true);
      expect(messageSenderSendSpy.notCalled).toBe(true);
    });

    // @TODO fix in SL-105
    it.skip('should not send empty messages if no sendable questions were produced', async () => {
      const chatRepoGetChatsSpy = sinon.stub(chatRepository, 'getChatsWithPreferences')
        .callsFake(() => Promise.resolve([
          {
            telegramId: 123456,
            preferences: {
              tags: ['node.js', 'v8'],
            },
          },
        ]));
      const questionRepoGeQuestionsByTagsAndDateSpy = sinon.stub(questionRepository, 'getQuestionsByTagsAndDate')
        .callsFake(() => Promise.resolve([]));
      const messageSchedulerSpy = sinon.spy(messageScheduler, 'scheduleMessage');

      await reaperCron.run();

      expect(chatRepoGetChatsSpy.calledOnce).toBe(true);
      expect(questionRepoGeQuestionsByTagsAndDateSpy.calledOnce).toBe(true);
      expect(messageSchedulerSpy.notCalled).toBe(true);
    });
  });
});
