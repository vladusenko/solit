import {Injectable} from '@nestjs/common';
import {SeAnswer, SeClient, SeQuestion} from '../se-client';
import {QuestionRepository} from '../repositories/models/question.repository';
import * as _ from 'lodash';
import {spawn, Thread, Worker} from 'threads';
import {ChunkProcessor} from './chunk-processor';
import {AnswerRepository} from '../repositories/models/answer.repository';
import {InjectPinoLogger, PinoLogger} from 'nestjs-pino/dist';

@Injectable()
export class UpdaterCron {
  constructor(
    private readonly seClient: SeClient,
    private readonly questionRepository: QuestionRepository,
    private readonly chunkProcessor: ChunkProcessor,
    private readonly answerRepository: AnswerRepository,
    @InjectPinoLogger(UpdaterCron.name) private readonly logger: PinoLogger,
  ) {}

  public async run() {
    const interval = setInterval(() => {
      this.logger.info(process.memoryUsage());
    }, 5000);
    const yesterdayQuestionIds = await this.questionRepository.getYesterdayQuestionSeIds();
    try {
      this.logger.info({
        yesterdayQuestionIds,
      });
      this.logger.info({yesterdayQuestionsAmount: yesterdayQuestionIds.length});
    } catch (err) {
      this.logger.error(err);
    }
    if (yesterdayQuestionIds.length === 0) {
      this.logger.info('No questions to update');
      return;
    }
    const chunks = _.chunk(yesterdayQuestionIds, 30);
    const chunkSeries = _.chunk(chunks, 5);
    const currentQuestionsStates: SeQuestion[] = await this.chunkProcessor.process<SeQuestion>(
      chunkSeries,
      this.seClient.getQuestionsCurrentState,
    );
    const currentQuestionsStatesIds = currentQuestionsStates.map((state) => state.question_id);
    const looseArrayDifference = await spawn(new Worker('../../utils/bigint.util.exposable'));
    const staleQuestionIds = await looseArrayDifference(yesterdayQuestionIds, currentQuestionsStatesIds);
    this.logger.info({staleQuestionsAmount: staleQuestionIds.length});
    await Thread.terminate(looseArrayDifference);
    if (staleQuestionIds.length > 0) {
      const staleDeletedCount = await this.questionRepository.bulkDelete(staleQuestionIds);
      this.logger.info({source: UpdaterCron.name, data: {deleted: staleDeletedCount}});
      try {
        this.logger.info({
          source: UpdaterCron.name,
          data: {
            staleQuestionIds,
          },
        });
      } catch (err) {
        this.logger.error(err);
      }
    }
    if (currentQuestionsStates.length > 0) {
      const updatedCount = await this.questionRepository.bulkUpdate(currentQuestionsStates);
      this.logger.info({updated: updatedCount});
    } else {
      this.logger.info({updated: 0});
    }
    if (currentQuestionsStatesIds.length > 0) {
      const answers = await this.chunkProcessor.process<SeAnswer>(
        chunkSeries,
        this.seClient.getAnswers,
      );
      this.logger.info('73');
      this.logger.info(`Collected ${answers.length} answers`);
      await this.answerRepository.bulkCreate(answers);
    }
    clearInterval(interval);
  }
}
