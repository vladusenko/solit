import {Injectable} from '@nestjs/common';
import { SeClient, SeQuestion } from '../se-client';
import * as moment from 'moment';
import { QuestionRepository } from '../repositories/models/question.repository';
import { FetcherCronStateRepository } from '../repositories/crons/fetcher-cron-state.repository';
import {InjectPinoLogger, PinoLogger} from 'nestjs-pino/dist';

@Injectable()
export class FetcherCron {
  constructor(
    private readonly seClient: SeClient,
    private readonly questionRepository: QuestionRepository,
    private readonly fetcherCronStateRepository: FetcherCronStateRepository,
    @InjectPinoLogger(FetcherCron.name) private readonly logger: PinoLogger,
  ) {}

  public async run() {
    let lastQueried = Math.floor(moment().subtract(1, 'days').endOf('day').toDate().getTime() / 1000); // today midnight
    if (!(await this.fetcherCronStateRepository.setLastQueriedEpochIfNotExists(lastQueried))) {
      lastQueried = await this.fetcherCronStateRepository.getLastQueriedEpoch();
    }

    let hasMore = true;
    let page = 0;
    let seQuestions: SeQuestion[] = [];
    const tomorrowMidnight = Math.floor(moment().add(1, 'days').endOf('day').toDate().getTime() / 1000);
    while (hasMore) {
      page++;
      const response = await this.seClient.getQuestions(lastQueried, page, tomorrowMidnight);
      seQuestions = seQuestions.concat(response.items);
      hasMore = response.has_more;
    }

    await this.questionRepository.bulkCreate(seQuestions);

    this.logger.info({fetched: seQuestions.length});

    await this.fetcherCronStateRepository.setLastQueriedEpoch(Math.floor(new Date().getTime() / 1000));
  }
}
