import {Test, TestingModule} from '@nestjs/testing';
import {SeClient} from '../se-client';
import {QuestionRepository} from '../repositories/models/question.repository';
import {UpdaterCronStateRepository} from '../repositories/crons/updater-cron-state.repository';
import {UpdaterCron} from './updater.cron';
import * as sinon from 'sinon';
import {ChunkProcessor} from './chunk-processor';
import {AnswerRepository} from '../repositories/models/answer.repository';
import {LoggerModule} from 'nestjs-pino/dist';

// @ts-ignore
BigInt.prototype.toJSON = function() {
  return this.toString();
};
describe('UpdaterCron unit tests', () => {
  let seClient: SeClient;
  let questionRepository: QuestionRepository;
  let answerRepository: AnswerRepository;
  let updaterCronStateRepository: UpdaterCronStateRepository;
  let updaterCron: UpdaterCron;

  const fakeTodayQuestionIds: number[] = [];
  for (let i = 5000000; i < 5001000; i++) {
    fakeTodayQuestionIds.push(i);
  }
  const fakeStaleQuestionIds: number[] = [
    5000000, 5000006, 5000034,
  ];

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [
        LoggerModule.forRoot(),
      ],
      providers: [
        SeClient, UpdaterCronStateRepository, QuestionRepository, ChunkProcessor, AnswerRepository, UpdaterCron,
        {
          provide: 'SequelizeToken',
          useValue: {
            transaction: (options, fun) => {
              return fun();
            },
          },
        },
        {
          provide: 'RedisToken',
          useValue: {},
        },
        {
          provide: 'TELEGRAM_TOKEN',
          useValue: 'tg_token',
        },
        {
          provide: 'TELEGRAM_API_HOST',
          useValue: 'tghost',
        },
        {
          provide: 'SE_API_URL',
          useValue: 'se_api',
        },
        {
          provide: 'SE_API_KEY',
          useValue: 'se_api_key',
        },
        {
          provide: 'SE_FILTER_NAME',
          useValue: 'se_filter_name',
        },
        {
          provide: 'ChatModel',
          useValue: {},
        },
        {
          provide: 'PreferenceModel',
          useValue: {},
        },
        {
          provide: 'QuestionModel',
          useValue: {},
        },
        {
          provide: 'AnswerModel',
          useValue: {},
        }],
    }).compile();

    seClient = app.get(SeClient);
    questionRepository = app.get(QuestionRepository);
    answerRepository = app.get(AnswerRepository);
    updaterCronStateRepository = app.get(UpdaterCronStateRepository);
    updaterCron = app.get(UpdaterCron);
  });

  describe('UpdaterCron.run', () => {
    it('should update stored questions and fetch their answers', async () => {
      const questionRepositoryGetYesterdayQuestionIdsSpy = sinon.stub(questionRepository, 'getYesterdayQuestionSeIds')
        .callsFake(() => Promise.resolve(fakeTodayQuestionIds));
      const seClientGetQuestionsCurrentStateSpy = sinon.stub(seClient, 'getQuestionsCurrentState').callsFake((ids) => {
        const response = [];
        ids.forEach((id) => response.push({question_id: id}));
        return response;
      });
      const seClientGetAnswersSpy = sinon.stub(seClient, 'getAnswers').callsFake((ids) => {
        const response = [];
        ids.forEach((id) => response.push({question_id: id}));
        return response;
      });
      const answerRepositoryBulkCreate = sinon.stub(answerRepository, 'bulkCreate').callsFake(() => Promise.resolve());
      const questionRepositoryBulkDeleteSpy = sinon.stub(questionRepository, 'bulkDelete').callsFake((ids) => ids.length);
      const questionRepositoryBulkUpdateSpy = sinon.stub(questionRepository, 'bulkUpdate').callsFake((ids) => ids.length);

      await updaterCron.run();

      expect(questionRepositoryGetYesterdayQuestionIdsSpy.calledOnce).toBe(true);
      expect(seClientGetQuestionsCurrentStateSpy.callCount).toBe(Math.ceil(1000 / 30));
      expect(seClientGetAnswersSpy.callCount).toBe(Math.ceil(1000 / 30));
      expect(answerRepositoryBulkCreate.calledOnce).toBe(true);
      expect(questionRepositoryBulkDeleteSpy.notCalled).toBe(true);
      expect(questionRepositoryBulkUpdateSpy.calledOnce).toBe(true);
    }, 35000);

    it('should update stored questions, fetch their answers and delete stale ones', async () => {
      const questionRepositoryGetYesterdayQuestionIdsSpy = sinon.stub(questionRepository, 'getYesterdayQuestionSeIds')
        .callsFake(() => Promise.resolve(fakeTodayQuestionIds));
      const seClientGetQuestionsCurrentStateSpy = sinon.stub(seClient, 'getQuestionsCurrentState').callsFake((ids) => {
        const response = [];
        ids.forEach((id) => {
          if (fakeStaleQuestionIds.includes(id)) {
            return;
          } else {
            response.push({question_id: id});
          }
        });
        return response;
      });
      const seClientGetAnswersSpy = sinon.stub(seClient, 'getAnswers').callsFake((ids: number[]) => {
        const response = [];
        ids.forEach((id) => response.push({question_id: id}));
        return response;
      });
      const answerRepositoryBulkCreate = sinon.stub(answerRepository, 'bulkCreate').callsFake(() => Promise.resolve());
      const questionRepositoryBulkDeleteSpy = sinon.stub(questionRepository, 'bulkDelete').callsFake((ids) => ids.length);
      const questionRepositoryBulkUpdateSpy = sinon.stub(questionRepository, 'bulkUpdate').callsFake((ids) => ids.length);

      await updaterCron.run();

      expect(questionRepositoryGetYesterdayQuestionIdsSpy.calledOnce).toBe(true);
      expect(seClientGetQuestionsCurrentStateSpy.callCount).toBe(Math.ceil(1000 / 30));
      expect(seClientGetAnswersSpy.callCount).toBe(Math.ceil(1000 / 30));
      expect(answerRepositoryBulkCreate.calledOnce).toBe(true);
      expect(questionRepositoryBulkDeleteSpy.calledOnceWith(fakeStaleQuestionIds)).toBe(true);
      expect(questionRepositoryBulkUpdateSpy.calledOnce).toBe(true);
    }, 35000);

    it('should not update questions states and make api calls if there are no questions stored', async () => {
      const questionRepositoryGetYesterdayQuestionIdsSpy = sinon.stub(questionRepository, 'getYesterdayQuestionSeIds')
        .callsFake(() => Promise.resolve([]));
      const seClientGetQuestionsCurrentStateSpy = sinon.spy(seClient, 'getQuestionsCurrentState');
      const seClientGetAnswersSpy = sinon.spy(seClient, 'getAnswers');
      const answerRepositoryBulkCreate = sinon.stub(answerRepository, 'bulkCreate').callsFake(() => Promise.resolve());
      const questionRepositoryBulkDeleteSpy = sinon.stub(questionRepository, 'bulkDelete').callsFake((ids) => ids.length);
      const questionRepositoryBulkUpdateSpy = sinon.stub(questionRepository, 'bulkUpdate').callsFake((ids) => ids.length);

      await updaterCron.run();

      expect(questionRepositoryGetYesterdayQuestionIdsSpy.calledOnce).toBe(true);
      expect(seClientGetQuestionsCurrentStateSpy.notCalled).toBe(true);
      expect(seClientGetAnswersSpy.notCalled).toBe(true);
      expect(answerRepositoryBulkCreate.notCalled).toBe(true);
      expect(questionRepositoryBulkDeleteSpy.notCalled).toBe(true);
      expect(questionRepositoryBulkUpdateSpy.notCalled).toBe(true);
    });
  });
});
