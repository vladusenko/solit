import { Test, TestingModule } from '@nestjs/testing';
import { SeClient, SeQuestion } from '../se-client';
import { FetcherCronStateRepository } from '../repositories/crons/fetcher-cron-state.repository';
import { QuestionRepository } from '../repositories/models/question.repository';
import * as sinon from 'sinon';
import { FetcherCron } from './fetcher.cron';
import { UuidUtil } from '../../utils/uuid.util';
import {LoggerModule} from 'nestjs-pino/dist';

const nodeJsAndJsQuestion: SeQuestion = {
  tags: [
    'node.js',
    'javascript',
  ],
  owner: {
    reputation: 1505,
    user_id: 10064334,
    user_type: 'registered',
    profile_image: 'https://i.stack.imgur.com/CTqAD.jpg?s=128&g=1',
    display_name: 'MichaelB',
    link: 'https://stackoverflow.com/users/10064334/michaelb',
  },
  is_answered: false,
  view_count: 56,
  closed_date: 1564116993,
  answer_count: 1,
  score: 3,
  last_activity_date: 1564149646,
  creation_date: 1564064052,
  last_edit_date: 1564149646,
  question_id: 57204092,
  link: 'https://link.com',
  closed_reason: 'duplicate',
  title: 'Node.js and Javascript question',
};
const javaQuestion: SeQuestion = {
  tags: [
    'java',
  ],
  owner: {
    reputation: 1505,
    user_id: 10064334,
    user_type: 'registered',
    profile_image: 'https://i.stack.imgur.com/CTqAD.jpg?s=128&g=1',
    display_name: 'MichaelB',
    link: 'https://stackoverflow.com/users/10064334/michaelb',
  },
  is_answered: false,
  view_count: 40,
  answer_count: 1,
  score: 2,
  last_activity_date: 1564149646,
  creation_date: 1564064052,
  last_edit_date: 1564149646,
  question_id: 57204093,
  link: 'https://link.com',
  title: 'Java questions',
};

const jsQuestion: SeQuestion = {
  tags: [
    'javascript',
  ],
  owner: {
    reputation: 1505,
    user_id: 10064334,
    user_type: 'registered',
    profile_image: 'https://i.stack.imgur.com/CTqAD.jpg?s=128&g=1',
    display_name: 'MichaelB',
    link: 'https://stackoverflow.com/users/10064334/michaelb',
  },
  is_answered: false,
  view_count: 37,
  answer_count: 2,
  score: 1,
  last_activity_date: 1564149646,
  creation_date: 1564064052,
  last_edit_date: 1564149646,
  question_id: 57204094,
  link: 'https://link.com',
  title: 'Javascript question',
};
const nodeJsQuestion: SeQuestion = {
  tags: [
    'node.js',
  ],
  owner: {
    reputation: 1505,
    user_id: 10064334,
    user_type: 'registered',
    profile_image: 'https://i.stack.imgur.com/CTqAD.jpg?s=128&g=1',
    display_name: 'MichaelB',
    link: 'https://stackoverflow.com/users/10064334/michaelb',
  },
  is_answered: false,
  view_count: 67,
  answer_count: 3,
  score: 4,
  last_activity_date: 1564149646,
  creation_date: 1564064052,
  last_edit_date: 1564149646,
  question_id: 57204095,
  link: 'https://link.com',
  title: 'Node.js question',
};

describe('FetcherCron unit tests', () => {
  let seClient: SeClient;
  let questionRepository: QuestionRepository;
  let fetcherCronStateRepository: FetcherCronStateRepository;
  let fetcherCron: FetcherCron;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [
        LoggerModule.forRoot(),
      ],
      providers: [
        SeClient, FetcherCronStateRepository, QuestionRepository, FetcherCron,
        {
          provide: 'SequelizeToken',
          useValue: {
            transaction: (options, fun) => {
              return fun();
            },
          },
        },
        {
          provide: 'RedisToken',
          useValue: {},
        },
        {
          provide: 'TELEGRAM_TOKEN',
          useValue: 'tg_token',
        },
        {
          provide: 'TELEGRAM_API_HOST',
          useValue: 'tghost',
        },
        {
          provide: 'SE_API_URL',
          useValue: 'se_api',
        },
        {
          provide: 'SE_API_KEY',
          useValue: 'se_api_key',
        },
        {
          provide: 'SE_FILTER_NAME',
          useValue: 'se_filter_name',
        },
        {
          provide: 'ChatModel',
          useValue: {},
        },
        {
          provide: 'PreferenceModel',
          useValue: {},
        },
        {
          provide: 'QuestionModel',
          useValue: {},
        }],
    }).compile();

    seClient = app.get(SeClient);
    questionRepository = app.get(QuestionRepository);
    fetcherCronStateRepository = app.get(FetcherCronStateRepository);
    fetcherCron = app.get(FetcherCron);
  });

  describe('run tests', () => {
    it('should successfully fetch questions and save them, when last queried epoch is available', async () => {
      const fetcherCronStateRepositorySetLastQueriedEpochIfNotExistsSpy = sinon.stub(fetcherCronStateRepository, 'setLastQueriedEpochIfNotExists')
        .callsFake(() => Promise.resolve(false));
      const fetcherCronStateRepositoryGetLastQueriedEpoch = sinon.stub(fetcherCronStateRepository, 'getLastQueriedEpoch')
        .callsFake(() => Promise.resolve(123456789));
      const seClientGetQuestionsSpy = sinon.stub(seClient, 'getQuestions').callsFake(() => Promise.resolve({
        items: [nodeJsQuestion, javaQuestion, jsQuestion, nodeJsAndJsQuestion],
        has_more: false,
      }));
      const questionRepositoryBulkCreateSpy = sinon.stub(questionRepository, 'bulkCreate').callsFake(() => Promise.resolve());
      const fetcherCronStateRepositorySetLastQueriedEpochSpy = sinon.stub(fetcherCronStateRepository, 'setLastQueriedEpoch')
        .callsFake(() => Promise.resolve());
      sinon.stub(UuidUtil, 'v4').callsFake(() => 'fake_uuid');

      await fetcherCron.run();

      expect(fetcherCronStateRepositorySetLastQueriedEpochIfNotExistsSpy.calledOnce).toEqual(true);
      expect(fetcherCronStateRepositoryGetLastQueriedEpoch.calledOnce).toEqual(true);
      expect(seClientGetQuestionsSpy.calledOnce).toEqual(true);
      expect(questionRepositoryBulkCreateSpy.calledOnceWith([nodeJsQuestion, javaQuestion, jsQuestion, nodeJsAndJsQuestion])).toEqual(true);
      expect(fetcherCronStateRepositorySetLastQueriedEpochSpy.calledOnce).toEqual(true);

      // tslint:disable-next-line:no-string-literal
      UuidUtil.v4['restore']();
    });

    it('should query questions several times if SE responds with has more flag', async () => {
      const fetcherCronStateRepositorySetLastQueriedEpochIfNotExistsSpy = sinon.stub(fetcherCronStateRepository, 'setLastQueriedEpochIfNotExists')
        .callsFake(() => Promise.resolve(false));
      const fetcherCronStateRepositoryGetLastQueriedEpoch = sinon.stub(fetcherCronStateRepository, 'getLastQueriedEpoch')
        .callsFake(() => Promise.resolve(123456789));
      const seClientGetQuestionsSpy = sinon.stub(seClient, 'getQuestions');
      seClientGetQuestionsSpy.onCall(0).returns({
        items: [nodeJsQuestion, javaQuestion],
        has_more: true,
      });
      seClientGetQuestionsSpy.onCall(1).returns({
        items: [jsQuestion, nodeJsAndJsQuestion],
        has_more: false,
      });
      const questionRepositoryBulkCreateSpy = sinon.stub(questionRepository, 'bulkCreate').callsFake(() => Promise.resolve());
      const fetcherCronStateRepositorySetLastQueriedEpochSpy = sinon.stub(fetcherCronStateRepository, 'setLastQueriedEpoch')
        .callsFake(() => Promise.resolve());
      sinon.stub(UuidUtil, 'v4').callsFake(() => 'fake_uuid');

      await fetcherCron.run();

      expect(fetcherCronStateRepositorySetLastQueriedEpochIfNotExistsSpy.calledOnce).toEqual(true);
      expect(fetcherCronStateRepositoryGetLastQueriedEpoch.calledOnce).toEqual(true);
      expect(seClientGetQuestionsSpy.calledTwice).toEqual(true);
      expect(questionRepositoryBulkCreateSpy.calledOnceWith([nodeJsQuestion, javaQuestion, jsQuestion, nodeJsAndJsQuestion])).toEqual(true);
      expect(fetcherCronStateRepositorySetLastQueriedEpochSpy.calledOnce).toEqual(true);

      // tslint:disable-next-line:no-string-literal
      UuidUtil.v4['restore']();
    });

    it('should not fetch last queried epoch if it was just set', async () => {
      const fetcherCronStateRepositorySetLastQueriedEpochIfNotExistsSpy = sinon.stub(fetcherCronStateRepository, 'setLastQueriedEpochIfNotExists')
        .callsFake(() => Promise.resolve(true));
      const fetcherCronStateRepositoryGetLastQueriedEpoch = sinon.stub(fetcherCronStateRepository, 'getLastQueriedEpoch')
        .callsFake(() => Promise.resolve(123456789));
      const seClientGetQuestionsSpy = sinon.stub(seClient, 'getQuestions').callsFake(() => Promise.resolve({
        items: [nodeJsQuestion, javaQuestion, jsQuestion, nodeJsAndJsQuestion],
        has_more: false,
      }));
      const questionRepositoryBulkCreateSpy = sinon.stub(questionRepository, 'bulkCreate').callsFake(() => Promise.resolve());
      const fetcherCronStateRepositorySetLastQueriedEpochSpy = sinon.stub(fetcherCronStateRepository, 'setLastQueriedEpoch')
        .callsFake(() => Promise.resolve());
      sinon.stub(UuidUtil, 'v4').callsFake(() => 'fake_uuid');

      await fetcherCron.run();

      expect(fetcherCronStateRepositorySetLastQueriedEpochIfNotExistsSpy.calledOnce).toEqual(true);
      expect(fetcherCronStateRepositoryGetLastQueriedEpoch.notCalled).toEqual(true);
      expect(seClientGetQuestionsSpy.calledOnce).toEqual(true);
      expect(questionRepositoryBulkCreateSpy.calledOnceWith([nodeJsQuestion, javaQuestion, jsQuestion, nodeJsAndJsQuestion])).toEqual(true);
      expect(fetcherCronStateRepositorySetLastQueriedEpochSpy.calledOnce).toEqual(true);

      // tslint:disable-next-line:no-string-literal
      UuidUtil.v4['restore']();
    });
  });
});
