import * as _ from 'lodash';
import {Injectable} from '@nestjs/common';
import {wait} from '../../utils/generic-utils';

@Injectable()
export class ChunkProcessor {
  public async process<T>(chunkSeries: number[][][], runnable: (chunk: number[]) => Promise<T | T[]>): Promise<T[]> {
    const data: T[] = [];
    for (const series of chunkSeries) {
      const promises = [];
      for (const chunk of series) {
        promises.push(runnable(chunk));
      }
      data.push(... _.flatten(await Promise.all(promises)));
      await wait(2000);
    }
    return data;
  }
}
