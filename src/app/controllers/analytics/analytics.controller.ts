import {Controller, Get, HttpException, HttpStatus, Param, Res} from '@nestjs/common';
import {AnalyticsService} from '../../services/analytics.service';
import { Response } from 'express';
import {LinkService} from '../../services/link.service';
import {EntityNotFoundError} from '../../errors/entity-not-found.error';
import {MessageService} from '../../services/message.service';
import {InjectPinoLogger, PinoLogger} from 'nestjs-pino/dist';

@Controller('/analytics')
export class AnalyticsController {
  constructor(
    private readonly analyticsService: AnalyticsService,
    private readonly linkService: LinkService,
    private readonly messageService: MessageService,
    @InjectPinoLogger(AnalyticsController.name) private readonly logger: PinoLogger,
  ) {}

  @Get('/:id')
  public async analytics(@Param() params: {id: string}, @Res() response: Response) {
    let headersSent = false;
    try {
      const linkInstance = await this.linkService.getById(params.id);
      response.redirect(linkInstance.address);
      headersSent = true;
      await this.analyticsService.incrementClickCount(linkInstance.id);
      await this.messageService.updateMessageViewByLinkId(linkInstance.id);
    } catch (error) {
      this.logger.error(error);
      if (error instanceof EntityNotFoundError && !headersSent) {
        throw new HttpException('Link is not recognised', HttpStatus.NOT_FOUND);
      }

      if (!headersSent) {
        throw error;
      }
    }
  }
}
