import {Body, Controller, HttpCode, Post, UseFilters} from '@nestjs/common';
import { TelegramWebhookDto } from './telegram-webhook.dto';
import { ValidationPipe422 } from '../../../pipes/validation-422.pipe';
import { CommandHandlersRegistry } from '../../command-handlers-registry';
import { AppErrorFilter } from '../../filters/app-error.filter';
import { CommandRouter } from '../../command-router';
import { parseWebhook } from '../../tg-webhook-update-parser';
import { UnsupportedOperationError } from '../../errors/unsupported-operation.error';
import { MessageRouter } from '../../message-router';
import {InjectPinoLogger, PinoLogger} from 'nestjs-pino/dist';

@Controller('/api/webhooks')
@UseFilters(new AppErrorFilter())
export class WebhookController {
  constructor(
    private readonly commandHandlersRegistry: CommandHandlersRegistry,
    private readonly commandRouter: CommandRouter,
    private readonly messageRouter: MessageRouter,
    @InjectPinoLogger(WebhookController.name) private readonly logger: PinoLogger,
  ) {}

  @HttpCode(200)
  @Post('/main')
  public async webhook(@Body(new ValidationPipe422()) body: TelegramWebhookDto) {
    try {
      const tgUpdate = parseWebhook(body);
      if (tgUpdate.command) {
        await this.commandRouter.route(tgUpdate);
      } else {
        await this.messageRouter.route(tgUpdate);
      }
    } catch (err) {
      this.logger.error(err);

      if (err instanceof UnsupportedOperationError) {
        return {
          code: err.name,
        };
      }

      throw err;
    }
  }
}
