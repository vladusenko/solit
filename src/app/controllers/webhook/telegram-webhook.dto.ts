import { IsBoolean, IsDefined, IsNumber, IsOptional, IsPositive, IsString, ValidateNested } from 'class-validator';
import { IsNonPrimitiveArray } from '../../../decorators/is-non-primitive-array.decorator';
import { Type } from 'class-transformer';

class MessageEntity {
  @IsNumber()
  readonly offset: number;

  @IsNumber()
  readonly length: number;

  @IsString()
  readonly type: string;
}

class MessageChat { // tslint:disable-line max-classes-per-file
  @IsNumber()
  readonly id: number;

  @IsString()
  @IsOptional()
  readonly first_name: string; // tslint:disable-line variable-name

  @IsString()
  @IsOptional()
  readonly last_name: string; // tslint:disable-line variable-name

  @IsString()
  @IsOptional()
  readonly username: string;

  @IsString()
  readonly type: string;
}

class MessageFrom { // tslint:disable-line max-classes-per-file
  @IsNumber()
  readonly id: number;

  @IsBoolean()
  readonly is_bot: false; // tslint:disable-line variable-name

  @IsString()
  readonly first_name: string; // tslint:disable-line variable-name

  @IsString()
  @IsOptional()
  readonly last_name: string; // tslint:disable-line variable-name

  @IsString()
  @IsOptional()
  readonly username: string;

  @IsString()
  readonly language_code: string; // tslint:disable-line variable-name
}

class Message { // tslint:disable-line max-classes-per-file
  @IsNumber()
  readonly message_id: number; // tslint:disable-line variable-name

  @ValidateNested({always: true})
  @IsDefined()
  @Type(() => MessageFrom)
  readonly from: MessageFrom;

  @ValidateNested({always: true})
  @IsDefined()
  @Type(() => MessageChat)
  readonly chat: MessageChat;

  @IsNumber()
  readonly date: number;

  @IsString()
  readonly text: string;

  @ValidateNested({each: true})
  @IsNonPrimitiveArray()
  @IsOptional()
  @Type(() => MessageEntity)
  readonly entities?: MessageEntity[];
}

export class TelegramWebhookDto { // tslint:disable-line max-classes-per-file
  @IsNumber()
  @IsPositive()
  readonly update_id: number; // tslint:disable-line variable-name

  @ValidateNested({always: true})
  @IsOptional()
  @Type(() => Message)
  readonly message: Message;
}
