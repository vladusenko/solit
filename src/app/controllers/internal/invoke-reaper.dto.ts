import {IsString} from 'class-validator';

export class InvokeReaperDto {
  @IsString()
  region: string;
}
