import {Body, Controller, Post, Res, UseGuards} from '@nestjs/common';
import {ValidationPipe422} from '../../../pipes/validation-422.pipe';
import {InvokeReaperDto} from './invoke-reaper.dto';
import {ReaperCron} from '../../crons/reaper.cron';
import {Response} from 'express';
import {InternalApiGuard} from '../../guards/internal-api.guard';
import {InjectPinoLogger, PinoLogger} from 'nestjs-pino/dist';

@Controller('/api/internal')
@UseGuards(InternalApiGuard)
export class ReaperCronController {
  constructor(
    private readonly reaperCron: ReaperCron,
    @InjectPinoLogger(ReaperCronController.name) private readonly logger: PinoLogger,
  ) {}

  @Post('/invoke-reaper')
  public async invoke(
    @Body(new ValidationPipe422()) body: InvokeReaperDto,
    @Res() response: Response,
  ) {
    try {
      this.reaperCron.run()
        .then(() => this.logger.info('ReaperCron successfully finished'));
      response.json({invoked: true}).status(200).end();
    } catch (error) {
      this.logger.error(error);
    }
  }
}
