import {Controller, Post, Res, UseGuards} from '@nestjs/common';
import {Response} from 'express';
import {InternalApiGuard} from '../../guards/internal-api.guard';
import {UpdaterCron} from '../../crons/updater.cron';
import {InjectPinoLogger, PinoLogger} from 'nestjs-pino/dist';

@Controller('/api/internal')
@UseGuards(InternalApiGuard)
export class UpdaterCronController {
  constructor(
    private readonly updaterCron: UpdaterCron,
    @InjectPinoLogger(UpdaterCronController.name) private readonly logger: PinoLogger,
  ) {}

  @Post('/invoke-updater')
  public async invoke(
    @Res() response: Response,
  ) {
    try {
      this.updaterCron.run()
        .then(() => this.logger.info('UpdaterCron successfully finished'))
        .catch((error: Error) => this.logger.error({...error}));
      response.json({invoked: true}).status(200).end();
    } catch (error) {
      this.logger.error(error);
    }
  }
}
