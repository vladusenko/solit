import {Controller, Post, Res, UseGuards} from '@nestjs/common';
import {PinoLogger} from 'nestjs-pino';
import {Response} from 'express';
import {InternalApiGuard} from '../../guards/internal-api.guard';
import {FetcherCron} from '../../crons/fetcher.cron';
import {InjectPinoLogger} from 'nestjs-pino/dist';

@Controller('/api/internal')
@UseGuards(InternalApiGuard)
export class FetcherCronController {
  constructor(
    private readonly fetcherCron: FetcherCron,
    @InjectPinoLogger(FetcherCronController.name) private readonly logger: PinoLogger,
  ) {}

  @Post('/invoke-fetcher')
  public async invoke(
    @Res() response: Response,
  ) {
    try {
      this.fetcherCron.run()
        .then(() => this.logger.info('FetcherCron successfully finished'));
      response.json({invoked: true}).status(200).end();
    } catch (error) {
      this.logger.error(error);
    }
  }
}
