import {CanActivate, ExecutionContext, Inject, Injectable} from '@nestjs/common';

@Injectable()
export class InternalApiGuard implements CanActivate {
  constructor(
    @Inject('INTERNAL_API_SECRET_KEY') private readonly secretKey: string,
  ) {}

  canActivate(context: ExecutionContext): boolean {
    const request = context.switchToHttp().getRequest();
    const secret = request.headers['x-secret-key'];
    return secret === this.secretKey;
  }
}
