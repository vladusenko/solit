import { ChatRepository } from './repositories/models/chat.repository';
import { Sequelize } from 'sequelize';
import { Test, TestingModule } from '@nestjs/testing';
import { Commands } from '../enums/commands';
import { UnsupportedCommandError } from './errors/unsupported-command.error';
import { ChatStateRepository } from './repositories/states/chat-state.repository';
import { TagsStateRepository } from './repositories/states/tags-state.repository';
import { PreferenceRepository } from './repositories/models/preference.repository';
import { SeClient } from './se-client';
import { MessageHandlersRegistry } from './message-handlers-registry';
import { StartCommandStatesMessageHandler } from './message-handlers/start-command-states.message-handler';
import { TemplateProvider } from './template-provider';
import {MessageScheduler} from './message-scheduler/message-scheduler';
import {MessageSchedulerRepository} from './repositories/message-scheduler.repository';
import {TagsService} from './services/tags.service';
import {QuestionRepository} from './repositories/models/question.repository';
import {AnswerRepository} from './repositories/models/answer.repository';
import {MessageService} from './services/message.service';
import {LinkRepository} from './repositories/models/link.repository';
import {MessageRepository} from './repositories/models/message.repository';
import {TelegramClient} from './services/telegram-client.service';
import {LoggerModule} from 'nestjs-pino/dist';

describe('MessageHandlersRegistry unit tests', () => {
  let chatRepository: ChatRepository;
  let messageSchedulerRepository: MessageSchedulerRepository;
  let sequelize: Sequelize;
  let chatStateRepository: ChatStateRepository;
  let tagsStateRepository: TagsStateRepository;
  let preferenceRepository: PreferenceRepository;
  let seClient: SeClient;
  let messageHandlersRegistry: MessageHandlersRegistry;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [
        LoggerModule.forRoot(),
      ],
      providers: [
        MessageHandlersRegistry, ChatRepository, ChatStateRepository,
        TagsStateRepository, QuestionRepository, AnswerRepository, PreferenceRepository, TagsService,
        SeClient, TemplateProvider, MessageScheduler, MessageSchedulerRepository, MessageService,
        LinkRepository, MessageRepository, TelegramClient,
        {
          provide: 'SequelizeToken',
          useValue: {
            transaction: (options, fun) => {
              return fun();
            },
          },
        },
        {
          provide: 'RedisToken',
          useValue: {},
        },
        {
          provide: 'TELEGRAM_TOKEN',
          useValue: 'tg_token',
        },
        {
          provide: 'TELEGRAM_API_HOST',
          useValue: 'tghost',
        },
        {
          provide: 'SE_API_URL',
          useValue: 'se_api',
        },
        {
          provide: 'SE_API_KEY',
          useValue: 'se_api_key',
        },
        {
          provide: 'SE_FILTER_NAME',
          useValue: 'se_filter_name',
        },
        {
          provide: 'ChatModel',
          useValue: {},
        },
        {
          provide: 'PreferenceModel',
          useValue: {},
        },
        {
          provide: 'QuestionModel',
          useValue: {},
        },
        {
          provide: 'AnswerModel',
          useValue: {},
        },
        {
          provide: 'MessageModel',
          useValue: {},
        },
        {
          provide: 'LinkModel',
          useValue: {},
        },
        {
          provide: 'ANALYTICS_BASE_URL',
          useValue: 'base_url',
        }],
    }).compile();

    chatRepository = app.get<ChatRepository>(ChatRepository);
    messageSchedulerRepository = app.get<MessageSchedulerRepository>(MessageSchedulerRepository);
    sequelize = app.get<Sequelize>('SequelizeToken');
    chatStateRepository = app.get<ChatStateRepository>(ChatStateRepository);
    tagsStateRepository = app.get<TagsStateRepository>(TagsStateRepository);
    preferenceRepository = app.get<PreferenceRepository>(PreferenceRepository);
    seClient = app.get<SeClient>(SeClient);
    messageHandlersRegistry = app.get<MessageHandlersRegistry>(MessageHandlersRegistry);
  });

  it('should return start command handler for /start command', async () => {
    const handler = messageHandlersRegistry.get(Commands.START);

    expect(handler).toBeInstanceOf(StartCommandStatesMessageHandler);
  });

  it('should throw UnsupportedCommandError in case there is no handler for this command', async () => {
    let error;

    try {
      messageHandlersRegistry.get('/fake_command' as Commands);
    } catch (err) {
      error = err;
    }

    expect(error).toBeInstanceOf(UnsupportedCommandError);
  });
});
