import {Inject, Injectable} from '@nestjs/common';
import Axios, { AxiosInstance } from 'axios';
import * as _ from 'lodash';
import {wait} from '../utils/generic-utils';
import {ApiDownError} from './errors/api-down.error';
import {InjectPinoLogger, PinoLogger} from 'nestjs-pino/dist';

export interface SeUser {
  accept_rate?: number;
  badge_counts?: {
    bronze: number;
    gold: number;
    silver: number;
  };
  display_name?: string;
  link?: string;
  profile_image?: string;
  reputation?: number;
  user_id?: number;
  user_type?: 'unregistered' | 'registered' | 'moderator' | 'team_admin' | 'does_not_exist';
}

export interface SeQuestion {
  tags: string[];
  owner: SeUser;
  is_answered: boolean;
  view_count: number;
  closed_date?: number;
  answer_count: number;
  score: number;
  last_activity_date: number;
  creation_date: number;
  last_edit_date: number;
  question_id: number;
  link: string;
  closed_reason?: string;
  title: string;
}

export interface SeAnswer {
  answer_id: number;
  body: string;
  body_markdown: string;
  can_flag: string;
  comments_count: number;
  community_owned_date?: number;
  creation_date?: number;
  down_vote_count: number;
  is_accepted: boolean;
  last_activity_date: number;
  last_edit_date?: number;
  last_editor?: SeUser;
  link: string;
  locked_date?: number;
  owner?: SeUser;
  question_id: number;
  score: number;
  share_link: string;
  title: string;
  up_vote_count: number;
}

@Injectable()
export class SeClient {
  private readonly axios: AxiosInstance;

  constructor(
    @Inject('SE_API_URL') private readonly seApiUrl: string,
    @Inject('SE_API_KEY') private readonly seApiKey: string,
    @Inject('SE_FILTER_NAME') private readonly seFilterName: string,
    @InjectPinoLogger(SeClient.name) private readonly logger: PinoLogger,
  ) {
    this.validateTag = this.validateTag.bind(this);
    this.searchMostRelevantQuestions = this.searchMostRelevantQuestions.bind(this);
    this.getQuestions = this.getQuestions.bind(this);
    this.getQuestionsCurrentState = this.getQuestionsCurrentState.bind(this);
    this.getRelatedTags = this.getRelatedTags.bind(this);
    this.getTagSynonyms = this.getTagSynonyms.bind(this);
    this.getAnswers = this.getAnswers.bind(this);
    this.axios = Axios.create({
      baseURL: seApiUrl,
      headers: {
        'Accept-Encoding': 'gzip',
      },
    });
  }

  private logQuota(remainingQuota: number): void {
    this.logger.info({source: SeClient.name, data: {remainingQuota}});
  }

  private async retryRequestOnSocketHungUp(url: string, attempts: number, interval: number) {
    let counter = 1;
    while (counter !== attempts) {
      try {
        return await this.axios(url);
      } catch (err) {
        if (err.code === 'ECONNRESET' || _.inRange(_.get(err, 'response.status', 0), 499, 599)) {
          counter++;
          await wait(interval);
        } else {
          throw err;
        }
      }
    }

    throw new ApiDownError(`API failed to respond after ${attempts} attempts`, url);
  }

  public async validateTag(tag: string): Promise<string> {
    const { data } = await this.axios(`/tags/${encodeURIComponent(tag)}/info?order=desc&sort=popular&site=stackoverflow&key=${this.seApiKey}`);
    this.logQuota(data.quota_remaining);
    return _.get(data, 'items[0].name', null);
  }

  public async searchMostRelevantQuestions(tag: string, from: number, to: number): Promise<SeQuestion[]> {
    const { data } = await this.axios(
      // tslint:disable-next-line:max-line-length
      `search?pagesize=5&fromdate=${from}&todate=${to}&order=desc&sort=votes&tagged=${encodeURIComponent(tag)}&site=stackoverflow&key=${this.seApiKey}`,
    );
    this.logQuota(data.quota_remaining);
    return _.get(data, 'items', []);
  }

  public async getQuestions(lastQueried: number, page: number, to: number): Promise<{has_more: boolean, items: SeQuestion[]}> {
    const {data} = await this.axios(
      `questions?page=${page}&pagesize=50&fromdate=${lastQueried}&todate=${to}&order=asc&sort=creation&site=stackoverflow&key=${this.seApiKey}`,
    );

    this.logQuota(data.quota_remaining);

    return {
      has_more: data.has_more,
      items: data.items,
    };
  }

  public async getQuestionsCurrentState(ids: number[]): Promise<SeQuestion[]> {
    const questions = [];
    let hasMore = true;
    let page = 1;
    let quota;

    while (hasMore) {
      const url = `questions/${encodeURIComponent(ids.join(';'))}?order=desc&sort=creation&page=${page}&site=stackoverflow&key=${this.seApiKey}`;
      const {data} = await this.retryRequestOnSocketHungUp(url, 20, 7000);
      questions.push(...data.items);
      hasMore = data.has_more;
      page++;
      quota = data.quota_remaining;
    }

    this.logQuota(quota);

    return questions;
  }

  public async getRelatedTags(tag: string): Promise<string[]> {
    const tags = [];
    let page = 1;
    let hasMore = true;

    while (hasMore || page === 2) {
      const url = `tags/${encodeURIComponent(tag)}/related?page=${page}&site=stackoverflow&key=${this.seApiKey}`;
      const {data} = await this.retryRequestOnSocketHungUp(url, 10, 5000);
      tags.push(...data.items);
      hasMore = data.has_more;
      page++;
      this.logQuota(data.quota_remaining);
    }

    return tags.map((item) => item.name);
  }

  public async getTagSynonyms(tag: string): Promise<string[]> {
    const tags = [];
    let page = 1;
    let hasMore = true;

    while (hasMore || page === 2) {
      const url = `tags/${encodeURIComponent(tag)}/synonyms?page=${page}&site=stackoverflow&key=${this.seApiKey}`;
      const {data} = await this.retryRequestOnSocketHungUp(url, 10, 5000);
      tags.push(...data.items);
      hasMore = data.has_more;
      page++;
      this.logQuota(data.quota_remaining);
    }

    return tags.map((item) => item.from_tag);
  }

  public async getAnswers(questionIds: number[]): Promise<SeAnswer[]> {
    const answers = [];
    let hasMore = true;
    let page = 1;
    let quota;

    while (hasMore) {
      // tslint:disable-next-line:max-line-length
      const url = `questions/${encodeURIComponent(questionIds.join(';'))}/answers?order=desc&sort=creation&page=${page}&site=stackoverflow&key=${this.seApiKey}&filter=${this.seFilterName}`;
      const {data} = await this.retryRequestOnSocketHungUp(url, 20, 7000);
      answers.push(...data.items);
      hasMore = data.has_more;
      page++;
      quota = data.quota_remaining;
    }

    this.logQuota(quota);

    return answers;
  }
}
