export const START_COMMAND_STATE = {
  command: '/start',
  steps: {
    AFTER_WELCOME: 'after_welcome',
    TAGS_SETUP: 'tags_setup',
  },
};
