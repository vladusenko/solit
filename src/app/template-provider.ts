import { Injectable } from '@nestjs/common';
import * as fs from 'fs';
import * as path from 'path';
import * as Mustache from 'mustache';
import { TemplateReadingError } from './errors/template-reading.error';

export const templates = {
  WELCOME: 'welcome.html',
  TAG_NOT_FOUND: 'tagNotFoundOnSe.html',
  TAG_ALREADY_ADDED: 'tagAlreadyAdded.html',
  LIT_LIST: 'litList.html',
  ALREADY_HAVE_ACCOUNT: 'alreadyHaveAnAccount.html',
  AFTER_WALKTHROUGH: 'afterWalkthrough.html',
  ADDED_TAG_WALKTHROUGH: 'addedTagWalkthrough.html',
  USE_MENU: 'useMenu.html',
  MY_SUBSCRIPTIONS: 'mySubscriptions.html',
  MY_SUBSCRIPTIONS_NO_TAGS: 'mySubscriptionsNoTags.html',
};

@Injectable()
export class TemplateProvider {
  private readonly templatesFolder = path.resolve(`${__dirname}/templates`);

  public async build(templateFileName: string, params: object): Promise<string> {
    const template = await (new Promise((resolve, reject) => {
      fs.readFile(`${this.templatesFolder}/${templateFileName}`, (err, data) => {
        if (err) {
          return reject(new TemplateReadingError(err.message));
        }
        resolve(data.toString('utf8'));
      });
    }));

    return Mustache.render(template, params);
  }
}
