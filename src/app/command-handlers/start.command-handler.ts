import { ChatRepository } from '../repositories/models/chat.repository';
import { Sequelize } from 'sequelize';
import { UuidUtil } from '../../utils/uuid.util';
import { CommandHandler } from './command-handler';
import { START_COMMAND_STATE } from '../states/states';
import { TelegramUpdate } from '../tg-webhook-update-parser';
import { ChatStateRepository } from '../repositories/states/chat-state.repository';
import { TemplateProvider, templates } from '../template-provider';
import { MessageScheduler } from '../message-scheduler/message-scheduler';
import {DateUtil} from '../../utils/date.util';
import {MESSAGE_CATEGORIES} from '../dictionaries/message-categories';
import {MessageService} from '../services/message.service';
import {PinoLogger} from 'nestjs-pino/dist';

export class StartCommandHandler extends CommandHandler {
  constructor(
    private readonly chatRepository: ChatRepository,
    private readonly chatStateRepository: ChatStateRepository,
    private readonly sequelize: Sequelize,
    private readonly templateProvider: TemplateProvider,
    private readonly messageScheduler: MessageScheduler,
    private readonly messageService: MessageService,
    private readonly logger: PinoLogger,
  ) {
    super();
  }

  async handle(tgUpdate: TelegramUpdate): Promise<void> {
    const { chatId, dateSent } = tgUpdate;
    const chat = await this.chatRepository.getByTelegramId(chatId);
    if (chat) {
      const messageData = {
        content: await this.templateProvider.build(templates.ALREADY_HAVE_ACCOUNT, {}),
        category: MESSAGE_CATEGORIES.REGULAR,
        chatId: chat.privateId,
      };
      const messageId = await this.messageService.createMessage(messageData);
      return void await this.messageScheduler.scheduleMessage({
        ...messageData,
        privateChatId: chat.privateId,
        telegramChatId: chat.telegramId,
      }, messageId, DateUtil.currentEpoch());
    }
    const createdChat = await this.chatRepository.create({id: UuidUtil.v4(), telegramId: chatId});
    this.logger.info(`Successfully created chat ${createdChat.id} with telegram id ${createdChat.telegramId}`);

    const welcomeMessageData = {
      content: await this.templateProvider.build(templates.WELCOME, {}),
      category: MESSAGE_CATEGORIES.WELCOME,
      chatId: createdChat.privateId,
    };
    const welcomeMessageId = await this.messageService.createMessage(welcomeMessageData);
    await this.messageScheduler.scheduleMessage({
      ...welcomeMessageData,
      privateChatId: createdChat.privateId,
      telegramChatId: createdChat.telegramId,
    }, welcomeMessageId, DateUtil.currentEpoch());

    await this.chatStateRepository.setStateByChatId(chatId, {
      command: tgUpdate.command,
      step: START_COMMAND_STATE.steps.AFTER_WELCOME,
      lastAction: dateSent,
      started: dateSent,
    });
  }
}
