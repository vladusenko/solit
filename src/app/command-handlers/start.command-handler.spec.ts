import { Test, TestingModule } from '@nestjs/testing';
import { StartCommandHandler } from './start.command-handler';
import { ChatRepository } from '../repositories/models/chat.repository';
import { Sequelize } from 'sequelize';
import * as sinon from 'sinon';
import { ChatStateRepository } from '../repositories/states/chat-state.repository';
import { TelegramUpdate } from '../tg-webhook-update-parser';
import { TemplateProvider, templates } from '../template-provider';
import {MessageScheduler} from '../message-scheduler/message-scheduler';
import {MessageSchedulerRepository} from '../repositories/message-scheduler.repository';
import {DateUtil} from '../../utils/date.util';
import {MessageService} from '../services/message.service';
import {MessageRepository} from '../repositories/models/message.repository';
import {LinkRepository} from '../repositories/models/link.repository';
import {TelegramClient} from '../services/telegram-client.service';
import {LoggerModule, PinoLogger} from 'nestjs-pino/dist';

const tgUpdate: TelegramUpdate = {
  updateId: 12345,
  chatId: 12345,
  originalText: '/start',
  dateSent: 123456789,
  command: '/start',
};
describe('StartCommandHandler unit tests', () => {
  let startCommandHandler: StartCommandHandler;
  let chatRepository: ChatRepository;
  let sequelize: Sequelize;
  let chatStateRepository: ChatStateRepository;
  let templateProvider: TemplateProvider;
  let messageSchedulerRepository: MessageSchedulerRepository;
  let messageScheduler: MessageScheduler;
  let logger: PinoLogger;
  let messageService: MessageService;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [
        LoggerModule.forRoot(),
      ],
      providers: [ChatRepository, ChatStateRepository, TemplateProvider, MessageScheduler, MessageSchedulerRepository, MessageService,
        MessageRepository, LinkRepository, TelegramClient,
        {
          provide: 'SequelizeToken',
          useValue: {
            transaction: (options, fun) => {
              return fun();
            },
          },
        },
        {
          provide: 'RedisToken',
          useValue: {},
        },
        {
          provide: 'TELEGRAM_TOKEN',
          useValue: 'tg_token',
        },
        {
          provide: 'TELEGRAM_API_HOST',
          useValue: 'tghost',
        },
        {
          provide: 'ANALYTICS_BASE_URL',
          useValue: 'http://some.domain/analytics',
        },
        {
          provide: 'ChatModel',
          useValue: {},
        },
        {
          provide: 'PreferenceModel',
          useValue: {},
        },
        {
          provide: 'MessageModel',
          useValue: {},
        },
        {
          provide: 'LinkModel',
          useValue: {},
        }],
    }).compile();

    chatRepository = app.get<ChatRepository>(ChatRepository);
    sequelize = app.get<Sequelize>('SequelizeToken');
    chatStateRepository = app.get<ChatStateRepository>(ChatStateRepository);
    templateProvider = app.get<TemplateProvider>(TemplateProvider);
    messageScheduler = app.get<MessageScheduler>(MessageScheduler);
    messageSchedulerRepository = app.get<MessageSchedulerRepository>(MessageSchedulerRepository);
    logger = app.get<PinoLogger>(PinoLogger);
    messageService = app.get(MessageService);

    startCommandHandler = new StartCommandHandler(
      chatRepository, chatStateRepository, sequelize,
      templateProvider, messageScheduler, messageService, logger,
    );
  });

  it('should create a chat if it does not exist', async () => {
    const getByChatIdStub = sinon.stub(chatRepository, 'getByTelegramId').callsFake(() => Promise.resolve(null));
    const createStub = sinon.stub(chatRepository, 'create').callsFake(() => Promise.resolve({id: 'id', privateId: 1, telegramId: tgUpdate.chatId}));
    const setStateStub = sinon.stub(chatStateRepository, 'setStateByChatId').callsFake(() => Promise.resolve());
    sinon.stub(messageSchedulerRepository, 'scheduleMessage').callsFake(() => Promise.resolve());
    const messageSchedulerScheduleMessageSpy = sinon.spy(messageScheduler, 'scheduleMessage');
    sinon.stub(DateUtil, 'currentEpoch').callsFake(() => 12345);
    sinon.stub(messageService, 'createMessage').callsFake(() => Promise.resolve(4848));

    await startCommandHandler.handle(tgUpdate);

    expect(getByChatIdStub.calledOnce).toBe(true);
    expect(createStub.calledOnce).toBe(true);
    expect(messageSchedulerScheduleMessageSpy.calledOnce).toBe(true);
    expect(messageSchedulerScheduleMessageSpy.calledWith({
      content: await templateProvider.build(templates.WELCOME, {}),
      category: 'welcome',
      chatId: 1,
      telegramChatId: tgUpdate.chatId,
      privateChatId: 1,
    }, 4848, DateUtil.currentEpoch())).toBe(true);
    expect(setStateStub.calledOnce).toBe(true);

    DateUtil.currentEpoch['restore'](); // tslint:disable-line:no-string-literal
  });

  it('should not create a chat if it already exists', async () => {
    const getByChatIdStub = sinon.stub(chatRepository, 'getByTelegramId')
      .callsFake(() => Promise.resolve({id: 'id', privateId: 1, telegramId: tgUpdate.chatId}));
    const createStub = sinon.stub(chatRepository, 'create').callsFake(() => Promise.resolve());
    sinon.stub(messageSchedulerRepository, 'scheduleMessage').callsFake(() => Promise.resolve());
    const messageSchedulerScheduleMessageSpy = sinon.spy(messageScheduler, 'scheduleMessage');
    sinon.stub(DateUtil, 'currentEpoch').callsFake(() => 12345);
    sinon.stub(chatStateRepository, 'setStateByChatId').callsFake(() => Promise.resolve());
    sinon.stub(messageService, 'createMessage').callsFake(() => Promise.resolve(4848));

    await startCommandHandler.handle(tgUpdate);

    expect(getByChatIdStub.calledOnce).toBe(true);
    expect(createStub.called).toBe(false);
    expect(messageSchedulerScheduleMessageSpy.calledOnce).toBe(true);
    expect(messageSchedulerScheduleMessageSpy.calledWith({
      content: await templateProvider.build(templates.ALREADY_HAVE_ACCOUNT, {}),
      category: 'regular',
      telegramChatId: tgUpdate.chatId,
      privateChatId: 1,
      chatId: 1,
    }, 4848, DateUtil.currentEpoch())).toBe(true);

    DateUtil.currentEpoch['restore'](); // tslint:disable-line:no-string-literal
  });
});
