import { TelegramUpdate } from '../tg-webhook-update-parser';

export abstract class CommandHandler {
  public abstract handle(tgUpdate: TelegramUpdate): Promise<any>;
}
