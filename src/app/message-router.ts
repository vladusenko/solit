import { Injectable } from '@nestjs/common';
import { TelegramUpdate } from './tg-webhook-update-parser';
import { MessageHandlersRegistry } from './message-handlers-registry';
import { ChatStateRepository } from './repositories/states/chat-state.repository';
import * as _ from 'lodash';
import {MenuMessageHandler} from './message-handlers/menu.message-handler';
import {MAIN_MENU} from './dictionaries/main-menu';

@Injectable()
export class MessageRouter {
  constructor(
    private readonly chatStateRepository: ChatStateRepository,
    private readonly messageHandlersRegistry: MessageHandlersRegistry,
    private readonly menuMessageHandler: MenuMessageHandler,
  ) {}

  public async route(tgUpdate: TelegramUpdate) {
    const state = await this.chatStateRepository.getByChatId(tgUpdate.chatId);
    if (_.keys(state).length !== 0) { // has state
      return await this.messageHandlersRegistry.get(state.command).handle(tgUpdate, state);
    }

    if (_.includes(_.values(MAIN_MENU), tgUpdate.originalText)) { // menu command
      return await this.menuMessageHandler.handle(tgUpdate.chatId, tgUpdate.originalText);
    }

    return; // ignore otherwise
  }
}
