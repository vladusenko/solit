import {Inject, Injectable} from '@nestjs/common';
import { MessageHandler } from './message-handlers/message-handler';
import { StartCommandStatesMessageHandler } from './message-handlers/start-command-states.message-handler';
import { ChatStateRepository } from './repositories/states/chat-state.repository';
import { TagsStateRepository } from './repositories/states/tags-state.repository';
import { SeClient } from './se-client';
import { PreferenceRepository } from './repositories/models/preference.repository';
import { ChatRepository } from './repositories/models/chat.repository';
import { UnsupportedCommandError } from './errors/unsupported-command.error';
import { TemplateProvider } from './template-provider';
import {MessageScheduler} from './message-scheduler/message-scheduler';
import {TagsService} from './services/tags.service';
import {QuestionRepository} from './repositories/models/question.repository';
import {AnswerRepository} from './repositories/models/answer.repository';
import {MessageService} from './services/message.service';

@Injectable()
export class MessageHandlersRegistry {
  private readonly registry: Map<string, MessageHandler> = new Map();

  constructor(
    private readonly chatRepository: ChatRepository,
    private readonly chatStateRepository: ChatStateRepository,
    private readonly preferenceRepository: PreferenceRepository,
    private readonly tagsStateRepository: TagsStateRepository,
    private readonly questionsRepository: QuestionRepository,
    private readonly answersRepository: AnswerRepository,
    private readonly seClient: SeClient,
    private readonly templateProvider: TemplateProvider,
    private readonly messageScheduler: MessageScheduler,
    private readonly tagsService: TagsService,
    private readonly messageService: MessageService,
    @Inject('ANALYTICS_BASE_URL') private readonly analyticsEndpoint: string,
  ) {
    this.registry.set('/start', new StartCommandStatesMessageHandler(
      chatRepository,
      chatStateRepository,
      tagsStateRepository,
      preferenceRepository,
      questionsRepository,
      answersRepository,
      seClient,
      messageScheduler,
      templateProvider,
      tagsService,
      messageService,
      analyticsEndpoint,
    ));
  }

  public get(command: string): MessageHandler {
    const handler = this.registry.get(command);
    if (!handler) {
      throw new UnsupportedCommandError(`No message handler found for command ${command}`);
    }

    return handler;
  }
}
