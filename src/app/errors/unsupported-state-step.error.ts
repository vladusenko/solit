import { AppError } from './app.error';

export class UnsupportedStateStepError extends AppError {
  constructor(message?: string) {
    super(message);
    this.name = 'UnsupportedStateStepError';
    Error.captureStackTrace(this);
    Object.setPrototypeOf(this, UnsupportedStateStepError.prototype);
  }
}
