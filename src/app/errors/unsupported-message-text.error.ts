import { UnsupportedOperationError } from './unsupported-operation.error';

export class UnsupportedMessageTextError extends UnsupportedOperationError {
  constructor(message?: string) {
    super(message);
    this.name = 'UnsupportedMessageTextError';
    Error.captureStackTrace(this);
    Object.setPrototypeOf(this, UnsupportedMessageTextError.prototype);
  }
}
