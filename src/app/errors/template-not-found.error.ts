import { AppError } from './app.error';

export class TemplateNotFoundError extends AppError {
  constructor(message) {
    super(message);
    this.name = 'TemplateNotFoundError';
    Error.captureStackTrace(this, this.constructor);
    Object.setPrototypeOf(this, TemplateNotFoundError.prototype);
  }
}
