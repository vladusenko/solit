import { UnsupportedOperationError } from './unsupported-operation.error';

export class UnsupportedChatTypeError extends UnsupportedOperationError {
  constructor(message?: string) {
    super(message);
    this.name = 'UnsupportedChatTypeError';
    Error.captureStackTrace(this);
    Object.setPrototypeOf(this, UnsupportedChatTypeError.prototype);
  }
}
