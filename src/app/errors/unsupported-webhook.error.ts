import { UnsupportedOperationError } from './unsupported-operation.error';

export class UnsupportedWebhookError extends UnsupportedOperationError {
  constructor(message?: string) {
    super(message);
    this.name = 'UnsupportedWebhookError';
    Error.captureStackTrace(this);
    Object.setPrototypeOf(this, UnsupportedWebhookError.prototype);
  }
}
