import { UnsupportedOperationError } from './unsupported-operation.error';

export class UnsupportedMessageTypeError extends UnsupportedOperationError {
  constructor(message?: string) {
    super(message);
    this.name = 'UnsupportedMessageTypeError';
    Error.captureStackTrace(this);
    Object.setPrototypeOf(this, UnsupportedMessageTypeError.prototype);
  }
}
