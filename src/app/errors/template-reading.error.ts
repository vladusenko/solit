import { AppError } from './app.error';

export class TemplateReadingError extends AppError {
  constructor(message) {
    super(message);
    this.name = 'TemplateReadingError';
    Error.captureStackTrace(this, this.constructor);
    Object.setPrototypeOf(this, TemplateReadingError.prototype);
  }
}
