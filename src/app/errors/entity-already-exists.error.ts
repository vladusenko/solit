import { AppError } from './app.error';

export class EntityAlreadyExistsError extends AppError {
  constructor(message) {
    super(message);
    this.name = 'EntityAlreadyExistsError';
    Error.captureStackTrace(this, this.constructor);
    Object.setPrototypeOf(this, EntityAlreadyExistsError.prototype);
  }
}
