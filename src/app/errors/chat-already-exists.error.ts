import { EntityAlreadyExistsError } from './entity-already-exists.error';

export class ChatAlreadyExistsError extends EntityAlreadyExistsError {
  constructor(message) {
    super(message);
    this.name = 'ChatAlreadyExistsError';
    Error.captureStackTrace(this, this.constructor);
    Object.setPrototypeOf(this, ChatAlreadyExistsError.prototype);
  }
}
