import { AppError } from './app.error';

export class UnsupportedOperationError extends AppError {
  constructor(message?: string) {
    super(message);
    this.name = 'UnsupportedOperationError';
    Error.captureStackTrace(this);
    Object.setPrototypeOf(this, UnsupportedOperationError.prototype);
  }
}
