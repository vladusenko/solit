import { AppError } from './app.error';

export class MessageSendingError extends AppError {
  constructor(message) {
    super(message);
    this.name = 'MessageSendingError';
    Error.captureStackTrace(this, this.constructor);
    Object.setPrototypeOf(this, MessageSendingError.prototype);
  }
}
