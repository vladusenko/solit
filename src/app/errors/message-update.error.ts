import { AppError } from './app.error';

export class MessageUpdateError extends AppError {
  constructor(message) {
    super(message);
    this.name = 'MessageUpdateError';
    Error.captureStackTrace(this, this.constructor);
    Object.setPrototypeOf(this, MessageUpdateError.prototype);
  }
}
