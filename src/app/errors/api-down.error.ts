import {AppError} from './app.error';

export class ApiDownError extends AppError {
  public readonly url: string;
  constructor(message, url) {
    super(message);
    this.name = 'ApiDownError';
    this.url = url;
    Error.captureStackTrace(this, this.constructor);
    Object.setPrototypeOf(this, ApiDownError.prototype);
  }
}
