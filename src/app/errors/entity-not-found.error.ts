import {AppError} from './app.error';

export class EntityNotFoundError extends AppError {
    constructor(
        public readonly entityName: string,
        message?: string,
    ) {
        super(message);
        this.name = 'EntityNotFoundError';
        Error.captureStackTrace(this);
        Object.setPrototypeOf(this, EntityNotFoundError.prototype);
    }
}
