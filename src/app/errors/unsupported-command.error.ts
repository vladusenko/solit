import { UnsupportedOperationError } from './unsupported-operation.error';

export class UnsupportedCommandError extends UnsupportedOperationError {
  constructor(message?: string) {
    super(message);
    this.name = 'UnsupportedCommandError';
    Error.captureStackTrace(this);
    Object.setPrototypeOf(this, UnsupportedCommandError.prototype);
  }
}
