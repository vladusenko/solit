import {Injectable} from '@nestjs/common';
import {MessageSchedulerRepository, ScheduledMessage} from '../repositories/message-scheduler.repository';

@Injectable()
export class MessageScheduler {
  constructor(
    private readonly messageSchedulerRepository: MessageSchedulerRepository,
  ) {}

  public async scheduleMessage(scheduledMessage: ScheduledMessage, id: string, rank: number) {
    await this.messageSchedulerRepository.scheduleMessage(id, 0, scheduledMessage, rank);
  }
}
