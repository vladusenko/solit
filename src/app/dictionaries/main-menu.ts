export const MAIN_MENU = {
  MY_SUBSCRIPTIONS: '📍 My subscriptions',
  EDIT_SUBSCRIPTIONS: '📝 Edit subscriptions',
  REGION: '🌍 Region',
  STOP_WORDS: '🚫 Stop words',
  BOT_STATS: '📈 Bot stats',
  DONATE: '💳 Donate',
};
