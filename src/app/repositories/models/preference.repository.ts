import { Inject, Injectable } from '@nestjs/common';
import { Transaction } from 'sequelize';

export interface Preference {
  id: string;
  privateId: number;
  chatId: number;
  tags: string[];
  createdAt: Date;
  updatedAt: Date;
}

@Injectable()
export class PreferenceRepository {
  constructor(
    @Inject('PreferenceModel') private readonly preferenceModel: any,
  ) {}

  public create(preference: Partial<Preference>, transaction?: Transaction) {
    return this.preferenceModel.create(preference, transaction);
  }
}
