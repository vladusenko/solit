import {Inject, Injectable} from '@nestjs/common';
import {Op, Sequelize, Transaction} from 'sequelize';
import {Link} from './link.repository';
import {Chat} from './chat.repository';
import {MESSAGE_CATEGORIES} from '../../dictionaries/message-categories';

export interface Message {
  id: string;
  privateId: number;
  chatId?: number;
  telegramId?: number;
  content: string;
  category: string;
  status: string;
  createdAt: Date;
  updatedAt: Date;
}

export interface MessageWithLinks extends Message {
  chat: Chat;
  links: Link[];
}

@Injectable()
export class MessageRepository {
  constructor(
    @Inject('MessageModel') private readonly messageModel,
    @Inject('LinkModel') private readonly linkModel,
    @Inject('ChatModel') private readonly chatModel,
    @Inject('SequelizeToken') private readonly sequelize: Sequelize,
  ) {}

  public create(message: Partial<Message>, transactionRef?: Transaction) {
    return this.messageModel.create(message, {ignoreDuplicates: true, transaction: transactionRef});
  }

  public setStatusById(id: string, status: string): Promise<void> {
    return void this.messageModel.update({
      status,
    }, {
      where: {
        id: {
          [Op.eq]: id,
        },
      },
    });
  }

  public setSentById(id: string, telegramId: number): Promise<void> {
    return void this.messageModel.update({
      status: 'sent',
      telegramId,
    }, {
      where: {
        id: {
          [Op.eq]: id,
        },
      },
    });
  }

  public bulkCreate(messages: Array<Partial<Message>>) {
    return this.messageModel.bulkCreate(messages);
  }

  public async getFullMessageInfoByLinkId(id: string, plain = true): Promise<MessageWithLinks> {
    // @ts-ignore
    const whereSubQuery = this.sequelize.dialect.QueryGenerator.selectQuery('Links', {
      attributes: ['messageId'],
      where: {
        id: {
          [Op.eq]: id,
        },
      },
    }).slice(0, -1); // getting rid of semicolon for subquery

    const result = await this.messageModel.findOne({
      where: {
        privateId: {
          [Op.eq]: Sequelize.literal(`(${whereSubQuery})`),
        },
        category: {
          [Op.eq]: MESSAGE_CATEGORIES.LIT_LIST,
        },
      },
      include: [{
        model: this.linkModel,
        as: 'links',
        required: true,
      }, 'chat'],
      order: [
        ['links', 'createdAt', 'ASC'],
        ['links', 'privateId', 'ASC'],
      ],
    });

    if (plain) {
      return result.toJSON();
    } else {
      return result;
    }
  }

  public async updateContentById(id: string, newContent: string): Promise<void> {
    await this.messageModel.update({
      content: newContent,
    }, {
      where: {
        id: {
          [Op.eq]: id,
        },
      },
    });
  }
}
