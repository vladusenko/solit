import {Inject, Injectable} from '@nestjs/common';
import {User} from './question.repository';
import {SeAnswer} from '../../se-client';
import {Document, Model} from 'mongoose';
import {InjectPinoLogger, PinoLogger} from 'nestjs-pino/dist';

export interface Answer {
  answer_id: number;
  body: string;
  body_markdown: string;
  can_flag: string;
  comments_count: number;
  community_owned_date?: number;
  creation_date?: number;
  down_vote_count: number;
  is_accepted: boolean;
  last_activity_date: number;
  last_edit_date?: number;
  last_editor?: User;
  link: string;
  locked_date?: number;
  owner?: User;
  question_id: number;
  score: number;
  share_link: string;
  title: string;
  up_vote_count: number;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface AnswerDocument extends Answer, Document {}

export interface AnswerAggregationResult {
  tags: string[];
  share_link: string;
  title: string;
  score: number;
  answer_id: number;
  question_id: number;
  question_link: string;
  owner?: User;
}

@Injectable()
export class AnswerRepository {
  constructor(
    @Inject('AnswerModel') private readonly answerModel: Model<AnswerDocument>,
    @InjectPinoLogger(AnswerRepository.name) private readonly logger: PinoLogger,
  ) {}

  public async bulkCreate(answers: SeAnswer[]): Promise<void> {
    try {
      await this.answerModel.collection.insertMany(answers, {ordered: false});
      this.logger.info(`Successfully inserted ${answers.length} questions`);
    } catch (err) {
      if (err.name === 'BulkWriteError') {
        this.logger.error({source: AnswerRepository.name, error: err.name, errorStack: err.stack});
        return;
      }

      throw err;
    }
  }

  public getTopByTagsAndOwnerReputation(
    tags: string[], fromDate: Date, toDate: Date,
    minOwnerReputation: number, amount: number,
  ): Promise<AnswerAggregationResult[]> {
    return this.answerModel.aggregate([
      {
        $lookup: {
          from: 'questions',
          localField: 'question_id',
          foreignField: 'question_id',
          as: 'questions',
        },
      },
      {
        $unwind: '$questions',
      },
      {
        $match: {
          'creation_date': {
            $gte: fromDate.getTime() / 1000,
            $lte: toDate.getTime() / 1000,
          },
          'questions.score': {
            $gte: 0,
          },
          'score': {
            $gte: 0,
          },
          'owner.reputation': {
            $gte: minOwnerReputation,
          },
          'questions.tags': {
            $elemMatch: {
              $in: tags,
            },
          },
        },
      },
      {
        $project: {
          tags: '$questions.tags',
          share_link: 1,
          title: 1,
          score: 1,
          answer_id: 1,
          question_id: 1,
          question_link: '$questions.link',
          owner: 1,
        },
      },
      {
        $sort: {
          score: -1,
        },
      },
      {
        $limit: amount,
      },
    ]).exec();
  }
}
