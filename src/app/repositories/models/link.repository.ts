import {Inject} from '@nestjs/common';
import {Op, Transaction} from 'sequelize';

export interface Link {
  id: string;
  privateId: number;
  category: string;
  address: string;
  tags: string[];
  title: string;
  clickCount: number;
  messageId: number;
  createdAt: Date;
  updatedAt: Date;
}

export class LinkRepository {
  constructor(
    @Inject('LinkModel') private readonly linkModel,
  ) {}

  public bulkCreate(links: Array<Partial<Link>>, transactionRef: Transaction) {
    return this.linkModel.bulkCreate(links, {transaction: transactionRef});
  }

  public incrementClickCountById(id: string) {
    return this.linkModel.increment({clickCount: 1}, {
      where: {
        id: {
          [Op.eq]: id,
        },
      },
    });
  }

  public getById(id: string) {
    return this.linkModel.findOne({
      where: {
        id: {
          [Op.eq]: id,
        },
      },
    });
  }
}
