import { Inject, Injectable } from '@nestjs/common';
import { Op, Transaction } from 'sequelize';
import { ChatModel } from '../../../db/pg/models/chat.model';
import {Preference} from './preference.repository';

export interface Chat {
  id: string;
  privateId: number;
  telegramId: number;
}

export interface ChatWithPreferences extends Chat {
  preferences: Preference;
}

@Injectable()
export class ChatRepository {
  constructor(
    @Inject('ChatModel') private readonly chatModel,
    @Inject('PreferenceModel') private readonly preferenceModel,
  ) {}

  public create(chat: Partial<Chat>, transaction?: Transaction): Promise<Chat> {
    return this.chatModel.create(chat, transaction);
  }

  public getByTelegramId(telegramId: number, transaction?: Transaction): Promise<Chat> {
    return this.chatModel.findOne({
      where: {
        telegramId: {
          [Op.eq]: telegramId,
        },
      },
      transaction,
    });
  }

  public async deleteByTelegramId(telegramId: number, transaction?: Transaction): Promise<boolean> {
    await this.chatModel.destroy({
      where: {
        telegramId: {
          [Op.eq]: telegramId,
        },
      },
      transaction,
    });

    return true;
  }

  public getByPrivateId(privateId: number, transaction?: Transaction): Promise<Chat> {
    return this.chatModel.findOne({
      where: {
        privateId: {
          [Op.eq]: privateId,
        },
      },
      transaction,
    });
  }

  public getChatsWithPreferences(): Promise<ChatWithPreferences[]> {
    return this.chatModel.findAll({
      include: [{
        model: this.preferenceModel,
        as: 'preferences',
        required: true, // force INNER JOIN
      }],
    });
  }

  public getChatWithPreferencesByTelegramId(id: number, transaction?: Transaction): Promise<ChatWithPreferences> {
    return this.chatModel.findOne({
      where: {
        telegramId: {
          [Op.eq]: id,
        },
      },
      include: [{
        model: this.preferenceModel,
        as: 'preferences',
        required: true,
      }],
      transaction,
    });
  }
}
