import { Inject, Injectable } from '@nestjs/common';
import {SeQuestion} from '../../se-client';
import {Model} from 'mongoose';
import { Document } from 'mongoose';
import {Logger} from 'pino';
import {DateUtil} from '../../../utils/date.util';

export interface User {
  accept_rate?: number;
  badge_counts?: {
    bronze: number;
    gold: number;
    silver: number;
  };
  display_name?: string;
  link?: string;
  profile_image?: string;
  reputation?: number;
  user_id?: number;
  user_type?: 'unregistered' | 'registered' | 'moderator' | 'team_admin' | 'does_not_exist';
}

export interface Question {
  tags: string[];
  owner: User;
  is_answered: boolean;
  view_count: number;
  closed_date?: number;
  answer_count: number;
  score: number;
  last_activity_date: number;
  creation_date: number;
  last_edit_date: number;
  question_id: number;
  link: string;
  closed_reason?: string;
  title: string;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface QuestionDocument extends Question, Document {}

@Injectable()
export class QuestionRepository {
  constructor(
    @Inject('QuestionModel') private readonly questionModel: Model<QuestionDocument>,
    @Inject('Logger') private readonly logger: Logger,
  ) {}

  public async bulkCreate(questions: Array<Partial<SeQuestion>>): Promise<void> {
    try {
      await this.questionModel.insertMany(questions, {ordered: false});
    } catch (err) {
      if (err.name === 'BulkWriteError') {
        this.logger.error({source: QuestionRepository.name, error: err.name, errorStack: err.stack});
        return;
      }

      throw err;
    }
  }

  public async getYesterdayQuestionSeIds(): Promise<number[]> {
    const docs = await this.questionModel.find({
      $and: [
          {
            creation_date: {
              $gte: DateUtil.oneDayBack().getTime() / 1000,
            },
          },
          {
            creation_date: {
              $lt: DateUtil.currentEpoch(),
            },
          },
        ],
      }).select('question_id');

    return docs.map((doc) => doc.question_id);
  }

  public async getQuestionsByTagsAndDate(tags: string[], fromDate: Date, toDate: Date, plain = true): Promise<Question[]> {
    const questions = await this.questionModel.find({
      creation_date: {
        $gte: fromDate.getTime() / 1000,
        $lte: toDate.getTime() / 1000,
      },
        tags: {
          $elemMatch: {
            $in: tags,
          },
        },
        score: {
          $gte: 0,
        },
      },
    );

    if (plain) {
      return questions.map((question) => question.toObject());
    } else {
      return questions;
    }
  }

  public async bulkUpdate(questions: SeQuestion[]): Promise<any> {
    const operations = [];
    for (const question of questions) {
      operations.push({
        updateOne: {
          filter: {
            question_id: question.question_id,
          },
          update: question,
        },
      });
    }
    return this.questionModel.bulkWrite(operations) as any;
  }

  public bulkDelete(ids: number[]): Promise<number> {
    return this.questionModel.deleteMany({
      question_id: {
        $in: ids,
      },
    }) as any;
  }
}
