import {Injectable} from '@nestjs/common';

@Injectable()
export class UpdaterCronStateRepository {
  public readonly lockResourceKey = 'lock:updater';
}
