import { Inject, Injectable } from '@nestjs/common';
import { Redis } from 'ioredis';

@Injectable()
export class FetcherCronStateRepository {
  public readonly lockResourceKey = 'lock:fetcher';

  private readonly lastQueriedKey = 'cron:fetcher:last_queried';

  constructor(
    @Inject('RedisToken') private readonly redis: Redis,
  ) {}

  public async setLastQueriedEpochIfNotExists(epoch: number): Promise<boolean> {
    const response = Number.parseInt(await this.redis.setnx(this.lastQueriedKey, epoch), 10);
    return !!response;
  }

  public async getLastQueriedEpoch(): Promise<number> {
    const response = await this.redis.get(this.lastQueriedKey);
    return Number.parseInt(response, 10);
  }

  public async setLastQueriedEpoch(epoch: number): Promise<void> {
    await this.redis.set(this.lastQueriedKey, epoch);
  }
}
