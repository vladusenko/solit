import {Inject, Injectable} from '@nestjs/common';
import {Redis} from 'ioredis';

@Injectable()
export class CleanerCronStateRepository {
  public readonly lockResourceKey = 'lock:cleaner';

  private readonly lastRunKey = 'cron:cleaner:last_run';

  constructor(
    @Inject('RedisToken') private readonly redis: Redis,
  ) {}

  public async setLastRun(epoch: number): Promise<void> {
    await this.redis.set(this.lastRunKey, epoch);
  }
}
