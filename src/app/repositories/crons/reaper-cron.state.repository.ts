import {Inject, Injectable} from '@nestjs/common';
import {Redis} from 'ioredis';

@Injectable()
export class ReaperCronStateRepository {
  public readonly lockResourceKey = 'lock:reaper';

  constructor(
    @Inject('RedisToken') private readonly redis: Redis,
  ) {}
}
