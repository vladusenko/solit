import {Inject, Injectable} from '@nestjs/common';
import {Redis} from 'ioredis';

@Injectable()
export class MailmanCronStateRepository {
  public readonly lockResourceKey = 'lock:mailman';

  constructor(
    @Inject('RedisToken') private readonly redis: Redis,
  ) {}
}
