import { Inject, Injectable } from '@nestjs/common';
import { Redis, Pipeline } from 'ioredis';

export interface RedisOptions {
  startPipeline?: boolean;
  startTransaction?: boolean;
  usePipeline?: Pipeline;
}

@Injectable()
export class TagsStateRepository {
  constructor(
    @Inject('RedisToken') private readonly redis: Redis,
  ) {}

  public pushTag(chatId: number, tag: string, options: RedisOptions = {}) {
    if (options.startPipeline) {
      return this.redis.pipeline().lpush(`tags:chat:${chatId}`, tag);
    } else if (options.startTransaction) {
      return this.redis.multi().lpush(`tags:chat:${chatId}`, tag);
    } else if (options.usePipeline) {
      return options.usePipeline.lpush(`tags:chat:${chatId}`, tag);
    } else {
      return this.redis.lpush(`tags:chat:${chatId}`, tag);
    }
  }

  public pushTagSynonyms(chatId: number, tag: string, synonyms: string[], options: RedisOptions = {}) {
    if (options.startPipeline) {
      return this.redis.pipeline().lpush(`synonyms:chat:${chatId}:${tag}`, ...synonyms);
    } else if (options.startTransaction) {
      return this.redis.multi().lpush(`synonyms:chat:${chatId}:${tag}`, ...synonyms);
    } else if (options.usePipeline) {
      return options.usePipeline.lpush(`synonyms:chat:${chatId}:${tag}`, ...synonyms);
    } else {
      return this.redis.lpush(`synonyms:chat:${chatId}:${tag}`, ...synonyms);
    }
  }

  public getCount(chatId: number) {
    return this.redis.llen(`tags:chat:${chatId}`);
  }

  public deleteTagsByChatId(chatId: number, options: RedisOptions = {}) {
    if (options.startPipeline) {
      return this.redis.pipeline().del(`tags:chat:${chatId}`);
    } else if (options.startTransaction) {
      return this.redis.multi().del(`tags:chat:${chatId}`);
    } else if (options.usePipeline) {
      return options.usePipeline.del(`tags:chat:${chatId}`);
    } else {
      return this.redis.del(`tags:chat:${chatId}`);
    }
  }

  public getAllTags(chatId: number, options: RedisOptions = {}) {
    if (options.startPipeline) {
      return this.redis.pipeline().lrange(`tags:chat:${chatId}`, 0, -1);
    } else if (options.startTransaction) {
      return this.redis.multi().lrange(`tags:chat:${chatId}`, 0, -1);
    } else if (options.usePipeline) {
      return options.usePipeline.lrange(`tags:chat:${chatId}`, 0, -1);
    } else {
      return this.redis.lrange(`tags:chat:${chatId}`, 0, -1);
    }
  }

  public getSynonymsByTag(chatId: number, tag: string, options: RedisOptions = {}) {
    if (options.startPipeline) {
      return this.redis.pipeline().lrange(`synonyms:chat:${chatId}:${tag}`, 0, -1);
    } else if (options.startTransaction) {
      return this.redis.multi().lrange(`synonyms:chat:${chatId}:${tag}`, 0, -1);
    } else if (options.usePipeline) {
      return options.usePipeline.lrange(`synonyms:chat:${chatId}:${tag}`, 0, -1);
    } else {
      return this.redis.lrange(`synonyms:chat:${chatId}:${tag}`, 0, -1);
    }
  }

  public deleteTagSynonymsByChatId(chatId: number, options: RedisOptions = {}) {
    const lua = 'for _,k in ipairs(redis.call(\'keys\',\'synonyms:chat:\'..ARGV[1]..\':*\')) do redis.call(\'del\',k) end';

    if (options.startPipeline) {
      return this.redis.pipeline().eval(lua, 1, 'chat_id', chatId);
    } else if (options.startTransaction) {
      return this.redis.multi().eval(lua, 1, 'chat_id', chatId);
    } else if (options.usePipeline) {
      return options.usePipeline.eval(lua, 1, 'chat_id', chatId);
    } else {
      return this.redis.eval(lua, 1, 'chat_id', chatId);
    }
  }
}
