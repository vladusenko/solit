import { Inject, Injectable } from '@nestjs/common';
import { Pipeline, Redis } from 'ioredis';
import { RedisOptions } from './tags-state.repository';

export interface State {
  command: string;
  step: string;
  started: number;
  lastAction: number;
}

@Injectable()
export class ChatStateRepository {
  constructor(
    @Inject('RedisToken') private readonly redis: Redis,
  ) {}

  public async getByChatId(chatId: number, options: RedisOptions = {}) {
    if (options.startPipeline) {
      return this.redis.pipeline().hgetall(`state:chat:${chatId}`);
    } else if (options.startTransaction) {
      return this.redis.multi().hgetall(`state:chat:${chatId}`);
    } else if (options.usePipeline) {
      return options.usePipeline.hgetall(`state:chat:${chatId}`);
    } else {
      return this.redis.hgetall(`state:chat:${chatId}`);
    }
  }

  public async setStateByChatId(chatId: number, state: State, options: RedisOptions = {}) {
    if (options.startPipeline) {
      return this.redis.pipeline().hmset(`state:chat:${chatId}`, state);
    } else if (options.startTransaction) {
      return this.redis.multi().hmset(`state:chat:${chatId}`, state);
    } else if (options.usePipeline) {
      return options.usePipeline.hmset(`state:chat:${chatId}`, state);
    } else {
      return this.redis.hmset(`state:chat:${chatId}`, state);
    }
  }

  public async updateStateByChatId(chatId: number, state: Partial<State>, options: RedisOptions = {}) {
    let pipeline: Pipeline;
    if (options.startPipeline) {
      pipeline = this.redis.pipeline();
    } else if (options.startTransaction) {
      pipeline = this.redis.multi();
    } else if (options.usePipeline) {
      pipeline = options.usePipeline;
    } else {
      pipeline = this.redis.pipeline();
    }

    for (const key in state) {
      if (state.hasOwnProperty(key)) {
        pipeline.hset(`state:chat:${chatId}`, key, state[key]);
      }
    }
    if (!options.usePipeline && !options.startTransaction && !options.startPipeline) {
      await pipeline.exec();
    } else {
      return pipeline;
    }
  }

  public async deleteStateByChatId(chatId: number, options: RedisOptions = {}) {
    if (options.startPipeline) {
      return this.redis.pipeline().del(`state:chat:${chatId}`);
    } else if (options.startTransaction) {
      return this.redis.multi().del(`state:chat:${chatId}`);
    } else if (options.usePipeline) {
      return options.usePipeline.del(`state:chat:${chatId}`);
    } else {
      return this.redis.del(`state:chat:${chatId}`);
    }
  }
}
