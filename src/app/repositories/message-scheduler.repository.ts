import {Inject, Injectable} from '@nestjs/common';
import {Redis} from 'ioredis';
import * as _ from 'lodash';

export interface ScheduledMessagePack {
  id: string;
  attempt: number;
  scheduledMessage: ScheduledMessage;
}

export interface ScheduledMessage {
  content: string;
  customKeyboard?: string[][];
  category: string;
  privateChatId: number;
  telegramChatId: number;
}

@Injectable()
export class MessageSchedulerRepository {
  constructor(
    @Inject('RedisToken') private readonly redis: Redis,
  ) {}

  public async scheduleMessage(id: string, attempt: number, scheduledMessage: ScheduledMessage, rank: number) {
    const messagePack = JSON.stringify({id, attempt, scheduledMessage});
    // @ts-ignore
    await this.redis.zadd('scheduled_messages', rank, messagePack);
  }

  public async getMessages(amount: number): Promise<Array<{scheduledMessagePack: ScheduledMessagePack, rank: number}>> {
    // @ts-ignore
    const raw: Array<(string | number)> = await this.redis.zpopmin('scheduled_messages', amount);
    const parsed = raw.map((item) => {
      if (typeof item === 'number') {
        return item;
      } else {
        return JSON.parse(item) as ScheduledMessagePack;
      }
    });
    const chunks = _.chunk(parsed, 2);
    const results: Array<{scheduledMessagePack: ScheduledMessagePack, rank: number}> = [];
    for (const chunk of chunks) {
      results.push({
        scheduledMessagePack: chunk[0] as ScheduledMessagePack,
        rank: chunk[1] as number,
      });
    }

    return results;
  }
}
