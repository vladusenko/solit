import { ChatRepository } from './repositories/models/chat.repository';
import { Sequelize } from 'sequelize';
import { Test, TestingModule } from '@nestjs/testing';
import { CommandHandlersRegistry } from './command-handlers-registry';
import { Commands } from '../enums/commands';
import { StartCommandHandler } from './command-handlers/start.command-handler';
import { UnsupportedCommandError } from './errors/unsupported-command.error';
import { ChatStateRepository } from './repositories/states/chat-state.repository';
import { TemplateProvider } from './template-provider';
import {MessageScheduler} from './message-scheduler/message-scheduler';
import {MessageSchedulerRepository} from './repositories/message-scheduler.repository';
import {MessageRepository} from './repositories/models/message.repository';
import {LinkRepository} from './repositories/models/link.repository';
import {MessageService} from './services/message.service';
import {TelegramClient} from './services/telegram-client.service';
import {LoggerModule} from 'nestjs-pino/dist';

describe('CommandHandlersRegistry unit tests', () => {
  let commandHandlersRegistry: CommandHandlersRegistry;
  let chatRepository: ChatRepository;
  let sequelize: Sequelize;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [
        LoggerModule.forRoot(),
      ],
      providers: [CommandHandlersRegistry, ChatStateRepository, ChatRepository, MessageScheduler, TemplateProvider,
        MessageSchedulerRepository, MessageService, MessageRepository, LinkRepository, TelegramClient,
        {
          provide: 'SequelizeToken',
          useValue: {
            transaction: (options, fun) => {
              return fun();
            },
          },
        },
        {
          provide: 'RedisToken',
          useValue: {},
        },
        {
          provide: 'TELEGRAM_TOKEN',
          useValue: 'tg_token',
        },
        {
          provide: 'TELEGRAM_API_HOST',
          useValue: 'tghost',
        },
        {
          provide: 'ChatModel',
          useValue: {},
        },
        {
          provide: 'PreferenceModel',
          useValue: {},
        },
        {
          provide: 'MessageModel',
          useValue: {},
        },
        {
          provide: 'LinkModel',
          useValue: {},
        },
        {
          provide: 'ANALYTICS_BASE_URL',
          useValue: 'base_url',
        }],
    }).compile();

    chatRepository = app.get<ChatRepository>(ChatRepository);
    sequelize = app.get<Sequelize>('SequelizeToken');
    commandHandlersRegistry = app.get<CommandHandlersRegistry>(CommandHandlersRegistry);
  });

  it('should return start command handler for /start command', async () => {
    const handler = commandHandlersRegistry.getHandlerForCommand(Commands.START);

    expect(handler).toBeInstanceOf(StartCommandHandler);
  });

  it('should throw UnsupportedCommandError in case there is no handler for this command', async () => {
    let error;

    try {
      commandHandlersRegistry.getHandlerForCommand('/fake_command' as Commands);
    } catch (err) {
      error = err;
    }

    expect(error).toBeInstanceOf(UnsupportedCommandError);
  });
});
