import * as mongoose from 'mongoose';
import {SeUserSchema} from './se-user.schema';

export const AnswerSchema = new mongoose.Schema({
  answer_id: {
    type: Number,
    required: true,
  },
  body: {
    type: String,
    required: false,
  },
  body_markdown: {
    type: String,
    required: false,
  },
  can_flag: {
    type: String,
    required: false,
  },
  comments_count: {
    type: Number,
    required: false,
  },
  community_owned_date: {
    type: Number,
    required: false,
  },
  creation_date: {
    type: Number,
    required: true,
  },
  down_vote_count: {
    type: Number,
    required: false,
  },
  is_accepted: {
    type: Boolean,
    required: true,
  },
  last_activity_date: {
    type: Number,
    required: false,
  },
  last_edit_date: {
    type: Number,
    required: false,
  },
  last_editor: {
    type: SeUserSchema,
    required: false,
  },
  locked_date: {
    type: Number,
    required: false,
  },
  owner: {
    type: SeUserSchema,
    required: false,
  },
  question_id: {
    type: Number,
    required: true,
  },
  score: {
    type: Number,
    required: true,
  },
  share_link: {
    type: String,
    required: true,
  },
  title: {
    type: String,
    required: true,
  },
  up_vote_count: {
    type: Number,
    required: false,
  },
}, {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  },
});

AnswerSchema.index({question_id: 1, answer_id: 1}, {unique: true});
AnswerSchema.index({creation_date: 1});
AnswerSchema.index({created_at: 1}, {expires: '14 days'});

AnswerSchema.virtual('creation_date_converted')
  .get(function() {
    return new Date(this.creation_date * 1000);
  });
