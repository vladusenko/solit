import * as mongoose from 'mongoose';
import {SeUserSchema} from './se-user.schema';

export const QuestionSchema = new mongoose.Schema({
  tags: {
    type: [String],
    required: true,
  },
  owner: SeUserSchema,
  is_answered: {
    type: Boolean,
    required: true,
  },
  view_count: {
    type: Number,
    required: true,
  },
  closed_date: {
    type: Number,
    required: false,
  },
  answer_count: {
    type: Number,
    required: true,
  },
  score: {
    type: Number,
    required: true,
  },
  last_activity_date: {
    type: Number,
    required: true,
  },
  creation_date: {
    type: Number,
    required: true,
  },
  last_edit_date: {
    type: Number,
    required: false,
  },
  question_id: {
    type: Number,
    required: true,
    unique: true,
  },
  link: {
    type: String,
    required: true,
  },
  closed_reason: {
    type: String,
    required: false,
  },
  title: {
    type: String,
    required: true,
  },
}, {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  },
});

QuestionSchema.index({question_id: 1}, {unique: true});
QuestionSchema.index({created_at: 1}, {expires: '14 days'});

QuestionSchema.virtual('creation_date_converted')
  .get(function() {
    return new Date(this.creation_date * 1000);
  });
