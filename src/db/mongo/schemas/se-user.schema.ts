import * as mongoose from 'mongoose';

export const SeUserSchema = new mongoose.Schema({
  accept_rate: {
    type: Number,
    required: false,
  },
  badge_counts: {
    bronze: {
      type: Number,
    },
    gold: {
      type: Number,
    },
    silver: {
      type: Number,
    },
  },
  display_name: {
    type: String,
    required: false,
  },
  link: {
    type: String,
    required: false,
  },
  profile_image: {
    type: String,
    required: false,
  },
  reputation: {
    type: Number,
    required: false,
  },
  user_id: {
    type: Number,
    required: false,
  },
  user_type: {
    type: String,
    required: true,
  },
});
