import * as mongoose from 'mongoose';
import {QuestionSchema} from '../schemas/question.schema';

export const Question = mongoose.model('Question', QuestionSchema);
