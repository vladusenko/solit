import * as mongoose from 'mongoose';
import {AnswerSchema} from '../schemas/answer.schema';

export const Answer = mongoose.model('Answer', AnswerSchema);
