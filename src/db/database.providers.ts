import {Sequelize} from 'sequelize';
import {ChatModel} from './pg/models/chat.model';
// @ts-ignore
import Redis from 'ioredis';
import {PreferenceModel} from './pg/models/preference';
import {setupRelations} from './pg/relations';
import {Provider} from '@nestjs/common';
import * as Redlock from 'redlock';
import * as moment from 'moment';
import {MessageModel} from './pg/models/message';

import {Question} from './mongo/models/question.model';
import {Answer} from './mongo/models/answer.model';
import * as mongoose from 'mongoose';
import {LinkModel} from './pg/models/link';

export const databaseProviders: Provider[] = [
  {
    provide: 'SequelizeToken',
    useFactory: async (host, port, username, password, database) => {
      const sequelize = new Sequelize({
        dialect: 'postgres',
        host,
        port,
        username,
        password,
        database,
      });

      const models = [
        ChatModel,
        PreferenceModel,
        MessageModel,
        LinkModel,
      ];
      models.forEach((model) => model.define(sequelize));
      setupRelations(sequelize);
      if (process.env.NODE_ENV === 'TEST') {
        await sequelize.sync({force: true});
      }
      return sequelize;
    },
    inject: ['PG_HOST', 'PG_PORT', 'PG_USERNAME', 'PG_PASSWORD', 'PG_DATABASE'],
  },
  {
    provide: 'MongooseToken',
    useFactory: async (mongoConnectionString: string) => {
      await mongoose.connect(mongoConnectionString, {useNewUrlParser: true});
      return mongoose;
    },
    inject: ['MONGO_CONNECTION_STRING'],
  },
  {
    provide: 'ChatModel',
    useValue: ChatModel,
  },
  {
    provide: 'PreferenceModel',
    useValue: PreferenceModel,
  },
  {
    provide: 'QuestionModel',
    useValue: Question,
  },
  {
    provide: 'MessageModel',
    useValue: MessageModel,
  },
  {
    provide: 'LinkModel',
    useValue: LinkModel,
  },
  {
    provide: 'AnswerModel',
    useValue: Answer,
  },
  {
    provide: 'RedisToken',
    useFactory: async (host, port) => {
      const redis = new Redis({
        host,
        port,
        lazyConnect: true,
      });

      await redis.connect();

      if (process.env.NODE_ENV === 'DEV' || process.env.NODE_ENV === 'TEST') {
        await redis.set('cron:fetcher:last_queried', Math.floor(moment().subtract(1, 'hours').toDate().getTime() / 1000));
      }
      return redis;
    },
    inject: ['REDIS_CLUSTER_HOST', 'REDIS_CLUSTER_PORT'],
  },
  {
    provide: 'RedlockToken',
    useFactory: (redis) => {
      return new Redlock([redis], {
        driftFactor: 0.01,
        retryCount: 2,
        retryDelay: 100,
        retryJitter: 200,
      });
    },
    inject: ['RedisToken'],
  },
];
