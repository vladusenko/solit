import { Sequelize, Model, DataTypes } from 'sequelize';

export class ChatModel extends Model {
  public static define(sequelize: Sequelize) {
    ChatModel.init({
      id: {
        type: DataTypes.UUID,
        allowNull: false,
        unique: true,
      },
      privateId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      telegramId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        unique: true,
      },
    }, {
      sequelize,
      timestamps: true,
      tableName: 'Chats',
    });
  }
}
