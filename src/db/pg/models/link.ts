import {Model, Sequelize, DataTypes} from 'sequelize';

export class LinkModel extends Model {
  public static define(sequelize: Sequelize) {
    LinkModel.init({
      id: {
        type: DataTypes.UUID,
        unique: true,
        allowNull: false,
      },
      privateId: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      category: {
        type: DataTypes.ENUM,
        values: ['answered_question', 'non_answered_question', 'guru_answer'],
        allowNull: false,
      },
      address: {
        type: DataTypes.STRING(512),
        allowNull: false,
      },
      tags: {
        type: DataTypes.ARRAY(DataTypes.STRING),
        allowNull: false,
      },
      title: {
        type: DataTypes.STRING(512),
        allowNull: false,
      },
      clickCount: {
        type: DataTypes.SMALLINT,
        allowNull: false,
      },
    }, {
      sequelize,
      timestamps: true,
      tableName: 'Links',
    });
  }
}
