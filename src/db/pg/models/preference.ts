import { Model, Sequelize, DataTypes } from 'sequelize';

export class PreferenceModel extends Model {
  public static define(sequelize: Sequelize) {
    PreferenceModel.init({
      id: {
        type: DataTypes.UUID,
        unique: true,
        allowNull: false,
      },
      privateId: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      tags: {
        type: DataTypes.ARRAY(DataTypes.STRING(128)),
        allowNull: false,
      },
    }, {
      sequelize,
      timestamps: true,
      tableName: 'Preferences',
    });
  }
}
