import { Model, Sequelize, DataTypes } from 'sequelize';

export class MessageModel extends Model {
  public static define(sequelize: Sequelize) {
    MessageModel.init({
      id: {
        type: DataTypes.UUID,
        unique: true,
        allowNull: false,
      },
      privateId: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      telegramId: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      content: {
        type: DataTypes.STRING(8192),
        allowNull: false,
      },
      category: {
        type: DataTypes.ENUM,
        values: ['regular', 'lit_list', 'welcome'],
      },
      status: {
        type: DataTypes.ENUM,
        values: ['scheduled', 'sent', 'failed_to_send'],
      },
    }, {
      sequelize,
      timestamps: true,
      tableName: 'Messages',
    });
  }
}
