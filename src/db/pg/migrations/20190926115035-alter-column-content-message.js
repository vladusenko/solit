const { DataTypes } = require('sequelize');

module.exports = {
  up: async (queryInterface) => {
    await queryInterface.changeColumn('Messages', 'content', {
      type: DataTypes.STRING(8192),
      allowNull: false,
    });
  },

  down: async (queryInterface) => {
    await queryInterface.changeColumn('Messages', 'content', {
      type: DataTypes.STRING(4096),
      allowNull: false,
    });
  }
};
