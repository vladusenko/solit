const {DataTypes} = require("sequelize");

module.exports = {
  up: async (queryInterface) => {
    await queryInterface.createTable('Preferences', {
      id: {
        type: DataTypes.UUID,
        unique: true,
        allowNull: false,
      },
      privateId: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      chatId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      tags: {
        type: DataTypes.ARRAY(DataTypes.STRING(128)),
        allowNull: false,
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE
      },
    });

    await queryInterface.addConstraint('Preferences', ['chatId'], {
      type: 'foreign key',
      name: 'Preferences_chatId_fkey',
      references: {
        table: 'Chats',
        field: 'privateId'
      },
      onDelete: 'cascade',
      onUpdate: 'cascade'
    });
  },
  down: async (queryInterface) => {
    await queryInterface.dropTable('Preferences');
  }
};
