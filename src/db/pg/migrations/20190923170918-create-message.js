const { DataTypes } = require('sequelize');

module.exports = {
  up: async (queryInterface) => {
    await queryInterface.createTable('Messages', {
      id: {
        type: DataTypes.UUID,
        unique: true,
        allowNull: false,
      },
      privateId: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      telegramId: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      chatId: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      content: {
        type: DataTypes.STRING(4096),
        allowNull: false,
      },
      status: {
        type: DataTypes.ENUM,
        values: ['scheduled', 'sent', 'failed_to_send'],
      },
      category: {
        type: DataTypes.ENUM,
        values: ['regular', 'lit_list', 'welcome'],
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
    });

    await queryInterface.addConstraint('Messages', ['chatId'], {
      type: 'foreign key',
      name: 'Messages_chatId_fkey',
      references: {
        table: 'Chats',
        field: 'privateId',
      },
      onDelete: 'cascade',
      onUpdate: 'cascade'
    });
  },
  down: async (queryInterface) => {
    await queryInterface.dropTable('Messages');
  }
};
