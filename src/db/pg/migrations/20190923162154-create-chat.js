const {DataTypes} = require('sequelize');

module.exports = {
  up: async (queryInterface) => {
    await queryInterface.createTable('Chats', {
      id: {
        type: DataTypes.UUID,
        allowNull: false,
        unique: true,
      },
      privateId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      telegramId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        unique: true,
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE
      },
    });
  },
  down: async (queryInterface) => {
    await queryInterface.dropTable('Chats');
  },
};
