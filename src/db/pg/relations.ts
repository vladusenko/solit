export function setupRelations(sequelize) {
  sequelize.models.ChatModel.hasOne(sequelize.models.PreferenceModel, {
    as: 'preferences',
    foreignKey: 'chatId',
    onDelete: 'CASCADE',
  });

  sequelize.models.ChatModel.hasMany(sequelize.models.MessageModel, {
    as: 'messages',
    foreignKey: {
      name: 'chatId',
      allowNull: true,
    },
    onDelete: 'CASCADE',
  });

  sequelize.models.MessageModel.belongsTo(sequelize.models.ChatModel, {
    as: 'chat',
    foreignKey: {
      name: 'chatId',
      allowNull: true,
    },
    onDelete: 'CASCADE',
  });

  sequelize.models.LinkModel.belongsTo(sequelize.models.MessageModel, {
    as: 'links',
    foreignKey: {
      name: 'messageId',
      allowNull: false,
    },
    onDelete: 'CASCADE',
  });

  sequelize.models.MessageModel.hasMany(sequelize.models.LinkModel, {
    as: 'links',
    foreignKey: {
      name: 'messageId',
      allowNull: false,
    },
    onDelete: 'CASCADE',
  });
}
