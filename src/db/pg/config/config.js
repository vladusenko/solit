require('dotenv').config();

module.exports = {
    development: {
        username: 'app',
        password: 'password',
        database: 'solit',
        host: '127.0.0.1',
        dialect: 'postgres',
    },
    staging: {
        username: process.env.PG_USERNAME,
        password: process.env.PG_PASSWORD,
        database: process.env.PG_DATABASE,
        host: process.env.PG_HOST,
        port: process.env.PG_PORT,
        dialect: 'postgres',
    },
};
