import * as ipRangeCheck from 'ip-range-check';

export const ipFilterMiddleware = (cidrs: string[]) => {
  return (req, res, next) => {
    if (process.env.NODE_ENV === 'TEST' || process.env.NODE_ENV === 'CI') {
      return next();
    }

    const xForwardedForHeader = req.headers['X-Forwarded-For'] || req.headers['x-forwarded-for'];
    if (ipRangeCheck(xForwardedForHeader, cidrs)) {
      next();
    } else {
      return res.status(403).json({message: 'IP address not in range'});
    }
  };
};
