export enum Commands {
  START = '/start',
  FOLLOW_TAG = '/follow_tag',
  UNFOLLOW_TAG = '/unfollow_tag',
}
