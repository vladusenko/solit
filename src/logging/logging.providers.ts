import {Provider} from '@nestjs/common';
import * as pino from 'pino';

export const loggingProviders: Provider[] = [
  {
    provide: 'Logger',
    useFactory: async () => {
      return pino();
    },
  },
];
