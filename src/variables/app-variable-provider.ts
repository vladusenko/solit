import * as dotenv from 'dotenv';
import {SSM} from 'aws-sdk';
import * as os from 'os';
const ssm = new SSM({region: 'eu-west-3'});

process.env.NODE_ENV = process.env.NODE_ENV || 'DEV';

if (process.env.NODE_ENV === 'DEV') {
  dotenv.config();
} else if (process.env.NODE_ENV === 'TEST') {
  dotenv.config({path: './test/.env'});
}

export const appVariablesProviders = [
  {
    provide: 'TELEGRAM_TOKEN',
    useFactory: async () => {
      if (process.env.NODE_ENV === 'STAGING' || process.env.NODE_ENV === 'PROD') {
        return (await ssm.getParameter({Name: `${process.env.NODE_ENV}_TELEGRAM_TOKEN`, WithDecryption: true}).promise()).Parameter.Value;
      }
      return process.env.TELEGRAM_TOKEN;
    },
  },
  {
    provide: 'TELEGRAM_API_HOST',
    useFactory: async () => {
      if (process.env.NODE_ENV === 'STAGING' || process.env.NODE_ENV === 'PROD') {
        return (await ssm.getParameter({Name: `${process.env.NODE_ENV}_TELEGRAM_API_HOST`, WithDecryption: true}).promise()).Parameter.Value;
      }
      return process.env.TELEGRAM_API_HOST;
    },
  },
  {
    provide: 'SE_API_URL',
    useFactory: async () => {
      if (process.env.NODE_ENV === 'STAGING' || process.env.NODE_ENV === 'PROD') {
        return (await ssm.getParameter({Name: `${process.env.NODE_ENV}_SE_API_URL`, WithDecryption: true}).promise()).Parameter.Value;
      }
      return process.env.SE_API_URL;
    },
  },
  {
    provide: 'SE_API_KEY',
    useFactory: async () => {
      if (process.env.NODE_ENV === 'STAGING' || process.env.NODE_ENV === 'PROD') {
        return (await ssm.getParameter({Name: `${process.env.NODE_ENV}_SE_API_KEY`, WithDecryption: true}).promise()).Parameter.Value;
      }
      return process.env.SE_API_KEY;
    },
  },
  {
    provide: 'PG_HOST',
    useFactory: async () => {
      if (process.env.NODE_ENV === 'STAGING' || process.env.NODE_ENV === 'PROD') {
        return (await ssm.getParameter({Name: `${process.env.NODE_ENV}_PG_HOST`, WithDecryption: true}).promise()).Parameter.Value;
      }
      return process.env.PG_HOST || 'localhost';
    },
  },
  {
    provide: 'PG_PORT',
    useFactory: async () => {
      if (process.env.NODE_ENV === 'STAGING' || process.env.NODE_ENV === 'PROD') {
        return Number.parseInt(
          (await ssm.getParameter({Name: `${process.env.NODE_ENV}_PG_PORT`, WithDecryption: true}).promise()).Parameter.Value,
          10,
        );
      }
      return Number.parseInt(process.env.PG_PORT, 10) || 5432;
    },
  },
  {
    provide: 'PG_USERNAME',
    useFactory: async () => {
      if (process.env.NODE_ENV === 'STAGING' || process.env.NODE_ENV === 'PROD') {
        return (await ssm.getParameter({Name: `${process.env.NODE_ENV}_PG_USERNAME`, WithDecryption: true}).promise()).Parameter.Value;
      }
      return process.env.PG_USERNAME || 'app';
    },
  },
  {
    provide: 'PG_PASSWORD',
    useFactory: async () => {
      if (process.env.NODE_ENV === 'STAGING' || process.env.NODE_ENV === 'PROD') {
        return (await ssm.getParameter({Name: `${process.env.NODE_ENV}_PG_PASSWORD`, WithDecryption: true}).promise()).Parameter.Value;
      }
      return process.env.PG_PASSWORD || 'password';
    },
  },
  {
    provide: 'PG_DATABASE',
    useFactory: async () => {
      if (process.env.NODE_ENV === 'STAGING' || process.env.NODE_ENV === 'PROD') {
        return (await ssm.getParameter({Name: `${process.env.NODE_ENV}_PG_DATABASE`, WithDecryption: true}).promise()).Parameter.Value;
      }
      return process.env.PG_DATABASE || 'solit';
    },
  },
  {
    provide: 'REDIS_CLUSTER_HOST',
    useFactory: async () => {
      if (process.env.NODE_ENV === 'STAGING' || process.env.NODE_ENV === 'PROD') {
        return (await ssm.getParameter({Name: `${process.env.NODE_ENV}_REDIS_CLUSTER_HOST`, WithDecryption: true}).promise()).Parameter.Value;
      }
      return process.env.REDIS_CLUSTER_HOST || 'localhost';
    },
  },
  {
    provide: 'REDIS_CLUSTER_PORT',
    useFactory: async () => {
      if (process.env.NODE_ENV === 'STAGING' || process.env.NODE_ENV === 'PROD') {
        return Number.parseInt(
          (await ssm.getParameter({Name: `${process.env.NODE_ENV}_REDIS_CLUSTER_PORT`, WithDecryption: true}).promise()).Parameter.Value,
          10,
        );
      }
      return process.env.REDIS_CLUSTER_PORT || 6379;
    },
  },
  {
    provide: 'THREADPOOL_MAX_SIZE',
    useFactory: () => os.cpus().length,
  },
  {
    provide: 'SE_FILTER_NAME',
    useFactory: async () => {
      if (process.env.NODE_ENV === 'STAGING' || process.env.NODE_ENV === 'PROD') {
        return (await ssm.getParameter({Name: `${process.env.NODE_ENV}_SE_FILTER_NAME`, WithDecryption: true}).promise()).Parameter.Value;
      }
      return process.env.SE_FILTER_NAME;
    },
  },
  {
    provide: 'MONGO_CONNECTION_STRING',
    useFactory: async () => {
      if (process.env.NODE_ENV === 'STAGING' || process.env.NODE_ENV === 'PROD') {
        return (await ssm.getParameter({Name: `${process.env.NODE_ENV}_MONGO_CONNECTION_STRING`, WithDecryption: true}).promise()).Parameter.Value;
      }
      return process.env.MONGO_CONNECTION_STRING;
    },
  },
  {
    provide: 'ANALYTICS_BASE_URL',
    useFactory: async () => {
      if (process.env.NODE_ENV === 'STAGING' || process.env.NODE_ENV === 'PROD') {
        return (await ssm.getParameter({Name: `${process.env.NODE_ENV}_ANALYTICS_BASE_URL`, WithDecryption: true}).promise()).Parameter.Value;
      }
      return process.env.ANALYTICS_BASE_URL;
    },
  },
  {
    provide: 'INTERNAL_API_SECRET_KEY',
    useFactory: async () => {
      if (process.env.NODE_ENV === 'STAGING' || process.env.NODE_ENV === 'PROD') {
        return (await ssm.getParameter({Name: `${process.env.NODE_ENV}_INTERNAL_API_SECRET_KEY`, WithDecryption: true}).promise()).Parameter.Value;
      }
      return process.env.INTERNAL_API_SECRET_KEY;
    },
  },
];
