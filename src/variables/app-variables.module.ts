import {Global, Module} from '@nestjs/common';
import {appVariablesProviders} from './app-variable-provider';

@Global()
@Module({
    providers: [...appVariablesProviders],
    exports: [...appVariablesProviders],
})
export class AppVariablesModule {}
