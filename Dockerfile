# syntax=docker/dockerfile:experimental
FROM node:12-alpine

RUN apk update && apk upgrade && apk add --no-cache bash

RUN adduser -D dev

RUN apk add --no-cache python && \
   python -m ensurepip && \
   rm -r /usr/lib/python*/ensurepip && \
   pip install --upgrade pip setuptools && \
   rm -r /root/.cache

RUN apk --no-cache add g++ make

WORKDIR /home/dev/app

COPY package.json ./
COPY package-lock.json ./

RUN npm install

USER dev

COPY ./dist ./
